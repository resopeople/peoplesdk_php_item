<?php
/**
 * This class allows to define schema entity collection class.
 * key => SchemaEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\item\schema\model\SchemaEntity;



/**
 * @method null|SchemaEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(SchemaEntity $objEntity) @inheritdoc
 */
class SchemaEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return SchemaEntity::class;
    }



}