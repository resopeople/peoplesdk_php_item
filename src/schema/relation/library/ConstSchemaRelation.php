<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\relation\library;



class ConstSchemaRelation
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_SCHEMA_ENTITY_FACTORY = 'objSchemaEntityFactory';
    const DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG = 'tabSchemaEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_NAME = 'strAttrName';
    const ATTRIBUTE_KEY_SCHEMA = 'attrSchema';
    const ATTRIBUTE_KEY_SUB_SCHEMA = 'attrSubSchema';
    const ATTRIBUTE_KEY_SUB_ITEM_COUNT_MAX = 'intAttrSubItemCountMax';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_NAME = 'name';
    const ATTRIBUTE_ALIAS_SCHEMA = 'schema';
    const ATTRIBUTE_ALIAS_SUB_SCHEMA = 'sub-schema';
    const ATTRIBUTE_ALIAS_SUB_ITEM_COUNT_MAX = 'sub-item-count-max';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_NAME = 'name';
    const ATTRIBUTE_NAME_SAVE_SCHEMA = 'schema';
    const ATTRIBUTE_NAME_SAVE_SUB_SCHEMA = 'sub-schema';
    const ATTRIBUTE_NAME_SAVE_SUB_ITEM_COUNT_MAX = 'sub-item-count-max';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_PROFILE = 'profile';



    // Exception message constants
    const EXCEPT_MSG_SCHEMA_ENTITY_FACTORY_INVALID_FORMAT =
        'Following schema entity factory "%1$s" invalid! It must be a schema entity factory object.';
    const EXCEPT_MSG_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the schema entity factory execution configuration standard.';



}