<?php
/**
 * This class allows to define schema relation browser entity class.
 * Schema relation browser entity allows to define attributes,
 * to search schema relation entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\relation\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\item\schema\relation\browser\library\ConstSchemaRelationBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|string $strAttrCritLikeName
 * @property null|string $strAttrCritEqualName
 * @property null|string[] $tabAttrCritInName
 * @property null|boolean $boolAttrCritIsSubItemCountMaxDefined
 * @property null|integer $intAttrCritEqualSubItemCountMax
 * @property null|integer $intAttrCritLessSubItemCountMax
 * @property null|integer $intAttrCritGreaterSubItemCountMax
 * @property null|integer $intAttrCritEqualSchemaId
 * @property null|integer[] $tabAttrCritInSchemaId
 * @property null|string $strAttrCritLikeSchemaName
 * @property null|string $strAttrCritEqualSchemaName
 * @property null|string[] $tabAttrCritInSchemaName
 * @property null|integer $intAttrCritEqualSubSchemaId
 * @property null|integer[] $tabAttrCritInSubSchemaId
 * @property null|string $strAttrCritLikeSubSchemaName
 * @property null|string $strAttrCritEqualSubSchemaName
 * @property null|string[] $tabAttrCritInSubSchemaName
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortName
 * @property null|string $strAttrSortSubItemCountMax
 * @property null|string $strAttrSortSchemaId
 * @property null|string $strAttrSortSchemaName
 * @property null|string $strAttrSortSubSchemaId
 * @property null|string $strAttrSortSubSchemaName
 */
class SchemaRelationBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is sub item count maximum defined
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_SUB_ITEM_COUNT_MAX_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IS_SUB_ITEM_COUNT_MAX_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal sub item count maximum
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_COUNT_MAX,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_ITEM_COUNT_MAX,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria less than sub item count maximum
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_LESS_SUB_ITEM_COUNT_MAX,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LESS_SUB_ITEM_COUNT_MAX,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria greater than sub item count maximum
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_GREATER_SUB_ITEM_COUNT_MAX,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_GREATER_SUB_ITEM_COUNT_MAX,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal sub schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in sub schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SUB_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like sub schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SUB_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_SUB_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal sub schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in sub schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SUB_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort sub item count maximum
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_COUNT_MAX,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_SORT_SUB_ITEM_COUNT_MAX,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_SORT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_SORT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort sub schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_SORT_SUB_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort sub schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaRelationBrowser::ATTRIBUTE_ALIAS_SORT_SUB_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $getTabRuleConfigValidInteger = function(
            $boolCompareEqualRequire = true,
            $boolNullRequire = true
        )
        {
            $boolCompareEqualRequire = (is_bool($boolCompareEqualRequire) ? $boolCompareEqualRequire : true);
            $boolNullRequire = (is_bool($boolNullRequire) ? $boolNullRequire : true);
            $result = array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-valid-integer' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => $boolCompareEqualRequire
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' =>
                            '%1$s must be a ' .
                            ($boolCompareEqualRequire ? '' : 'strict ') .
                            'positive integer.'
                    ]
                ]
            );

            if($boolNullRequire)
            {
                $result[0][1]['rule_config']['is-null'] = array('is_null');
                $result[0][1]['error_message_pattern'] =
                    '%1$s must be null or a ' .
                    ($boolCompareEqualRequire ? '' : 'strict ') .
                    'positive integer.';
            }

            return $result;
        };
        $tabRuleConfigValidId = $getTabRuleConfigValidInteger(false);
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => $getTabRuleConfigValidInteger(
                                        false,
                                        false
                                    )
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigValidDate,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigValidDate,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigValidDate,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigValidDate,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME => $tabRuleConfigValidString,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME => $tabRuleConfigValidString,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME => $tabRuleConfigValidTabString,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_SUB_ITEM_COUNT_MAX_DEFINED => $tabRuleConfigValidBoolean,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_COUNT_MAX => $getTabRuleConfigValidInteger(),
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_LESS_SUB_ITEM_COUNT_MAX => $getTabRuleConfigValidInteger(false),
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_GREATER_SUB_ITEM_COUNT_MAX => $getTabRuleConfigValidInteger(),
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ID => $tabRuleConfigValidId,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ID => $tabRuleConfigValidTabId,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_NAME => $tabRuleConfigValidTabString,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_SCHEMA_ID => $tabRuleConfigValidId,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_SCHEMA_ID => $tabRuleConfigValidTabId,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SUB_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_SCHEMA_NAME => $tabRuleConfigValidTabString,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigValidSort,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigValidSort,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_NAME => $tabRuleConfigValidSort,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_COUNT_MAX => $tabRuleConfigValidSort,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_ID => $tabRuleConfigValidSort,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_NAME => $tabRuleConfigValidSort,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_SCHEMA_ID => $tabRuleConfigValidSort,
                ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_SCHEMA_NAME => $tabRuleConfigValidSort
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_COUNT_MAX:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_LESS_SUB_ITEM_COUNT_MAX:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_GREATER_SUB_ITEM_COUNT_MAX:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ID:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_SCHEMA_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ID:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_SCHEMA_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_string($value) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ($value instanceof DateTime) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_NAME:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_NAME:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SUB_SCHEMA_NAME:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_SCHEMA_NAME:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_NAME:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_COUNT_MAX:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_ID:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_NAME:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_SCHEMA_ID:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_SCHEMA_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_NAME:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_SCHEMA_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_SUB_ITEM_COUNT_MAX_DEFINED:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = $this->getAttributeValueFormatGet($strKey, $value);
                break;

            case ConstSchemaRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_SUB_ITEM_COUNT_MAX_DEFINED:
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}