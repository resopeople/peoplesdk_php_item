<?php
/**
 * This class allows to define schema relation entity collection class.
 * key => SchemaRelationEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\relation\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\item\schema\relation\model\SchemaRelationEntity;



/**
 * @method null|SchemaRelationEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(SchemaRelationEntity $objEntity) @inheritdoc
 */
class SchemaRelationEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return SchemaRelationEntity::class;
    }



}