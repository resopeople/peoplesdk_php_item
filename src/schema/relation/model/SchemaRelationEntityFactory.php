<?php
/**
 * This class allows to define schema relation entity factory class.
 * Schema relation entity factory allows to provide new schema relation entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\relation\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\item\schema\model\SchemaEntityFactory;
use people_sdk\item\schema\relation\library\ConstSchemaRelation;
use people_sdk\item\schema\relation\exception\SchemaEntityFactoryInvalidFormatException;
use people_sdk\item\schema\relation\exception\SchemaEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\schema\relation\model\SchemaRelationEntity;



/**
 * @method null|SchemaEntityFactory getObjSchemaEntityFactory() Get schema entity factory object.
 * @method null|array getTabSchemaEntityFactoryExecConfig() Get schema entity factory execution configuration array.
 * @method SchemaRelationEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjSchemaEntityFactory(null|SchemaEntityFactory $objSchemaEntityFactory) Set schema entity factory object.
 * @method void setTabSchemaEntityFactoryExecConfig(null|array $tabSchemaEntityFactoryExecConfig) Set schema entity factory execution configuration array.
 */
class SchemaRelationEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|SchemaEntityFactory $objSchemaEntityFactory = null
     * @param null|array $tabSchemaEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        SchemaEntityFactory $objSchemaEntityFactory = null,
        array $tabSchemaEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init schema entity factory
        $this->setObjSchemaEntityFactory($objSchemaEntityFactory);

        // Init schema entity factory execution config
        $this->setTabSchemaEntityFactoryExecConfig($tabSchemaEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstSchemaRelation::DATA_KEY_SCHEMA_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstSchemaRelation::DATA_KEY_SCHEMA_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstSchemaRelation::DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstSchemaRelation::DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstSchemaRelation::DATA_KEY_SCHEMA_ENTITY_FACTORY,
            ConstSchemaRelation::DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstSchemaRelation::DATA_KEY_SCHEMA_ENTITY_FACTORY:
                    SchemaEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstSchemaRelation::DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG:
                    SchemaEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => SchemaRelationEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstSchemaRelation::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new SchemaRelationEntity(
            $objDateTimeFactory,
            array(),
            $objValidator,
            $this->getObjSchemaEntityFactory(),
            $this->getTabSchemaEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}