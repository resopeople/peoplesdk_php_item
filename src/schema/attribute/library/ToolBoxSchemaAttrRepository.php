<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\attribute\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\repository\library\ConstRepository as BaseConstRepository;
use liberty_code\requisition\request\factory\library\ConstRequestFactory;
use liberty_code\requisition\persistence\library\ConstPersistor;
use liberty_code\attribute_model\attribute\library\ConstAttribute;
use people_sdk\library\table\library\ToolBoxTable;
use people_sdk\library\model\repository\library\ConstRepository;
use people_sdk\item\requisition\request\info\library\ToolBoxItemSndInfo;
use people_sdk\item\schema\attribute\library\ConstSchemaAttr;
use people_sdk\item\schema\attribute\model\SchemaAttrEntity;
use people_sdk\item\schema\attribute\model\SchemaAttrEntityCollection;
use people_sdk\item\schema\attribute\model\repository\SchemaAttrEntitySimpleCollectionRepository;



class ToolBoxSchemaAttrRepository extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get execution configuration array,
     * with specified schema engine.
     *
     * Configuration array format:
     * Null or @see SchemaAttrEntitySimpleCollectionRepository::load() configuration array format.
     *
     * Return format:
     * Null or @see SchemaAttrEntitySimpleCollectionRepository::load() configuration array format.
     *
     * @param string $strSubActionType
     * @param null|array $tabConfig = null
     * @return null|array
     */
    public static function getTabExecConfigWithSchemaEngine($strSubActionType, array $tabConfig = null)
    {
        // Init var
        $tabSrcConfig = $tabConfig;
        $tabConfig = (
            (
                is_string($strSubActionType) &&
                in_array(
                    $strSubActionType,
                    array(
                        ConstSchemaAttr::SUB_ACTION_TYPE_SCHEMA,
                        ConstSchemaAttr::SUB_ACTION_TYPE_PROFILE_SCHEMA_GET,
                        ConstSchemaAttr::SUB_ACTION_TYPE_PROFILE_SCHEMA_UPDATE
                    )
                )
            ) ?
                array(ConstRepository::TAB_EXEC_CONFIG_KEY_PERSIST_SUB_ACTION_TYPE => $strSubActionType) :
                null
        );
        $result = ToolBoxTable::getTabItemFromSrc($tabConfig, $tabSrcConfig);

        // Return result
        return $result;
    }



    /**
     * Get execution configuration array,
     * with schema.
     *
     * Configuration array format:
     * @see getTabExecConfigWithSchemaEngine() configuration array format.
     *
     * Return format:
     * @see getTabExecConfigWithSchemaEngine() return format.
     *
     * @param integer $intSchemaId
     * @param null|array $tabConfig = null
     * @return array
     */
    public static function getTabExecConfigWithSchema($intSchemaId, array $tabConfig = null)
    {
        // init var
        $tabSrcConfig = $tabConfig;
        $tabConfig = (
            (!is_null($tabSndInfo = ToolBoxItemSndInfo::getTabSndInfoWithSchemaUrlRouteArg($intSchemaId))) ?
                array(
                    BaseConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG => [
                        ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                            ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => $tabSndInfo
                        ]
                    ]
                ) :
                null
        );
        $result = static::getTabExecConfigWithSchemaEngine(
            ConstSchemaAttr::SUB_ACTION_TYPE_SCHEMA,
            ToolBoxTable::getTabItemFromSrc($tabConfig, $tabSrcConfig)
        );

        // Return result
        return $result;
    }



    /**
     * Get execution configuration array,
     * with profile schema engine.
     *
     * Configuration array format:
     * @see getTabExecConfigWithSchemaEngine() configuration array format.
     *
     * Return format:
     * @see getTabExecConfigWithSchemaEngine() return format.
     *
     * @param string $strSubActionType
     * @param integer $intAreaId
     * @param null|array $tabConfig = null
     * @return null|array
     */
    public static function getTabExecConfigWithProfileSchemaEngine(
        $strSubActionType,
        $intAreaId,
        array $tabConfig = null
    )
    {
        // init var
        $strSubActionType = (($strSubActionType !== ConstSchemaAttr::SUB_ACTION_TYPE_SCHEMA) ? $strSubActionType : null);
        $tabSrcConfig = $tabConfig;
        $tabConfig = (
            (!is_null($tabSndInfo = ToolBoxItemSndInfo::getTabSndInfoWithAreaUrlRouteArg($intAreaId))) ?
                array(
                    BaseConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG => [
                        ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                            ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => $tabSndInfo
                        ]
                    ]
                ) :
                null
        );
        $result = static::getTabExecConfigWithSchemaEngine(
            $strSubActionType,
            ToolBoxTable::getTabItemFromSrc($tabConfig, $tabSrcConfig)
        );

        // Return result
        return $result;
    }



    /**
     * Get execution configuration array,
     * with profile schema get.
     *
     * Configuration array format:
     * @see getTabExecConfigWithProfileSchemaEngine() configuration array format.
     *
     * Return format:
     * @see getTabExecConfigWithProfileSchemaEngine() return format.
     *
     * @param integer $intAreaId
     * @param null|array $tabConfig = null
     * @return array
     */
    public static function getTabExecConfigWithProfileSchemaGet($intAreaId, array $tabConfig = null)
    {
        // Return result
        return static::getTabExecConfigWithProfileSchemaEngine(
            ConstSchemaAttr::SUB_ACTION_TYPE_PROFILE_SCHEMA_GET,
            $intAreaId,
            $tabConfig
        );
    }



    /**
     * Get execution configuration array,
     * with profile schema update.
     *
     * Configuration array format:
     * @see getTabExecConfigWithProfileSchemaEngine() configuration array format.
     *
     * Return format:
     * @see getTabExecConfigWithProfileSchemaEngine() return format.
     *
     * @param integer $intAreaId
     * @param null|array $tabConfig = null
     * @return array
     */
    public static function getTabExecConfigWithProfileSchemaUpdate($intAreaId, array $tabConfig = null)
    {
        // Return result
        return static::getTabExecConfigWithProfileSchemaEngine(
            ConstSchemaAttr::SUB_ACTION_TYPE_PROFILE_SCHEMA_UPDATE,
            $intAreaId,
            $tabConfig
        );
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Load specified schema attribute entity collection,
     * from specified schema attribute entity simple collection repository,
     * to get specified schema engine.
     * Return true if success, false if an error occurs.
     *
     * Configuration array format:
     * @see getTabExecConfigWithSchemaEngine() configuration array format.
     *
     * @param SchemaAttrEntityCollection $objSchemaAttrEntityCollection
     * @param SchemaAttrEntitySimpleCollectionRepository $objSchemaAttrEntityCollectionRepository
     * @param null|array $tabConfig = null
     * @return boolean
     */
    public static function loadSchemaEngine(
        SchemaAttrEntityCollection $objSchemaAttrEntityCollection,
        SchemaAttrEntitySimpleCollectionRepository $objSchemaAttrEntityCollectionRepository,
        array $tabConfig = null
    )
    {
        // Init var
        $tabInitSchemaAttrEntity = array_values($objSchemaAttrEntityCollection->getTabItem());

        // Load schema
        $objSchemaAttrEntityCollection->removeItemAll();
        $result = (
            (!is_null($tabConfig)) ?
                $objSchemaAttrEntityCollectionRepository->load(
                    $objSchemaAttrEntityCollection,
                    array(),
                    $tabConfig
                ) :
                false
        );

        // Set initial schema attributes
        $tabInitSchemaAttrEntity = (
            $result ?
                array_filter(
                    $tabInitSchemaAttrEntity,
                    function(SchemaAttrEntity $objSchemaAttrEntity) use ($objSchemaAttrEntityCollection) {
                        $tabConfig = array([
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY => ConstAttribute::ATTRIBUTE_KEY_NAME,
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION => ConstEntity::COLLECTION_GET_CONFIG_OPERATION_EQUAL,
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_VALUE => $objSchemaAttrEntity->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_NAME)
                        ]);

                        return (count($objSchemaAttrEntityCollection->getTabItem($tabConfig)) == 0);
                    }
                ) :
                $tabInitSchemaAttrEntity
        );
        $objSchemaAttrEntityCollection->setTabItem($tabInitSchemaAttrEntity);

        // Reorder schema attributes, if required
        if($result)
        {
            $tabSchemaAttrEntity = $objSchemaAttrEntityCollection->getTabSortItem();

            $objSchemaAttrEntityCollection->removeItemAll();
            $objSchemaAttrEntityCollection->setTabItem($tabSchemaAttrEntity);
        }

        // Return result
        return $result;
    }



    /**
     * Load specified schema attribute entity collection,
     * from specified schema attribute entity simple collection repository,
     * to get specified schema.
     * Return true if success, false if an error occurs.
     *
     * Configuration array format:
     * @see loadSchemaEngine() configuration array format.
     *
     * @param SchemaAttrEntityCollection $objSchemaAttrEntityCollection
     * @param SchemaAttrEntitySimpleCollectionRepository $objSchemaAttrEntityCollectionRepository
     * @param integer $intSchemaId
     * @param null|array $tabConfig = null
     * @return boolean
     */
    public static function loadSchema(
        SchemaAttrEntityCollection $objSchemaAttrEntityCollection,
        SchemaAttrEntitySimpleCollectionRepository $objSchemaAttrEntityCollectionRepository,
        $intSchemaId,
        array $tabConfig = null
    )
    {
        // Init var
        $tabConfig = static::getTabExecConfigWithSchema($intSchemaId, $tabConfig);
        $result = static::loadSchemaEngine(
            $objSchemaAttrEntityCollection,
            $objSchemaAttrEntityCollectionRepository,
            $tabConfig
        );

        // Return result
        return $result;
    }



    /**
     * Load specified schema attribute entity collection,
     * from specified schema attribute entity simple collection repository,
     * to get specified profile schemas.
     * Return true if success, false if an error occurs.
     *
     * Configuration array format:
     * @see loadSchemaEngine() configuration array format.
     *
     * @param SchemaAttrEntityCollection $objSchemaAttrEntityCollection
     * @param SchemaAttrEntitySimpleCollectionRepository $objSchemaAttrEntityCollectionRepository
     * @param integer $intAreaId
     * @param boolean $boolProfileSchemaGetRequired = false
     * @param boolean $boolProfileSchemaUpdateRequired = false
     * @param null|array $tabConfig = null
     * @return boolean
     */
    public static function loadProfileSchema(
        SchemaAttrEntityCollection $objSchemaAttrEntityCollection,
        SchemaAttrEntitySimpleCollectionRepository $objSchemaAttrEntityCollectionRepository,
        $intAreaId,
        $boolProfileSchemaGetRequired = false,
        $boolProfileSchemaUpdateRequired = false,
        array $tabConfig = null
    )
    {
        // Init var
        $boolProfileSchemaGetRequired =(is_bool($boolProfileSchemaGetRequired) ? $boolProfileSchemaGetRequired : false);
        $boolProfileSchemaUpdateRequired =(is_bool($boolProfileSchemaUpdateRequired) ? $boolProfileSchemaUpdateRequired : false);
        $result = true;

        // Load profile schema get, if required
        if($boolProfileSchemaGetRequired)
        {
            $tabConfig = static::getTabExecConfigWithProfileSchemaGet($intAreaId, $tabConfig);
            $result = static::loadSchemaEngine(
                $objSchemaAttrEntityCollection,
                $objSchemaAttrEntityCollectionRepository,
                $tabConfig
            ) && $result;
        }

        // Load profile schema update, if required
        if($boolProfileSchemaUpdateRequired)
        {
            $tabConfig = static::getTabExecConfigWithProfileSchemaUpdate($intAreaId, $tabConfig);
            $result = static::loadSchemaEngine(
                $objSchemaAttrEntityCollection,
                $objSchemaAttrEntityCollectionRepository,
                $tabConfig
            ) && $result;
        }

        // Return result
        return $result;
    }



}