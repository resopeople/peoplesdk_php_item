<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\attribute\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\crypto\library\ToolBoxHash;



class ToolBoxSchemaAttr extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string attribute key (used in entity attribute configuration),
     * from specified attribute name.
     *
     * @param string $strName
     * @return null|string
     */
    public static function getStrAttributeKey($strName)
    {
        // Return result
        return (is_string($strName) ? ToolBoxHash::getStrHash($strName) : null);
    }



}