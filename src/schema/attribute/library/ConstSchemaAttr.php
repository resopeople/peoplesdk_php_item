<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\attribute\library;



class ConstSchemaAttr
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_SCHEMA_ENTITY_FACTORY = 'objSchemaEntityFactory';
    const DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG = 'tabSchemaEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_SCHEMA = 'attrSchema';

    const ATTRIBUTE_ALIAS_SCHEMA = 'schema';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_NAME = 'name';
    const ATTRIBUTE_NAME_SAVE_DATA_TYPE = 'data-type';
    const ATTRIBUTE_NAME_SAVE_VALUE_REQUIRED = 'value-required';
    const ATTRIBUTE_NAME_SAVE_LIST_VALUE = 'list-value';
    const ATTRIBUTE_NAME_SAVE_RULE_CONFIG = 'rule-config';
    const ATTRIBUTE_NAME_SAVE_DEFAULT_VALUE = 'default-value';
    const ATTRIBUTE_NAME_SAVE_ORDER = 'order';
    const ATTRIBUTE_NAME_SAVE_SCHEMA = 'schema';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_SCHEMA = 'schema';
    const SUB_ACTION_TYPE_PROFILE_SCHEMA_GET = 'profile_schema_get';
    const SUB_ACTION_TYPE_PROFILE_SCHEMA_UPDATE = 'profile_schema_update';



    // Exception message constants
    const EXCEPT_MSG_SCHEMA_ENTITY_FACTORY_INVALID_FORMAT =
        'Following schema entity factory "%1$s" invalid! It must be a schema entity factory object.';
    const EXCEPT_MSG_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the schema entity factory execution configuration standard.';
    const EXCEPT_MSG_ATTRIBUTE_VALUE_IS_NULL = 'Following attribute value "%1$s" is null!';



}