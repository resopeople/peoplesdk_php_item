<?php
/**
 * This class allows to define schema attribute entity class.
 * Schema attribute entity is a save attribute entity class,
 * can be used on item entity, from specific schema.
 *
 * Schema attribute entity uses the following specified configuration:
 * [
 *     Save attribute entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\attribute\model;

use liberty_code\attribute_model\attribute\repository\model\SaveAttributeEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\attribute_model\attribute\library\ConstAttribute;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\null_value\library\ToolBoxNullValue;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\item\schema\library\ConstSchema;
use people_sdk\item\schema\model\SchemaEntity;
use people_sdk\item\schema\model\SchemaEntityFactory;
use people_sdk\item\schema\attribute\library\ConstSchemaAttr;
use people_sdk\item\schema\attribute\library\ToolBoxSchemaAttr;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|string $strAttrName
 * @property null|string $strAttrAlias @deprecated
 * @property null|string $strAttrDataType
 * @property null|boolean $boolAttrValueRequired
 * @property null|array $tabAttrListValue
 * @property null|array $tabAttrRuleConfig
 * @property null|string|mixed $attrDefaultValue
 * @property null|integer $intAttrOrder
 * @property null|integer|SchemaEntity $attrSchema : schema id|entity
 */
class SchemaAttrEntity extends SaveAttributeEntity
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Schema entity factory instance.
     * @var null|SchemaEntityFactory
     */
    protected $objSchemaEntityFactory;



    /**
     * DI: Schema entity factory execution configuration.
     * @var null|array
     */
    protected $tabSchemaEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|SchemaEntityFactory $objSchemaEntityFactory = null
     * @param null|array $tabSchemaEntityFactoryExecConfig = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        AttrSpecInterface $objAttrSpec = null,
        SchemaEntityFactory $objSchemaEntityFactory = null,
        array $tabSchemaEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objSchemaEntityFactory = $objSchemaEntityFactory;
        $this->tabSchemaEntityFactoryExecConfig = $tabSchemaEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct(
            $tabValue,
            $objValidator,
            $objDateTimeFactory,
            $objAttrSpec
        );
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkAttrAliasRequired()
    {
        // Return result
        return false;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Init var
        $tabUpdateConfig = array(
            // Select and update attribute id
            ConstAttribute::ATTRIBUTE_KEY_ID => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute datetime create
            ConstAttribute::ATTRIBUTE_KEY_DT_CREATE => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute datetime update
            ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute name
            ConstAttribute::ATTRIBUTE_KEY_NAME => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute data type
            ConstAttribute::ATTRIBUTE_KEY_DATA_TYPE => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_DATA_TYPE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute value required
            ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_VALUE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute list value
            ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_LIST_VALUE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute rule configuration
            ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_RULE_CONFIG,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute default value
            ConstAttribute::ATTRIBUTE_KEY_DEFAULT_VALUE => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_DEFAULT_VALUE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute order
            ConstAttribute::ATTRIBUTE_KEY_ORDER => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_ORDER,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],
        );
        // Select attribute config
        $result = array_filter(
            parent::getTabConfig(),
            function(array $tabAttrConfig) use ($tabUpdateConfig) {
                $strAttrKey = $tabAttrConfig[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY];

                return array_key_exists(
                    $strAttrKey,
                    $tabUpdateConfig
                );
            }
        );
        // Format attribute config
        $result = array_map(
            function(array $tabAttrConfig) use ($tabUpdateConfig) {
                $strAttrKey = $tabAttrConfig[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY];
                $tabUpdateAttrConfig = $tabUpdateConfig[$strAttrKey];

                if(array_key_exists(ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE, $tabAttrConfig))
                {
                    unset($tabAttrConfig[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE]);
                }

                return array_merge(
                    $tabAttrConfig,
                    $tabUpdateAttrConfig
                );
            },
            $result
        );
        // Add attribute config
        $result = array_merge(
            $result,
            array(
                // Attribute schema (id|entity)
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttr::ATTRIBUTE_KEY_SCHEMA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttr::ATTRIBUTE_ALIAS_SCHEMA,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_SCHEMA,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init var
        $tabRuleConfig = parent::getTabRuleConfig();
        $result = array_merge(
            $tabRuleConfig,
            array(
                ConstAttribute::ATTRIBUTE_KEY_ID => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => [
                                    'is_null',
                                    [
                                        'callable',
                                        [
                                            'valid_callable' => function() {return $this->checkIsNew();}
                                        ]
                                    ]
                                ],
                                'is-valid-id' => [
                                    [
                                        'type_numeric',
                                        ['integer_only_require' => true]
                                    ],
                                    [
                                        'compare_greater',
                                        [
                                            'compare_value' => 0,
                                            'equal_enable_require' => false
                                        ]
                                    ],
                                    [
                                        'callable',
                                        [
                                            'valid_callable' => function() {return (!$this->checkIsNew());}
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_DT_CREATE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-date' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_DT_CREATE]
                            ],
                            'error_message_pattern' => '%1$s must be null or a date.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-date' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE]
                            ],
                            'error_message_pattern' => '%1$s must be null or a date.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_NAME => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_NAME]
                            ],
                            'error_message_pattern' => '%1$s must be null or a string not empty.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_DATA_TYPE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_DATA_TYPE]
                            ],
                            'error_message_pattern' => '%1$s must be null or a valid string.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED]
                            ],
                            'error_message_pattern' => '%1$s must be null or a boolean.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE]
                            ],
                            'error_message_pattern' => '%1$s must be null or an empty array or an array of null, string, numeric or boolean values.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG]
                            ],
                            'error_message_pattern' => '%1$s must be a valid rule configuration array.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_ORDER => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_ORDER]
                            ],
                            'error_message_pattern' => '%1$s must be null or a positive integer.'
                        ]
                    ]
                ],
                ConstSchemaAttr::ATTRIBUTE_KEY_SCHEMA => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-id' => [
                                    [
                                        'type_numeric',
                                        ['integer_only_require' => true]
                                    ],
                                    [
                                        'compare_greater',
                                        [
                                            'compare_value' => 0,
                                            'equal_enable_require' => false
                                        ]
                                    ]
                                ],
                                'is-valid-entity' => [
                                    [
                                        'type_object',
                                        [
                                            'class_path' => [SchemaEntity::class]
                                        ]
                                    ],
                                    [
                                        'validation_entity',
                                        [
                                            'attribute_key' => ConstSchema::ATTRIBUTE_KEY_ID
                                        ]
                                    ],
                                    [
                                        'new_entity',
                                        [
                                            'new_require' => false
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be a valid schema entity or ID.'
                        ]
                    ]
                ]
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstAttribute::ATTRIBUTE_KEY_ID:
            case ConstAttribute::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE:
            case ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED:
            case ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG:
            case ConstAttribute::ATTRIBUTE_KEY_ORDER:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        parent::getAttributeValueFormatSet($strKey, $value)
                );
                break;

            case ConstAttribute::ATTRIBUTE_KEY_NAME:
            case ConstAttribute::ATTRIBUTE_KEY_DATA_TYPE:
            case ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstSchemaAttr::ATTRIBUTE_KEY_SCHEMA:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstAttribute::ATTRIBUTE_KEY_ID:
            case ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED:
            case ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE :
            case ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG :
            case ConstAttribute::ATTRIBUTE_KEY_ORDER:
                $result = $value;
                break;

            case ConstAttribute::ATTRIBUTE_KEY_DEFAULT_VALUE:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatGet($value);
                break;

            case ConstSchemaAttr::ATTRIBUTE_KEY_SCHEMA:
                $result = array(
                    ConstSchema::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof SchemaEntity) ?
                            $value->getAttributeValueSave(ConstSchema::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        $objSchemaEntityFactory = $this->objSchemaEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAttribute::ATTRIBUTE_KEY_ID:
            case ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED:
            case ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE :
            case ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG :
            case ConstAttribute::ATTRIBUTE_KEY_ORDER:
                $result = $value;
                break;

            case ConstAttribute::ATTRIBUTE_KEY_DEFAULT_VALUE:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatSet($value);
                break;

            case ConstSchemaAttr::ATTRIBUTE_KEY_SCHEMA:
                $result = $value;
                if((!is_null($objSchemaEntityFactory)) && is_array($value))
                {
                    $objSchemaEntity = $objSchemaEntityFactory->getObjEntity(
                        array(),
                        // Try to select schema entity, by id, if required
                        (
                            (!is_null($this->tabSchemaEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabSchemaEntityFactoryExecConfig
                                        ) :
                                        $this->tabSchemaEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objSchemaEntity->hydrateSave($value))
                    {
                        $objSchemaEntity->setIsNew(false);
                        $result = $objSchemaEntity;
                    }
                }
                else if(isset($value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrAttributeKey()
    {
        // Init var
        $strAttributeName = $this->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_NAME);
        $result = sprintf(
            ConstAttribute::CONF_ATTRIBUTE_KEY_PATTERN,
            ToolBoxSchemaAttr::getStrAttributeKey($strAttributeName)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabEntityAttrConfig()
    {
        // Init var
        $result = array_merge(
            parent::getTabEntityAttrConfig(),
            array(
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    $this->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_NAME),
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
            )
        );
        unset($result[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE]);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabEntityAttrRuleConfig()
    {
        // Return result
        return array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-value' => parent::getTabEntityAttrRuleConfig()
                    ],
                    'error_message_pattern' => '%1$s must be null or a valid value.'
                ]
            ]
        );
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueSaveFormatGet($value)
    {
        // Return result
        return ToolBoxNullValue::getAttributeValueSaveFormatGet(
            parent::getEntityAttrValueSaveFormatGet($value)
        );
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueSaveFormatSet($value)
    {
        // Return result
        return parent::getEntityAttrValueSaveFormatSet(
            ToolBoxNullValue::getAttributeValueSaveFormatSet($value)
        );
    }



}