<?php
/**
 * This class allows to define schema attribute entity collection class.
 * Schema attribute entity collection is a save attribute entity collection,
 * used to store schema attribute entities.
 * key => schema attribute entity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\attribute\model;

use liberty_code\attribute_model\attribute\repository\model\SaveAttributeEntityCollection;

use people_sdk\item\schema\attribute\model\SchemaAttrEntity;



/**
 * @method null|SchemaAttrEntity getItem(string $strKey) @inheritdoc
 * @method null|SchemaAttrEntity getObjAttribute(string $strName) @inheritdoc
 * @method string setItem(SchemaAttrEntity $objEntity) @inheritdoc
 * @method string setAttribute(SchemaAttrEntity $objAttribute) @inheritdoc
 * @method SchemaAttrEntity removeItem($strKey) @inheritdoc
 * @method SchemaAttrEntity removeAttribute(string $strName) @inheritdoc
 */
class SchemaAttrEntityCollection extends SaveAttributeEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return SchemaAttrEntity::class;
    }



}