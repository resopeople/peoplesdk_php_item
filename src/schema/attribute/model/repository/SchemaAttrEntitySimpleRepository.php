<?php
/**
 * This class allows to define schema attribute entity simple repository class.
 * Schema attribute entity simple repository is simple repository,
 * allows to prepare data from schema attribute entity, to save in requisition persistence.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\attribute\model\repository;

use people_sdk\library\model\repository\simple\model\SimpleRepository;

use liberty_code\model\repository\library\ConstRepository;
use people_sdk\item\schema\attribute\model\SchemaAttrEntity;



class SchemaAttrEntitySimpleRepository extends SimpleRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Init var
        $result = array(
            ConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => SchemaAttrEntity::class,
            ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE => false
        );

        // Return result
        return $result;
    }



}


