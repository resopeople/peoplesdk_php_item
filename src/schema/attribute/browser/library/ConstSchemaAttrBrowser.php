<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\attribute\browser\library;



class ConstSchemaAttrBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_LIKE_NAME = 'strAttrCritLikeName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_NAME = 'strAttrCritEqualName';
    const ATTRIBUTE_KEY_CRIT_IN_NAME = 'tabAttrCritInName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_DATA_TYPE = 'strAttrCritEqualDataType';
    const ATTRIBUTE_KEY_CRIT_IN_DATA_TYPE = 'tabAttrCritInDataType';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ID = 'intAttrCritEqualSchemaId';
    const ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ID = 'tabAttrCritInSchemaId';
    const ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_NAME = 'strAttrCritLikeSchemaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_NAME = 'strAttrCritEqualSchemaName';
    const ATTRIBUTE_KEY_CRIT_IN_SCHEMA_NAME = 'tabAttrCritInSchemaName';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_NAME = 'strAttrSortName';
    const ATTRIBUTE_KEY_SORT_ORDER = 'strAttrSortOrder';
    const ATTRIBUTE_KEY_SORT_SCHEMA_ID = 'strAttrSortSchemaId';
    const ATTRIBUTE_KEY_SORT_SCHEMA_NAME = 'strAttrSortSchemaName';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_NAME = 'crit-like-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME = 'crit-equal-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_NAME = 'crit-in-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_DATA_TYPE = 'crit-equal-data-type';
    const ATTRIBUTE_ALIAS_CRIT_IN_DATA_TYPE = 'crit-in-data-type';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_ID = 'crit-equal-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_ID = 'crit-in-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_SCHEMA_NAME = 'crit-like-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_NAME = 'crit-equal-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_NAME = 'crit-in-schema-name';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_NAME = 'sort-name';
    const ATTRIBUTE_ALIAS_SORT_ORDER = 'sort-order';
    const ATTRIBUTE_ALIAS_SORT_SCHEMA_ID = 'sort-schema-id';
    const ATTRIBUTE_ALIAS_SORT_SCHEMA_NAME = 'sort-schema-name';



}