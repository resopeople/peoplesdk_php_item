<?php
/**
 * This class allows to define schema attribute browser entity class.
 * Schema attribute browser entity allows to define attributes,
 * to search schema attribute entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\attribute\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\item\schema\attribute\browser\library\ConstSchemaAttrBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|string $strAttrCritLikeName
 * @property null|string $strAttrCritEqualName
 * @property null|string[] $tabAttrCritInName
 * @property null|string $strAttrCritEqualDataType
 * @property null|string[] $tabAttrCritInDataType
 * @property null|integer $intAttrCritEqualSchemaId
 * @property null|integer[] $tabAttrCritInSchemaId
 * @property null|string $strAttrCritLikeSchemaName
 * @property null|string $strAttrCritEqualSchemaName
 * @property null|string[] $tabAttrCritInSchemaName
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortName
 * @property null|string $strAttrSortOrder
 * @property null|string $strAttrSortSchemaId
 * @property null|string $strAttrSortSchemaName
 */
class SchemaAttrBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal data type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_DATA_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_DATA_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in data type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_DATA_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_IN_DATA_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort order
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_ORDER,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_SORT_ORDER,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_SORT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstSchemaAttrBrowser::ATTRIBUTE_ALIAS_SORT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigValidDate,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigValidDate,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigValidDate,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigValidDate,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME => $tabRuleConfigValidString,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME => $tabRuleConfigValidString,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME => $tabRuleConfigValidTabString,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_DATA_TYPE => $tabRuleConfigValidString,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_DATA_TYPE => $tabRuleConfigValidTabString,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ID => $tabRuleConfigValidId,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ID => $tabRuleConfigValidTabId,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_NAME => $tabRuleConfigValidTabString,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigValidSort,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigValidSort,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_NAME => $tabRuleConfigValidSort,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_ORDER => $tabRuleConfigValidSort,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_ID => $tabRuleConfigValidSort,
                ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_NAME => $tabRuleConfigValidSort
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_string($value) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ($value instanceof DateTime) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_DATA_TYPE:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_NAME:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_NAME:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_NAME:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_ORDER:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_ID:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_DATA_TYPE:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstSchemaAttrBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = $this->getAttributeValueFormatGet($strKey, $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}