<?php
/**
 * Description :
 * This class allows to define schema attribute provider class.
 * Schema attribute provider is standard attribute provider,
 * uses schema attribute entity collection,
 * to provide attribute information, for item entity.
 *
 * Schema attribute provider uses the following specified configuration:
 * [
 *     Standard attribute provider configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\schema\attribute\provider\model;

use liberty_code\handle_model\attribute\provider\standard\model\StandardAttrProvider;

use liberty_code\cache\repository\api\RepositoryInterface;
use people_sdk\item\schema\attribute\model\SchemaAttrEntityCollection;



/**
 * @method SchemaAttrEntityCollection getObjAttributeCollection() @inheritdoc
 * @method void setObjAttributeCollection(SchemaAttrEntityCollection $objAttributeCollection) @inheritdoc
 */
class SchemaAttrProvider extends StandardAttrProvider
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SchemaAttrEntityCollection $objAttributeCollection
     */
    public function __construct(
        SchemaAttrEntityCollection $objAttributeCollection,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objAttributeCollection,
            $tabConfig,
            $objCacheRepo
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     *
     * @return string
     */
    protected function getStrFixAttributeCollectionClassPath()
    {
        // Return result
        return SchemaAttrEntityCollection::class;
    }



}