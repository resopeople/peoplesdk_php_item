<?php
/**
 * This class allows to define area member entity collection class.
 * key => AreaMemberEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\member\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\item\area\member\model\AreaMemberEntity;



/**
 * @method null|AreaMemberEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(AreaMemberEntity $objEntity) @inheritdoc
 */
class AreaMemberEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return AreaMemberEntity::class;
    }



}