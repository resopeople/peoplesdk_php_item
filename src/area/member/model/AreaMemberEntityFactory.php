<?php
/**
 * This class allows to define area member entity factory class.
 * Area member entity factory allows to provide new area member entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\member\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\member\library\ConstAreaMember;
use people_sdk\item\area\member\exception\UserProfileEntityFactoryInvalidFormatException;
use people_sdk\item\area\member\exception\AreaEntityFactoryInvalidFormatException;
use people_sdk\item\area\member\exception\UserProfileEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\area\member\exception\AreaEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\area\member\model\AreaMemberEntity;



/**
 * @method null|UserProfileEntityFactory getObjUserProfileEntityFactory() Get user profile entity factory object.
 * @method null|AreaEntityFactory getObjAreaEntityFactory() Get area entity factory object.
 * @method null|array getTabUserProfileEntityFactoryExecConfig() Get user profile entity factory execution configuration array.
 * @method null|array getTabAreaEntityFactoryExecConfig() Get area entity factory execution configuration array.
 * @method AreaMemberEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjUserProfileEntityFactory(null|UserProfileEntityFactory $objUserProfileEntityFactory) Set user profile entity factory object.
 * @method void setObjAreaEntityFactory(null|AreaEntityFactory $objAreaEntityFactory) Set area entity factory object.
 * @method void setTabUserProfileEntityFactoryExecConfig(null|array $tabUserProfileEntityFactoryExecConfig) Set user profile entity factory execution configuration array.
 * @method void setTabAreaEntityFactoryExecConfig(null|array $tabAreaEntityFactoryExecConfig) Set area entity factory execution configuration array.
 */
class AreaMemberEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|UserProfileEntityFactory $objUserProfileEntityFactory = null
     * @param null|AreaEntityFactory $objAreaEntityFactory = null
     * @param null|array $tabUserProfileEntityFactoryExecConfig = null
     * @param null|array $tabAreaEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        UserProfileEntityFactory $objUserProfileEntityFactory = null,
        AreaEntityFactory $objAreaEntityFactory = null,
        array $tabUserProfileEntityFactoryExecConfig = null,
        array $tabAreaEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init user profile entity factory
        $this->setObjUserProfileEntityFactory($objUserProfileEntityFactory);

        // Init area entity factory
        $this->setObjAreaEntityFactory($objAreaEntityFactory);

        // Init user profile entity factory execution config
        $this->setTabUserProfileEntityFactoryExecConfig($tabUserProfileEntityFactoryExecConfig);

        // Init area entity factory execution config
        $this->setTabAreaEntityFactoryExecConfig($tabAreaEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAreaMember::DATA_KEY_USER_PROFILE_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstAreaMember::DATA_KEY_USER_PROFILE_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstAreaMember::DATA_KEY_AREA_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstAreaMember::DATA_KEY_AREA_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstAreaMember::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstAreaMember::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstAreaMember::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstAreaMember::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAreaMember::DATA_KEY_USER_PROFILE_ENTITY_FACTORY,
            ConstAreaMember::DATA_KEY_AREA_ENTITY_FACTORY,
            ConstAreaMember::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG,
            ConstAreaMember::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAreaMember::DATA_KEY_USER_PROFILE_ENTITY_FACTORY:
                    UserProfileEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaMember::DATA_KEY_AREA_ENTITY_FACTORY:
                    AreaEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaMember::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG:
                    UserProfileEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaMember::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG:
                    AreaEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AreaMemberEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstAreaMember::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new AreaMemberEntity(
            $objDateTimeFactory,
            array(),
            $objValidator,
            $this->getObjUserProfileEntityFactory(),
            $this->getObjAreaEntityFactory(),
            $this->getTabUserProfileEntityFactoryExecConfig(),
            $this->getTabAreaEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}