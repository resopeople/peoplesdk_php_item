<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\library;



class ConstArea
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_SCHEMA_ENTITY_FACTORY = 'objSchemaEntityFactory';
    const DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG = 'tabSchemaEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_NAME = 'strAttrName';
    const ATTRIBUTE_KEY_SCHEMA = 'attrSchema';
    const ATTRIBUTE_KEY_ITEM_GET_ENABLE_REQUIRED = 'boolAttrItemGetEnableRequired';
    const ATTRIBUTE_KEY_ITEM_CREATE_ENABLE_REQUIRED = 'boolAttrItemCreateEnableRequired';
    const ATTRIBUTE_KEY_ITEM_UPDATE_ENABLE_REQUIRED = 'boolAttrItemUpdateEnableRequired';
    const ATTRIBUTE_KEY_ITEM_DELETE_ENABLE_REQUIRED = 'boolAttrItemDeleteEnableRequired';
    const ATTRIBUTE_KEY_ITEM_PROFILE_GET_ENABLE_REQUIRED = 'boolAttrItemProfileGetEnableRequired';
    const ATTRIBUTE_KEY_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED = 'boolAttrItemProfileUpdateEnableRequired';
    const ATTRIBUTE_KEY_ITEM_PROFILE_DELETE_ENABLE_REQUIRED = 'boolAttrItemProfileDeleteEnableRequired';
    const ATTRIBUTE_KEY_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED = 'boolAttrItemNoCategoryAccessEnableRequired';
    const ATTRIBUTE_KEY_ITEM_RELATION_GET_ENABLE_REQUIRED = 'boolAttrItemRelationGetEnableRequired';
    const ATTRIBUTE_KEY_ITEM_RELATION_CREATE_ENABLE_REQUIRED = 'boolAttrItemRelationCreateEnableRequired';
    const ATTRIBUTE_KEY_ITEM_RELATION_UPDATE_ENABLE_REQUIRED = 'boolAttrItemRelationUpdateEnableRequired';
    const ATTRIBUTE_KEY_ITEM_RELATION_DELETE_ENABLE_REQUIRED = 'boolAttrItemRelationDeleteEnableRequired';
    const ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED = 'boolAttrItemProfileRelationGetEnableRequired';
    const ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED = 'boolAttrItemProfileRelationCreateEnableRequired';
    const ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED = 'boolAttrItemProfileRelationUpdateEnableRequired';
    const ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED = 'boolAttrItemProfileRelationDeleteEnableRequired';
    const ATTRIBUTE_KEY_ITEM_DEFAULT_CATEGORY = 'tabAttrItemDefaultCategory';
    const ATTRIBUTE_KEY_ITEM_CATEGORY_GET_ENABLE_REQUIRED = 'boolAttrItemCategoryGetEnableRequired';
    const ATTRIBUTE_KEY_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED = 'boolAttrItemCategoryCreateEnableRequired';
    const ATTRIBUTE_KEY_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED = 'boolAttrItemCategoryUpdateEnableRequired';
    const ATTRIBUTE_KEY_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED = 'boolAttrItemCategoryDeleteEnableRequired';
    const ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED = 'boolAttrItemProfileCategoryGetEnableRequired';
    const ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED = 'boolAttrItemProfileCategoryCreateEnableRequired';
    const ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED = 'boolAttrItemProfileCategoryUpdateEnableRequired';
    const ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED = 'boolAttrItemProfileCategoryDeleteEnableRequired';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_NAME = 'name';
    const ATTRIBUTE_ALIAS_SCHEMA = 'schema';
    const ATTRIBUTE_ALIAS_ITEM_GET_ENABLE_REQUIRED = 'item-get-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_CREATE_ENABLE_REQUIRED = 'item-create-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_UPDATE_ENABLE_REQUIRED = 'item-update-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_DELETE_ENABLE_REQUIRED = 'item-delete-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_PROFILE_GET_ENABLE_REQUIRED = 'item-profile-get-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED = 'item-profile-update-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_PROFILE_DELETE_ENABLE_REQUIRED = 'item-profile-delete-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED = 'item-no-category-access-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_RELATION_GET_ENABLE_REQUIRED = 'item-relation-get-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_RELATION_CREATE_ENABLE_REQUIRED = 'item-relation-create-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_RELATION_UPDATE_ENABLE_REQUIRED = 'item-relation-update-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_RELATION_DELETE_ENABLE_REQUIRED = 'item-relation-delete-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED = 'item-profile-relation-get-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED = 'item-profile-relation-create-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED = 'item-profile-relation-update-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED = 'item-profile-relation-delete-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_DEFAULT_CATEGORY = 'item-default-category';
    const ATTRIBUTE_ALIAS_ITEM_CATEGORY_GET_ENABLE_REQUIRED = 'item-category-get-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED = 'item-category-create-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED = 'item-category-update-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED = 'item-category-delete-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED = 'item-profile-category-get-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED = 'item-profile-category-create-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED = 'item-profile-category-update-enable-required';
    const ATTRIBUTE_ALIAS_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED = 'item-profile-category-delete-enable-required';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_NAME = 'name';
    const ATTRIBUTE_NAME_SAVE_SCHEMA = 'schema';
    const ATTRIBUTE_NAME_SAVE_ITEM_GET_ENABLE_REQUIRED = 'item-get-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_CREATE_ENABLE_REQUIRED = 'item-create-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_UPDATE_ENABLE_REQUIRED = 'item-update-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_DELETE_ENABLE_REQUIRED = 'item-delete-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_GET_ENABLE_REQUIRED = 'item-profile-get-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED = 'item-profile-update-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_DELETE_ENABLE_REQUIRED = 'item-profile-delete-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED = 'item-no-category-access-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_RELATION_GET_ENABLE_REQUIRED = 'item-relation-get-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_RELATION_CREATE_ENABLE_REQUIRED = 'item-relation-create-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_RELATION_UPDATE_ENABLE_REQUIRED = 'item-relation-update-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_RELATION_DELETE_ENABLE_REQUIRED = 'item-relation-delete-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED = 'item-profile-relation-get-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED = 'item-profile-relation-create-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED = 'item-profile-relation-update-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED = 'item-profile-relation-delete-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_DEFAULT_CATEGORY = 'item-default-category';
    const ATTRIBUTE_NAME_SAVE_ITEM_CATEGORY_GET_ENABLE_REQUIRED = 'item-category-get-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED = 'item-category-create-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED = 'item-category-update-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED = 'item-category-delete-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED = 'item-profile-category-get-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED = 'item-profile-category-create-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED = 'item-profile-category-update-enable-required';
    const ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED = 'item-profile-category-delete-enable-required';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_PROFILE = 'profile';



    // Exception message constants
    const EXCEPT_MSG_SCHEMA_ENTITY_FACTORY_INVALID_FORMAT =
        'Following schema entity factory "%1$s" invalid! It must be a schema entity factory object.';
    const EXCEPT_MSG_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the schema entity factory execution configuration standard.';



}