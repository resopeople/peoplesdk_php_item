<?php
/**
 * This class allows to define area category scope entity class.
 * Area category scope entity allows to define a specific category scope,
 * which represents specific category, available on specific area.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\category\scope\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\item\category\library\ConstCategory;
use people_sdk\item\category\model\CategoryEntity;
use people_sdk\item\category\model\CategoryEntityFactory;
use people_sdk\item\area\library\ConstArea;
use people_sdk\item\area\model\AreaEntity;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\category\scope\library\ConstAreaCategoryScope;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|integer|AreaEntity $attrArea : area id|entity
 * @property null|integer|CategoryEntity $attrCategory : category id|entity
 */
class AreaCategoryScopeEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: Category entity factory instance.
     * @var null|CategoryEntityFactory
     */
    protected $objCategoryEntityFactory;



    /**
     * DI: Area entity factory instance.
     * @var null|AreaEntityFactory
     */
    protected $objAreaEntityFactory;



    /**
     * DI: Category entity factory execution configuration.
     * @var null|array
     */
    protected $tabCategoryEntityFactoryExecConfig;



    /**
     * DI: Area entity factory execution configuration.
     * @var null|array
     */
    protected $tabAreaEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     * @param null|CategoryEntityFactory $objCategoryEntityFactory = null
     * @param null|AreaEntityFactory $objAreaEntityFactory = null
     * @param null|array $tabCategoryEntityFactoryExecConfig = null
     * @param null|array $tabAreaEntityFactoryExecConfig = null
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        CategoryEntityFactory $objCategoryEntityFactory = null,
        AreaEntityFactory $objAreaEntityFactory = null,
        array $tabCategoryEntityFactoryExecConfig = null,
        array $tabAreaEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objCategoryEntityFactory = $objCategoryEntityFactory;
        $this->objAreaEntityFactory = $objAreaEntityFactory;
        $this->tabCategoryEntityFactoryExecConfig = $tabCategoryEntityFactoryExecConfig;
        $this->tabAreaEntityFactoryExecConfig = $tabAreaEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
    * @inheritdoc
    */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstAreaCategoryScope::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaCategoryScope::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaCategoryScope::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaCategoryScope::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaCategoryScope::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaCategoryScope::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaCategoryScope::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaCategoryScope::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaCategoryScope::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaCategoryScope::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute area
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaCategoryScope::ATTRIBUTE_KEY_AREA,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaCategoryScope::ATTRIBUTE_ALIAS_AREA,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaCategoryScope::ATTRIBUTE_NAME_SAVE_AREA,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute category
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaCategoryScope::ATTRIBUTE_KEY_CATEGORY,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaCategoryScope::ATTRIBUTE_ALIAS_CATEGORY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaCategoryScope::ATTRIBUTE_NAME_SAVE_CATEGORY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidArea = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-id' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ],
                        'is-valid-entity' => [
                            [
                                'type_object',
                                [
                                    'class_path' => [AreaEntity::class]
                                ]
                            ],
                            [
                                'validation_entity',
                                [
                                    'attribute_key' => ConstArea::ATTRIBUTE_KEY_ID
                                ]
                            ],
                            [
                                'new_entity',
                                [
                                    'new_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be a valid area entity or ID.'
                ]
            ]
        );
        $tabRuleConfigValidCategory = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-id' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ],
                        'is-valid-entity' => [
                            [
                                'type_object',
                                [
                                    'class_path' => [CategoryEntity::class]
                                ]
                            ],
                            [
                                'validation_entity',
                                [
                                    'attribute_key' => ConstCategory::ATTRIBUTE_KEY_ID
                                ]
                            ],
                            [
                                'new_entity',
                                [
                                    'new_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be a valid category entity or ID.'
                ]
            ]
        );

        // Init var
        $result = array(
            ConstAreaCategoryScope::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstAreaCategoryScope::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstAreaCategoryScope::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstAreaCategoryScope::ATTRIBUTE_KEY_AREA => $tabRuleConfigValidArea,
            ConstAreaCategoryScope::ATTRIBUTE_KEY_CATEGORY => $tabRuleConfigValidCategory
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaCategoryScope::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAreaCategoryScope::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaCategoryScope::ATTRIBUTE_KEY_ID:
            case ConstAreaCategoryScope::ATTRIBUTE_KEY_AREA:
            case ConstAreaCategoryScope::ATTRIBUTE_KEY_CATEGORY:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstAreaCategoryScope::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAreaCategoryScope::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_string($value) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ($value instanceof DateTime) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaCategoryScope::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAreaCategoryScope::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstAreaCategoryScope::ATTRIBUTE_KEY_AREA:
                $result = array(
                    ConstArea::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof AreaEntity) ?
                            $value->getAttributeValueSave(ConstArea::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            case ConstAreaCategoryScope::ATTRIBUTE_KEY_CATEGORY:
                $result = array(
                    ConstCategory::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof CategoryEntity) ?
                            $value->getAttributeValueSave(ConstCategory::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objCategoryEntityFactory = $this->objCategoryEntityFactory;
        $objAreaEntityFactory = $this->objAreaEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaCategoryScope::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAreaCategoryScope::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    is_string($value) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstAreaCategoryScope::ATTRIBUTE_KEY_AREA:
                $result = $value;
                if((!is_null($objAreaEntityFactory)) && is_array($value))
                {
                    $objAreaEntity = $objAreaEntityFactory->getObjEntity(
                        array(),
                        // Try to select area entity, by id, if required
                        (
                            (!is_null($this->tabAreaEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabAreaEntityFactoryExecConfig
                                        ) :
                                        $this->tabAreaEntityFactoryExecConfig
                                ) :
                                (
                                isset($value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]) ?
                                    array(
                                        ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                            $value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]
                                    ) :
                                    null
                                )
                        )
                    );
                    if($objAreaEntity->hydrateSave($value))
                    {
                        $objAreaEntity->setIsNew(false);
                        $result = $objAreaEntity;
                    }
                }
                else if(isset($value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstArea::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            case ConstAreaCategoryScope::ATTRIBUTE_KEY_CATEGORY:
                $result = $value;
                if((!is_null($objCategoryEntityFactory)) && is_array($value))
                {
                    $objCategoryEntity = $objCategoryEntityFactory->getObjEntity(
                        array(),
                        // Try to select category entity, by id, if required
                        (
                            (!is_null($this->tabCategoryEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstCategory::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstCategory::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabCategoryEntityFactoryExecConfig
                                        ) :
                                        $this->tabCategoryEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstCategory::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstCategory::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objCategoryEntity->hydrateSave($value))
                    {
                        $objCategoryEntity->setIsNew(false);
                        $result = $objCategoryEntity;
                    }
                }
                else if(isset($value[ConstCategory::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstCategory::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}