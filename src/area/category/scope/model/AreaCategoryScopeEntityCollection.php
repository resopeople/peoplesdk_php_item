<?php
/**
 * This class allows to define area category scope entity collection class.
 * key => AreaCategoryScopeEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\category\scope\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\item\area\category\scope\model\AreaCategoryScopeEntity;



/**
 * @method null|AreaCategoryScopeEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(AreaCategoryScopeEntity $objEntity) @inheritdoc
 */
class AreaCategoryScopeEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return AreaCategoryScopeEntity::class;
    }



}