<?php
/**
 * This class allows to define area category scope entity factory class.
 * Area category scope entity factory allows to provide new area category scope entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\category\scope\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\item\category\model\CategoryEntityFactory;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\category\scope\library\ConstAreaCategoryScope;
use people_sdk\item\area\category\scope\exception\CategoryEntityFactoryInvalidFormatException;
use people_sdk\item\area\category\scope\exception\AreaEntityFactoryInvalidFormatException;
use people_sdk\item\area\category\scope\exception\CategoryEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\area\category\scope\exception\AreaEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\area\category\scope\model\AreaCategoryScopeEntity;



/**
 * @method null|CategoryEntityFactory getObjCategoryEntityFactory() Get category entity factory object.
 * @method null|AreaEntityFactory getObjAreaEntityFactory() Get area entity factory object.
 * @method null|array getTabCategoryEntityFactoryExecConfig() Get category entity factory execution configuration array.
 * @method null|array getTabAreaEntityFactoryExecConfig() Get area entity factory execution configuration array.
 * @method AreaCategoryScopeEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjCategoryEntityFactory(null|CategoryEntityFactory $objCategoryEntityFactory) Set category entity factory object.
 * @method void setObjAreaEntityFactory(null|AreaEntityFactory $objAreaEntityFactory) Set area entity factory object.
 * @method void setTabCategoryEntityFactoryExecConfig(null|array $tabCategoryEntityFactoryExecConfig) Set category entity factory execution configuration array.
 * @method void setTabAreaEntityFactoryExecConfig(null|array $tabAreaEntityFactoryExecConfig) Set area entity factory execution configuration array.
 */
class AreaCategoryScopeEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|CategoryEntityFactory $objCategoryEntityFactory = null
     * @param null|AreaEntityFactory $objAreaEntityFactory = null
     * @param null|array $tabCategoryEntityFactoryExecConfig = null
     * @param null|array $tabAreaEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        CategoryEntityFactory $objCategoryEntityFactory = null,
        AreaEntityFactory $objAreaEntityFactory = null,
        array $tabCategoryEntityFactoryExecConfig = null,
        array $tabAreaEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init category entity factory
        $this->setObjCategoryEntityFactory($objCategoryEntityFactory);

        // Init area entity factory
        $this->setObjAreaEntityFactory($objAreaEntityFactory);

        // Init category entity factory execution config
        $this->setTabCategoryEntityFactoryExecConfig($tabCategoryEntityFactoryExecConfig);

        // Init area entity factory execution config
        $this->setTabAreaEntityFactoryExecConfig($tabAreaEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAreaCategoryScope::DATA_KEY_CATEGORY_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstAreaCategoryScope::DATA_KEY_CATEGORY_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstAreaCategoryScope::DATA_KEY_AREA_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstAreaCategoryScope::DATA_KEY_AREA_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstAreaCategoryScope::DATA_KEY_CATEGORY_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstAreaCategoryScope::DATA_KEY_CATEGORY_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstAreaCategoryScope::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstAreaCategoryScope::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAreaCategoryScope::DATA_KEY_CATEGORY_ENTITY_FACTORY,
            ConstAreaCategoryScope::DATA_KEY_AREA_ENTITY_FACTORY,
            ConstAreaCategoryScope::DATA_KEY_CATEGORY_ENTITY_FACTORY_EXEC_CONFIG,
            ConstAreaCategoryScope::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAreaCategoryScope::DATA_KEY_CATEGORY_ENTITY_FACTORY:
                    CategoryEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaCategoryScope::DATA_KEY_AREA_ENTITY_FACTORY:
                    AreaEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaCategoryScope::DATA_KEY_CATEGORY_ENTITY_FACTORY_EXEC_CONFIG:
                    CategoryEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaCategoryScope::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG:
                    AreaEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AreaCategoryScopeEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstAreaCategoryScope::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new AreaCategoryScopeEntity(
            $objDateTimeFactory,
            array(),
            $objValidator,
            $this->getObjCategoryEntityFactory(),
            $this->getObjAreaEntityFactory(),
            $this->getTabCategoryEntityFactoryExecConfig(),
            $this->getTabAreaEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}