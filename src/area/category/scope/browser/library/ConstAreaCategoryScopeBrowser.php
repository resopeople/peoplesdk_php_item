<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\category\scope\browser\library;



class ConstAreaCategoryScopeBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_EQUAL_AREA_ID = 'intAttrCritEqualAreaId';
    const ATTRIBUTE_KEY_CRIT_IN_AREA_ID = 'tabAttrCritInAreaId';
    const ATTRIBUTE_KEY_CRIT_LIKE_AREA_NAME = 'strAttrCritLikeAreaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_AREA_NAME = 'strAttrCritEqualAreaName';
    const ATTRIBUTE_KEY_CRIT_IN_AREA_NAME = 'tabAttrCritInAreaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_ID = 'intAttrCritEqualCategoryId';
    const ATTRIBUTE_KEY_CRIT_IN_CATEGORY_ID = 'tabAttrCritInCategoryId';
    const ATTRIBUTE_KEY_CRIT_LIKE_CATEGORY_NAME = 'strAttrCritLikeCategoryName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_NAME = 'strAttrCritEqualCategoryName';
    const ATTRIBUTE_KEY_CRIT_IN_CATEGORY_NAME = 'tabAttrCritInCategoryName';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_AREA_ID = 'strAttrSortAreaId';
    const ATTRIBUTE_KEY_SORT_AREA_NAME = 'strAttrSortAreaName';
    const ATTRIBUTE_KEY_SORT_CATEGORY_ID = 'strAttrSortCategoryId';
    const ATTRIBUTE_KEY_SORT_CATEGORY_NAME = 'strAttrSortCategoryName';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_ID = 'crit-equal-area-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_AREA_ID = 'crit-in-area-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_AREA_NAME = 'crit-like-area-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_NAME = 'crit-equal-area-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_AREA_NAME = 'crit-in-area-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_CATEGORY_ID = 'crit-equal-category-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_CATEGORY_ID = 'crit-in-category-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_CATEGORY_NAME = 'crit-like-category-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_CATEGORY_NAME = 'crit-equal-category-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_CATEGORY_NAME = 'crit-in-category-name';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_AREA_ID = 'sort-area-id';
    const ATTRIBUTE_ALIAS_SORT_AREA_NAME = 'sort-area-name';
    const ATTRIBUTE_ALIAS_SORT_CATEGORY_ID = 'sort-category-id';
    const ATTRIBUTE_ALIAS_SORT_CATEGORY_NAME = 'sort-category-name';



}