<?php
/**
 * This class allows to define area browser entity class.
 * Area browser entity allows to define attributes,
 * to search area entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\item\area\browser\library\ConstAreaBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|string $strAttrCritLikeName
 * @property null|string $strAttrCritEqualName
 * @property null|string[] $tabAttrCritInName
 * @property null|boolean $boolAttrCritIsItemGetEnableRequired
 * @property null|boolean $boolAttrCritIsItemCreateEnableRequired
 * @property null|boolean $boolAttrCritIsItemUpdateEnableRequired
 * @property null|boolean $boolAttrCritIsItemDeleteEnableRequired
 * @property null|boolean $boolAttrCritIsItemProfileGetEnableRequired
 * @property null|boolean $boolAttrCritIsItemProfileUpdateEnableRequired
 * @property null|boolean $boolAttrCritIsItemProfileDeleteEnableRequired
 * @property null|boolean $boolAttrCritIsItemNoCategoryAccessEnableRequired
 * @property null|boolean $boolAttrCritIsItemRelationGetEnableRequired
 * @property null|boolean $boolAttrCritIsItemRelationCreateEnableRequired
 * @property null|boolean $boolAttrCritIsItemRelationUpdateEnableRequired
 * @property null|boolean $boolAttrCritIsItemRelationDeleteEnableRequired
 * @property null|boolean $boolAttrCritIsItemProfileRelationGetEnableRequired
 * @property null|boolean $boolAttrCritIsItemProfileRelationCreateEnableRequired
 * @property null|boolean $boolAttrCritIsItemProfileRelationUpdateEnableRequired
 * @property null|boolean $boolAttrCritIsItemProfileRelationDeleteEnableRequired
 * @property null|string $strAttrCritLikeItemDefaultCategory
 * @property null|string $strAttrCritEqualItemDefaultCategory
 * @property null|string[] $tabAttrCritInItemDefaultCategory
 * @property null|string[] $tabAttrCritInAllItemDefaultCategory
 * @property null|boolean $boolAttrCritIsItemCategoryGetEnableRequired
 * @property null|boolean $boolAttrCritIsItemCategoryCreateEnableRequired
 * @property null|boolean $boolAttrCritIsItemCategoryUpdateEnableRequired
 * @property null|boolean $boolAttrCritIsItemCategoryDeleteEnableRequired
 * @property null|boolean $boolAttrCritIsItemProfileCategoryGetEnableRequired
 * @property null|boolean $boolAttrCritIsItemProfileCategoryCreateEnableRequired
 * @property null|boolean $boolAttrCritIsItemProfileCategoryUpdateEnableRequired
 * @property null|boolean $boolAttrCritIsItemProfileCategoryDeleteEnableRequired
 * @property null|integer $intAttrCritEqualSchemaId
 * @property null|integer[] $tabAttrCritInSchemaId
 * @property null|string $strAttrCritLikeSchemaName
 * @property null|string $strAttrCritEqualSchemaName
 * @property null|string[] $tabAttrCritInSchemaName
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortName
 * @property null|string $strAttrSortSchemaId
 * @property null|string $strAttrSortSchemaName
 */
class AreaBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item get enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item create enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item update enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item delete enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile item get enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile item update enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile item delete enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item with no category access enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item relation get enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_RELATION_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item relation create enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_RELATION_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item relation update enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_RELATION_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item relation delete enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_RELATION_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile item relation get enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile item relation create enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile item relation update enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile item relation delete enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like item default category
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_LIKE_ITEM_DEFAULT_CATEGORY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_ITEM_DEFAULT_CATEGORY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal item default category
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_DEFAULT_CATEGORY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ITEM_DEFAULT_CATEGORY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in item default category
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_DEFAULT_CATEGORY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ITEM_DEFAULT_CATEGORY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in all item default category
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_ITEM_DEFAULT_CATEGORY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ALL_ITEM_DEFAULT_CATEGORY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item category get enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_CATEGORY_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item category create enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item category update enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item category delete enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile item category get enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile item category create enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile item category update enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile item category delete enable required
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_SORT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaBrowser::ATTRIBUTE_ALIAS_SORT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigValidDate,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigValidDate,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigValidDate,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigValidDate,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME => $tabRuleConfigValidString,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME => $tabRuleConfigValidString,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME => $tabRuleConfigValidTabString,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_LIKE_ITEM_DEFAULT_CATEGORY => $tabRuleConfigValidString,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_DEFAULT_CATEGORY => $tabRuleConfigValidString,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_DEFAULT_CATEGORY => $tabRuleConfigValidTabString,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_ITEM_DEFAULT_CATEGORY => $tabRuleConfigValidTabString,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ID => $tabRuleConfigValidId,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ID => $tabRuleConfigValidTabId,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_NAME => $tabRuleConfigValidTabString,
                ConstAreaBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstAreaBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigValidSort,
                ConstAreaBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigValidSort,
                ConstAreaBrowser::ATTRIBUTE_KEY_SORT_NAME => $tabRuleConfigValidSort,
                ConstAreaBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_ID => $tabRuleConfigValidSort,
                ConstAreaBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_NAME => $tabRuleConfigValidSort
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_string($value) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ($value instanceof DateTime) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_LIKE_ITEM_DEFAULT_CATEGORY:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_DEFAULT_CATEGORY:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_NAME:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_NAME:
            case ConstAreaBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstAreaBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstAreaBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstAreaBrowser::ATTRIBUTE_KEY_SORT_NAME:
            case ConstAreaBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_ID:
            case ConstAreaBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_DEFAULT_CATEGORY:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_ALL_ITEM_DEFAULT_CATEGORY:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_GET_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CREATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_UPDATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_DELETE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_GET_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_DELETE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_GET_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_CREATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_UPDATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_DELETE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_GET_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = $this->getAttributeValueFormatGet($strKey, $value);
                break;

            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_GET_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CREATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_UPDATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_DELETE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_GET_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_DELETE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_GET_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_CREATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_UPDATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_DELETE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_GET_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED:
            case ConstAreaBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED:
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}