<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\browser\library;



class ConstAreaBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_LIKE_NAME = 'strAttrCritLikeName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_NAME = 'strAttrCritEqualName';
    const ATTRIBUTE_KEY_CRIT_IN_NAME = 'tabAttrCritInName';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_GET_ENABLE_REQUIRED = 'boolAttrCritIsItemGetEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_CREATE_ENABLE_REQUIRED = 'boolAttrCritIsItemCreateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_UPDATE_ENABLE_REQUIRED = 'boolAttrCritIsItemUpdateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_DELETE_ENABLE_REQUIRED = 'boolAttrCritIsItemDeleteEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_GET_ENABLE_REQUIRED = 'boolAttrCritIsItemProfileGetEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED = 'boolAttrCritIsItemProfileUpdateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_DELETE_ENABLE_REQUIRED = 'boolAttrCritIsItemProfileDeleteEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED = 'boolAttrCritIsItemNoCategoryAccessEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_GET_ENABLE_REQUIRED = 'boolAttrCritIsItemRelationGetEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_CREATE_ENABLE_REQUIRED = 'boolAttrCritIsItemRelationCreateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_UPDATE_ENABLE_REQUIRED = 'boolAttrCritIsItemRelationUpdateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_RELATION_DELETE_ENABLE_REQUIRED = 'boolAttrCritIsItemRelationDeleteEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED = 'boolAttrCritIsItemProfileRelationGetEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED = 'boolAttrCritIsItemProfileRelationCreateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED = 'boolAttrCritIsItemProfileRelationUpdateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED = 'boolAttrCritIsItemProfileRelationDeleteEnableRequired';
    const ATTRIBUTE_KEY_CRIT_LIKE_ITEM_DEFAULT_CATEGORY = 'strAttrCritLikeItemDefaultCategory';
    const ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_DEFAULT_CATEGORY = 'strAttrCritEqualItemDefaultCategory';
    const ATTRIBUTE_KEY_CRIT_IN_ITEM_DEFAULT_CATEGORY = 'tabAttrCritInItemDefaultCategory';
    const ATTRIBUTE_KEY_CRIT_IN_ALL_ITEM_DEFAULT_CATEGORY = 'tabAttrCritInAllItemDefaultCategory';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_GET_ENABLE_REQUIRED = 'boolAttrCritIsItemCategoryGetEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED = 'boolAttrCritIsItemCategoryCreateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED = 'boolAttrCritIsItemCategoryUpdateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED = 'boolAttrCritIsItemCategoryDeleteEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED = 'boolAttrCritIsItemProfileCategoryGetEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED = 'boolAttrCritIsItemProfileCategoryCreateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED = 'boolAttrCritIsItemProfileCategoryUpdateEnableRequired';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED = 'boolAttrCritIsItemProfileCategoryDeleteEnableRequired';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ID = 'intAttrCritEqualSchemaId';
    const ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ID = 'tabAttrCritInSchemaId';
    const ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_NAME = 'strAttrCritLikeSchemaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_NAME = 'strAttrCritEqualSchemaName';
    const ATTRIBUTE_KEY_CRIT_IN_SCHEMA_NAME = 'tabAttrCritInSchemaName';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_NAME = 'strAttrSortName';
    const ATTRIBUTE_KEY_SORT_SCHEMA_ID = 'strAttrSortSchemaId';
    const ATTRIBUTE_KEY_SORT_SCHEMA_NAME = 'strAttrSortSchemaName';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_NAME = 'crit-like-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME = 'crit-equal-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_NAME = 'crit-in-name';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_GET_ENABLE_REQUIRED = 'crit-is-item-get-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_CREATE_ENABLE_REQUIRED = 'crit-is-item-create-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_UPDATE_ENABLE_REQUIRED = 'crit-is-item-update-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_DELETE_ENABLE_REQUIRED = 'crit-is-item-delete-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_GET_ENABLE_REQUIRED = 'crit-is-item-profile-get-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED = 'crit-is-item-profile-update-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_DELETE_ENABLE_REQUIRED = 'crit-is-item-profile-delete-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED = 'crit-is-item-no-category-access-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_RELATION_GET_ENABLE_REQUIRED = 'crit-is-item-relation-get-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_RELATION_CREATE_ENABLE_REQUIRED = 'crit-is-item-relation-create-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_RELATION_UPDATE_ENABLE_REQUIRED = 'crit-is-item-relation-update-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_RELATION_DELETE_ENABLE_REQUIRED = 'crit-is-item-relation-delete-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED = 'crit-is-item-profile-relation-get-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED = 'crit-is-item-profile-relation-create-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED = 'crit-is-item-profile-relation-update-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED = 'crit-is-item-profile-relation-delete-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_ITEM_DEFAULT_CATEGORY = 'crit-like-item-default-category';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ITEM_DEFAULT_CATEGORY = 'crit-equal-item-default-category';
    const ATTRIBUTE_ALIAS_CRIT_IN_ITEM_DEFAULT_CATEGORY = 'crit-in-item-default-category';
    const ATTRIBUTE_ALIAS_CRIT_IN_ALL_ITEM_DEFAULT_CATEGORY = 'crit-in-all-item-default-category';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_CATEGORY_GET_ENABLE_REQUIRED = 'crit-is-item-category-get-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED = 'crit-is-item-category-create-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED = 'crit-is-item-category-update-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED = 'crit-is-item-category-delete-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED = 'crit-is-item-profile-category-get-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED = 'crit-is-item-profile-category-create-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED = 'crit-is-item-profile-category-update-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED = 'crit-is-item-profile-category-delete-enable-required';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_ID = 'crit-equal-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_ID = 'crit-in-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_SCHEMA_NAME = 'crit-like-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_NAME = 'crit-equal-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_NAME = 'crit-in-schema-name';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_NAME = 'sort-name';
    const ATTRIBUTE_ALIAS_SORT_SCHEMA_ID = 'sort-schema-id';
    const ATTRIBUTE_ALIAS_SORT_SCHEMA_NAME = 'sort-schema-name';



}