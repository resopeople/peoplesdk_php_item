<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\group\member\browser\library;



class ConstAreaGrpMemberBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_EQUAL_AREA_ID = 'intAttrCritEqualAreaId';
    const ATTRIBUTE_KEY_CRIT_IN_AREA_ID = 'tabAttrCritInAreaId';
    const ATTRIBUTE_KEY_CRIT_LIKE_AREA_NAME = 'strAttrCritLikeAreaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_AREA_NAME = 'strAttrCritEqualAreaName';
    const ATTRIBUTE_KEY_CRIT_IN_AREA_NAME = 'tabAttrCritInAreaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_ID = 'intAttrCritEqualGroupId';
    const ATTRIBUTE_KEY_CRIT_IN_GROUP_ID = 'tabAttrCritInGroupId';
    const ATTRIBUTE_KEY_CRIT_LIKE_GROUP_NAME = 'strAttrCritLikeGroupName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_GROUP_NAME = 'strAttrCritEqualGroupName';
    const ATTRIBUTE_KEY_CRIT_IN_GROUP_NAME = 'tabAttrCritInGroupName';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_AREA_ID = 'strAttrSortAreaId';
    const ATTRIBUTE_KEY_SORT_AREA_NAME = 'strAttrSortAreaName';
    const ATTRIBUTE_KEY_SORT_GROUP_ID = 'strAttrSortGroupId';
    const ATTRIBUTE_KEY_SORT_GROUP_NAME = 'strAttrSortGroupName';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_ID = 'crit-equal-area-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_AREA_ID = 'crit-in-area-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_AREA_NAME = 'crit-like-area-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_NAME = 'crit-equal-area-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_AREA_NAME = 'crit-in-area-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_ID = 'crit-equal-group-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_GROUP_ID = 'crit-in-group-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_GROUP_NAME = 'crit-like-group-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_GROUP_NAME = 'crit-equal-group-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_GROUP_NAME = 'crit-in-group-name';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_AREA_ID = 'sort-area-id';
    const ATTRIBUTE_ALIAS_SORT_AREA_NAME = 'sort-area-name';
    const ATTRIBUTE_ALIAS_SORT_GROUP_ID = 'sort-group-id';
    const ATTRIBUTE_ALIAS_SORT_GROUP_NAME = 'sort-group-name';



}