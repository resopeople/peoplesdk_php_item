<?php
/**
 * This class allows to define area group member entity factory class.
 * Area group member entity factory allows to provide new area group member entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\group\member\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\group\group\model\GroupEntityFactory;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\group\member\library\ConstAreaGrpMember;
use people_sdk\item\area\group\member\exception\GroupEntityFactoryInvalidFormatException;
use people_sdk\item\area\group\member\exception\AreaEntityFactoryInvalidFormatException;
use people_sdk\item\area\group\member\exception\GroupEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\area\group\member\exception\AreaEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\area\group\member\model\AreaGrpMemberEntity;



/**
 * @method null|GroupEntityFactory getObjGroupEntityFactory() Get group entity factory object.
 * @method null|AreaEntityFactory getObjAreaEntityFactory() Get area entity factory object.
 * @method null|array getTabGroupEntityFactoryExecConfig() Get group entity factory execution configuration array.
 * @method null|array getTabAreaEntityFactoryExecConfig() Get area entity factory execution configuration array.
 * @method AreaGrpMemberEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjGroupEntityFactory(null|GroupEntityFactory $objGroupEntityFactory) Set group entity factory object.
 * @method void setObjAreaEntityFactory(null|AreaEntityFactory $objAreaEntityFactory) Set area entity factory object.
 * @method void setTabGroupEntityFactoryExecConfig(null|array $tabGroupEntityFactoryExecConfig) Set group entity factory execution configuration array.
 * @method void setTabAreaEntityFactoryExecConfig(null|array $tabAreaEntityFactoryExecConfig) Set area entity factory execution configuration array.
 */
class AreaGrpMemberEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|GroupEntityFactory $objGroupEntityFactory = null
     * @param null|AreaEntityFactory $objAreaEntityFactory = null
     * @param null|array $tabGroupEntityFactoryExecConfig = null
     * @param null|array $tabAreaEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        GroupEntityFactory $objGroupEntityFactory = null,
        AreaEntityFactory $objAreaEntityFactory = null,
        array $tabGroupEntityFactoryExecConfig = null,
        array $tabAreaEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init group entity factory
        $this->setObjGroupEntityFactory($objGroupEntityFactory);

        // Init area entity factory
        $this->setObjAreaEntityFactory($objAreaEntityFactory);

        // Init group entity factory execution config
        $this->setTabGroupEntityFactoryExecConfig($tabGroupEntityFactoryExecConfig);

        // Init area entity factory execution config
        $this->setTabAreaEntityFactoryExecConfig($tabAreaEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAreaGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstAreaGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstAreaGrpMember::DATA_KEY_AREA_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstAreaGrpMember::DATA_KEY_AREA_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstAreaGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstAreaGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstAreaGrpMember::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstAreaGrpMember::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAreaGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY,
            ConstAreaGrpMember::DATA_KEY_AREA_ENTITY_FACTORY,
            ConstAreaGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG,
            ConstAreaGrpMember::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAreaGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY:
                    GroupEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaGrpMember::DATA_KEY_AREA_ENTITY_FACTORY:
                    AreaEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaGrpMember::DATA_KEY_GROUP_ENTITY_FACTORY_EXEC_CONFIG:
                    GroupEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaGrpMember::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG:
                    AreaEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AreaGrpMemberEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstAreaGrpMember::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new AreaGrpMemberEntity(
            $objDateTimeFactory,
            array(),
            $objValidator,
            $this->getObjGroupEntityFactory(),
            $this->getObjAreaEntityFactory(),
            $this->getTabGroupEntityFactoryExecConfig(),
            $this->getTabAreaEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}