<?php
/**
 * This class allows to define area group member entity class.
 * Area group member entity allows to design a specific area group member,
 * which represents specific group, where user profile members can access on specific area.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\group\member\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\group\group\library\ConstGroup;
use people_sdk\group\group\model\GroupEntity;
use people_sdk\group\group\model\GroupEntityFactory;
use people_sdk\item\area\library\ConstArea;
use people_sdk\item\area\model\AreaEntity;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\group\member\library\ConstAreaGrpMember;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|integer|AreaEntity $attrArea : area id|entity
 * @property null|integer|GroupEntity $attrGroup : group id|entity
 */
class AreaGrpMemberEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: Group entity factory instance.
     * @var null|GroupEntityFactory
     */
    protected $objGroupEntityFactory;



    /**
     * DI: Area entity factory instance.
     * @var null|AreaEntityFactory
     */
    protected $objAreaEntityFactory;



    /**
     * DI: Group entity factory execution configuration.
     * @var null|array
     */
    protected $tabGroupEntityFactoryExecConfig;



    /**
     * DI: Area entity factory execution configuration.
     * @var null|array
     */
    protected $tabAreaEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     * @param null|GroupEntityFactory $objGroupEntityFactory = null
     * @param null|AreaEntityFactory $objAreaEntityFactory = null
     * @param null|array $tabGroupEntityFactoryExecConfig = null
     * @param null|array $tabAreaEntityFactoryExecConfig = null
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        GroupEntityFactory $objGroupEntityFactory = null,
        AreaEntityFactory $objAreaEntityFactory = null,
        array $tabGroupEntityFactoryExecConfig = null,
        array $tabAreaEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objGroupEntityFactory = $objGroupEntityFactory;
        $this->objAreaEntityFactory = $objAreaEntityFactory;
        $this->tabGroupEntityFactoryExecConfig = $tabGroupEntityFactoryExecConfig;
        $this->tabAreaEntityFactoryExecConfig = $tabAreaEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
    * @inheritdoc
    */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstAreaGrpMember::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaGrpMember::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaGrpMember::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaGrpMember::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaGrpMember::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaGrpMember::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaGrpMember::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaGrpMember::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaGrpMember::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaGrpMember::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute area
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaGrpMember::ATTRIBUTE_KEY_AREA,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaGrpMember::ATTRIBUTE_ALIAS_AREA,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaGrpMember::ATTRIBUTE_NAME_SAVE_AREA,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute group
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaGrpMember::ATTRIBUTE_KEY_GROUP,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaGrpMember::ATTRIBUTE_ALIAS_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaGrpMember::ATTRIBUTE_NAME_SAVE_GROUP,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidArea = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-id' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ],
                        'is-valid-entity' => [
                            [
                                'type_object',
                                [
                                    'class_path' => [AreaEntity::class]
                                ]
                            ],
                            [
                                'validation_entity',
                                [
                                    'attribute_key' => ConstArea::ATTRIBUTE_KEY_ID
                                ]
                            ],
                            [
                                'new_entity',
                                [
                                    'new_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be a valid area entity or ID.'
                ]
            ]
        );
        $tabRuleConfigValidGroup = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-id' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ],
                        'is-valid-entity' => [
                            [
                                'type_object',
                                [
                                    'class_path' => [GroupEntity::class]
                                ]
                            ],
                            [
                                'validation_entity',
                                [
                                    'attribute_key' => ConstGroup::ATTRIBUTE_KEY_ID
                                ]
                            ],
                            [
                                'new_entity',
                                [
                                    'new_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be a valid group entity or ID.'
                ]
            ]
        );

        // Init var
        $result = array(
            ConstAreaGrpMember::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstAreaGrpMember::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstAreaGrpMember::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstAreaGrpMember::ATTRIBUTE_KEY_AREA => $tabRuleConfigValidArea,
            ConstAreaGrpMember::ATTRIBUTE_KEY_GROUP => $tabRuleConfigValidGroup
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaGrpMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAreaGrpMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaGrpMember::ATTRIBUTE_KEY_ID:
            case ConstAreaGrpMember::ATTRIBUTE_KEY_AREA:
            case ConstAreaGrpMember::ATTRIBUTE_KEY_GROUP:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstAreaGrpMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAreaGrpMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_string($value) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ($value instanceof DateTime) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaGrpMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAreaGrpMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstAreaGrpMember::ATTRIBUTE_KEY_AREA:
                $result = array(
                    ConstArea::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof AreaEntity) ?
                            $value->getAttributeValueSave(ConstArea::ATTRIBUTE_KEY_ID) :
                            $value
                        )
                );
                break;

            case ConstAreaGrpMember::ATTRIBUTE_KEY_GROUP:
                $result = array(
                    ConstGroup::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof GroupEntity) ?
                            $value->getAttributeValueSave(ConstGroup::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objGroupEntityFactory = $this->objGroupEntityFactory;
        $objAreaEntityFactory = $this->objAreaEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaGrpMember::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAreaGrpMember::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstAreaGrpMember::ATTRIBUTE_KEY_AREA:
                $result = $value;
                if((!is_null($objAreaEntityFactory)) && is_array($value))
                {
                    $objAreaEntity = $objAreaEntityFactory->getObjEntity(
                        array(),
                        // Try to select area entity, by id, if required
                        (
                            (!is_null($this->tabAreaEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabAreaEntityFactoryExecConfig
                                        ) :
                                        $this->tabAreaEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objAreaEntity->hydrateSave($value))
                    {
                        $objAreaEntity->setIsNew(false);
                        $result = $objAreaEntity;
                    }
                }
                else if(isset($value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstArea::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            case ConstAreaGrpMember::ATTRIBUTE_KEY_GROUP:
                $result = $value;
                if((!is_null($objGroupEntityFactory)) && is_array($value))
                {
                    $objGroupEntity = $objGroupEntityFactory->getObjEntity(
                        array(),
                        // Try to select group entity, by id, if required
                        (
                            (!is_null($this->tabGroupEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabGroupEntityFactoryExecConfig
                                        ) :
                                        $this->tabGroupEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objGroupEntity->hydrateSave($value))
                    {
                        $objGroupEntity->setIsNew(false);
                        $result = $objGroupEntity;
                    }
                }
                else if(isset($value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstGroup::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}