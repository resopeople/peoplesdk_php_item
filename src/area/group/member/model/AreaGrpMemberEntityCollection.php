<?php
/**
 * This class allows to define area group member entity collection class.
 * key => AreaGrpMemberEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\group\member\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\item\area\group\member\model\AreaGrpMemberEntity;



/**
 * @method null|AreaGrpMemberEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(AreaGrpMemberEntity $objEntity) @inheritdoc
 */
class AreaGrpMemberEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return AreaGrpMemberEntity::class;
    }



}