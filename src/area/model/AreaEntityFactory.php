<?php
/**
 * This class allows to define area entity factory class.
 * Area entity factory allows to provide new area entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\item\schema\model\SchemaEntityFactory;
use people_sdk\item\area\library\ConstArea;
use people_sdk\item\area\exception\SchemaEntityFactoryInvalidFormatException;
use people_sdk\item\area\exception\SchemaEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\area\model\AreaEntity;



/**
 * @method null|SchemaEntityFactory getObjSchemaEntityFactory() Get schema entity factory object.
 * @method null|array getTabSchemaEntityFactoryExecConfig() Get schema entity factory execution configuration array.
 * @method AreaEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjSchemaEntityFactory(null|SchemaEntityFactory $objSchemaEntityFactory) Set schema entity factory object.
 * @method void setTabSchemaEntityFactoryExecConfig(null|array $tabSchemaEntityFactoryExecConfig) Set schema entity factory execution configuration array.
 */
class AreaEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|SchemaEntityFactory $objSchemaEntityFactory = null
     * @param null|array $tabSchemaEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        SchemaEntityFactory $objSchemaEntityFactory = null,
        array $tabSchemaEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init schema entity factory
        $this->setObjSchemaEntityFactory($objSchemaEntityFactory);

        // Init schema entity factory execution config
        $this->setTabSchemaEntityFactoryExecConfig($tabSchemaEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstArea::DATA_KEY_SCHEMA_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstArea::DATA_KEY_SCHEMA_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstArea::DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstArea::DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstArea::DATA_KEY_SCHEMA_ENTITY_FACTORY,
            ConstArea::DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstArea::DATA_KEY_SCHEMA_ENTITY_FACTORY:
                    SchemaEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstArea::DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG:
                    SchemaEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AreaEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstArea::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new AreaEntity(
            $objDateTimeFactory,
            array(),
            $objValidator,
            $this->getObjSchemaEntityFactory(),
            $this->getTabSchemaEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}