<?php
/**
 * This class allows to define area entity collection class.
 * key => AreaEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\item\area\model\AreaEntity;



/**
 * @method null|AreaEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(AreaEntity $objEntity) @inheritdoc
 */
class AreaEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return AreaEntity::class;
    }



}