<?php
/**
 * This class allows to define area entity class.
 * Area allows to define specific context,
 * to use items, following specific schema definition.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\item\schema\library\ConstSchema;
use people_sdk\item\schema\model\SchemaEntity;
use people_sdk\item\schema\model\SchemaEntityFactory;
use people_sdk\item\area\library\ConstArea;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|string $strAttrName
 * @property null|integer|SchemaEntity $attrSchema : schema id|entity
 * @property null|boolean $boolAttrItemGetEnableRequired
 * @property null|boolean $boolAttrItemCreateEnableRequired
 * @property null|boolean $boolAttrItemUpdateEnableRequired
 * @property null|boolean $boolAttrItemDeleteEnableRequired
 * @property null|boolean $boolAttrItemProfileGetEnableRequired
 * @property null|boolean $boolAttrItemProfileUpdateEnableRequired
 * @property null|boolean $boolAttrItemProfileDeleteEnableRequired
 * @property null|boolean $boolAttrItemNoCategoryAccessEnableRequired
 * @property null|boolean $boolAttrItemRelationGetEnableRequired
 * @property null|boolean $boolAttrItemRelationCreateEnableRequired
 * @property null|boolean $boolAttrItemRelationUpdateEnableRequired
 * @property null|boolean $boolAttrItemRelationDeleteEnableRequired
 * @property null|boolean $boolAttrItemProfileRelationGetEnableRequired
 * @property null|boolean $boolAttrItemProfileRelationCreateEnableRequired
 * @property null|boolean $boolAttrItemProfileRelationUpdateEnableRequired
 * @property null|boolean $boolAttrItemProfileRelationDeleteEnableRequired
 * @property null|array $tabAttrItemDefaultCategory
 * @property null|boolean $boolAttrItemCategoryGetEnableRequired
 * @property null|boolean $boolAttrItemCategoryCreateEnableRequired
 * @property null|boolean $boolAttrItemCategoryUpdateEnableRequired
 * @property null|boolean $boolAttrItemCategoryDeleteEnableRequired
 * @property null|boolean $boolAttrItemProfileCategoryGetEnableRequired
 * @property null|boolean $boolAttrItemProfileCategoryCreateEnableRequired
 * @property null|boolean $boolAttrItemProfileCategoryUpdateEnableRequired
 * @property null|boolean $boolAttrItemProfileCategoryDeleteEnableRequired
 */
class AreaEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: Schema entity factory instance.
     * @var null|SchemaEntityFactory
     */
    protected $objSchemaEntityFactory;



    /**
     * DI: Schema entity factory execution configuration.
     * @var null|array
     */
    protected $tabSchemaEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     * @param null|SchemaEntityFactory $objSchemaEntityFactory = null
     * @param null|array $tabSchemaEntityFactoryExecConfig = null
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        SchemaEntityFactory $objSchemaEntityFactory = null,
        array $tabSchemaEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objSchemaEntityFactory = $objSchemaEntityFactory;
        $this->tabSchemaEntityFactoryExecConfig = $tabSchemaEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
    * @inheritdoc
    */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstSchema::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute schema
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_SCHEMA,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_SCHEMA,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_SCHEMA,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item get enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_GET_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item create enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_CREATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item update enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_UPDATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item delete enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_DELETE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile item get enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_GET_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_PROFILE_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile item update enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile item delete enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_DELETE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_PROFILE_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item with no category access enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item relation get enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_RELATION_GET_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_RELATION_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_RELATION_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item relation create enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_RELATION_CREATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_RELATION_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_RELATION_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item relation update enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_RELATION_UPDATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_RELATION_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_RELATION_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item relation delete enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_RELATION_DELETE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_RELATION_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_RELATION_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile item relation get enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile item relation create enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile item relation update enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile item relation delete enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item default category (used when item created)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_DEFAULT_CATEGORY,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_DEFAULT_CATEGORY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_DEFAULT_CATEGORY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item category get enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_CATEGORY_GET_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_CATEGORY_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_CATEGORY_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item category create enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item category update enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item category delete enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile item category get enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile item category create enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile item category update enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile item category delete enable required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstArea::ATTRIBUTE_ALIAS_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstArea::ATTRIBUTE_NAME_SAVE_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidSchema = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-id' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ],
                        'is-valid-entity' => [
                            [
                                'type_object',
                                [
                                    'class_path' => [SchemaEntity::class]
                                ]
                            ],
                            [
                                'validation_entity',
                                [
                                    'attribute_key' => ConstSchema::ATTRIBUTE_KEY_ID
                                ]
                            ],
                            [
                                'new_entity',
                                [
                                    'new_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be a valid schema entity or ID.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );

        // Init var
        $result = array(
            ConstArea::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstArea::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstArea::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstArea::ATTRIBUTE_KEY_NAME => $tabRuleConfigValidString,
            ConstArea::ATTRIBUTE_KEY_SCHEMA => $tabRuleConfigValidSchema,
            ConstArea::ATTRIBUTE_KEY_ITEM_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_RELATION_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_RELATION_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_RELATION_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_RELATION_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_DEFAULT_CATEGORY => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-iterate-category' => [
                                [
                                    'type_array',
                                    [
                                        'index_only_require' => true
                                    ]
                                ],
                                [
                                    'sub_rule_iterate',
                                    [
                                        'rule_config' => [
                                            [
                                                'group_sub_rule_or',
                                                [
                                                    'rule_config' => [
                                                        'is-valid-integer' => [
                                                            [
                                                                'type_numeric',
                                                                ['integer_only_require' => true]
                                                            ],
                                                            [
                                                                'compare_greater',
                                                                [
                                                                    'compare_value' => 0,
                                                                    'equal_enable_require' => false
                                                                ]
                                                            ]
                                                        ],
                                                        'is-valid-string' => [
                                                            'type_string',
                                                            [
                                                                'sub_rule_not',
                                                                [
                                                                    'rule_config' => ['is_empty']
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a valid array of category ids/names.'
                    ]
                ]
            ],
            ConstArea::ATTRIBUTE_KEY_ITEM_CATEGORY_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean,
            ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED => $tabRuleConfigValidBoolean
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstArea::ATTRIBUTE_KEY_DT_CREATE:
            case ConstArea::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstArea::ATTRIBUTE_KEY_ID:
            case ConstArea::ATTRIBUTE_KEY_SCHEMA:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstArea::ATTRIBUTE_KEY_DT_CREATE:
            case ConstArea::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_string($value) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ($value instanceof DateTime) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstArea::ATTRIBUTE_KEY_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;


            case ConstArea::ATTRIBUTE_KEY_ITEM_GET_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_CREATE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_UPDATE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_DELETE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_GET_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_UPDATE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_DELETE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_NO_CATEGORY_ACCESS_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_RELATION_GET_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_RELATION_CREATE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_RELATION_UPDATE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_RELATION_DELETE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_GET_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_CREATE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_UPDATE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_RELATION_DELETE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_CATEGORY_GET_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_CATEGORY_CREATE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_CATEGORY_UPDATE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_CATEGORY_DELETE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_GET_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_CREATE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_UPDATE_ENABLE_REQUIRED:
            case ConstArea::ATTRIBUTE_KEY_ITEM_PROFILE_CATEGORY_DELETE_ENABLE_REQUIRED:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            case ConstArea::ATTRIBUTE_KEY_ITEM_DEFAULT_CATEGORY:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_array($value) ?
                                array_values(array_map(
                                    function($value) {
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstArea::ATTRIBUTE_KEY_DT_CREATE:
            case ConstArea::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstArea::ATTRIBUTE_KEY_SCHEMA:
                $result = array(
                    ConstSchema::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof SchemaEntity) ?
                            $value->getAttributeValueSave(ConstSchema::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objSchemaEntityFactory = $this->objSchemaEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstArea::ATTRIBUTE_KEY_DT_CREATE:
            case ConstArea::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    is_string($value) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstArea::ATTRIBUTE_KEY_SCHEMA:
                $result = $value;
                if((!is_null($objSchemaEntityFactory)) && is_array($value))
                {
                    $objSchemaEntity = $objSchemaEntityFactory->getObjEntity(
                        array(),
                        // Try to select schema entity, by id, if required
                        (
                            (!is_null($this->tabSchemaEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabSchemaEntityFactoryExecConfig
                                        ) :
                                        $this->tabSchemaEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objSchemaEntity->hydrateSave($value))
                    {
                        $objSchemaEntity->setIsNew(false);
                        $result = $objSchemaEntity;
                    }
                }
                else if(isset($value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}