<?php
/**
 * This class allows to define area attribute scope entity class.
 * Area attribute scope entity allows to define a specific attribute scope,
 * which represents specific schema attribute, available on specific area.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\schema\attribute\scope\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\attribute_model\attribute\library\ConstAttribute;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\item\schema\attribute\library\ConstSchemaAttr;
use people_sdk\item\schema\attribute\model\SchemaAttrEntity;
use people_sdk\item\schema\attribute\model\SchemaAttrEntityFactory;
use people_sdk\item\area\library\ConstArea;
use people_sdk\item\area\model\AreaEntity;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\schema\attribute\scope\library\ConstAreaAttrScope;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|integer|AreaEntity $attrArea : area id|entity
 * @property null|integer|SchemaAttrEntity $attrSchemaAttr : schema attribute id|entity
 * @property null|boolean $boolAttrScopeGetRequired
 * @property null|boolean $boolAttrScopeUpdateRequired
 */
class AreaAttrScopeEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: Schema attribute entity factory instance.
     * @var null|SchemaAttrEntityFactory
     */
    protected $objSchemaAttrEntityFactory;



    /**
     * DI: Area entity factory instance.
     * @var null|AreaEntityFactory
     */
    protected $objAreaEntityFactory;



    /**
     * DI: Schema attribute entity factory execution configuration.
     * @var null|array
     */
    protected $tabSchemaAttrEntityFactoryExecConfig;



    /**
     * DI: Area entity factory execution configuration.
     * @var null|array
     */
    protected $tabAreaEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     * @param null|SchemaAttrEntityFactory $objSchemaAttrEntityFactory = null
     * @param null|AreaEntityFactory $objAreaEntityFactory = null
     * @param null|array $tabSchemaAttrEntityFactoryExecConfig = null
     * @param null|array $tabAreaEntityFactoryExecConfig = null
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        SchemaAttrEntityFactory $objSchemaAttrEntityFactory = null,
        AreaEntityFactory $objAreaEntityFactory = null,
        array $tabSchemaAttrEntityFactoryExecConfig = null,
        array $tabAreaEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objSchemaAttrEntityFactory = $objSchemaAttrEntityFactory;
        $this->objAreaEntityFactory = $objAreaEntityFactory;
        $this->tabSchemaAttrEntityFactoryExecConfig = $tabSchemaAttrEntityFactoryExecConfig;
        $this->tabAreaEntityFactoryExecConfig = $tabAreaEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
    * @inheritdoc
    */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstAreaAttrScope::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaAttrScope::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaAttrScope::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaAttrScope::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaAttrScope::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaAttrScope::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaAttrScope::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaAttrScope::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaAttrScope::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaAttrScope::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute area
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaAttrScope::ATTRIBUTE_KEY_AREA,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaAttrScope::ATTRIBUTE_ALIAS_AREA,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaAttrScope::ATTRIBUTE_NAME_SAVE_AREA,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute schema attribute
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaAttrScope::ATTRIBUTE_KEY_SCHEMA_ATTR,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaAttrScope::ATTRIBUTE_ALIAS_SCHEMA_ATTR,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaAttrScope::ATTRIBUTE_NAME_SAVE_SCHEMA_ATTR,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute scope get required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaAttrScope::ATTRIBUTE_KEY_SCOPE_GET_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaAttrScope::ATTRIBUTE_ALIAS_SCOPE_GET_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaAttrScope::ATTRIBUTE_NAME_SAVE_SCOPE_GET_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute scope update required option
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAreaAttrScope::ATTRIBUTE_KEY_SCOPE_UPDATE_REQUIRED,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAreaAttrScope::ATTRIBUTE_ALIAS_SCOPE_UPDATE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAreaAttrScope::ATTRIBUTE_NAME_SAVE_SCOPE_UPDATE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidArea = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-id' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ],
                        'is-valid-entity' => [
                            [
                                'type_object',
                                [
                                    'class_path' => [AreaEntity::class]
                                ]
                            ],
                            [
                                'validation_entity',
                                [
                                    'attribute_key' => ConstArea::ATTRIBUTE_KEY_ID
                                ]
                            ],
                            [
                                'new_entity',
                                [
                                    'new_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be a valid area entity or ID.'
                ]
            ]
        );
        $tabRuleConfigValidSchemaAttr = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-id' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ],
                        'is-valid-entity' => [
                            [
                                'type_object',
                                [
                                    'class_path' => [SchemaAttrEntity::class]
                                ]
                            ],
                            [
                                'validation_entity',
                                [
                                    'attribute_key' => ConstAttribute::ATTRIBUTE_KEY_ID
                                ]
                            ],
                            [
                                'new_entity',
                                [
                                    'new_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be a valid schema attribute entity or ID.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );

        // Init var
        $result = array(
            ConstAreaAttrScope::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstAreaAttrScope::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstAreaAttrScope::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstAreaAttrScope::ATTRIBUTE_KEY_AREA => $tabRuleConfigValidArea,
            ConstAreaAttrScope::ATTRIBUTE_KEY_SCHEMA_ATTR => $tabRuleConfigValidSchemaAttr,
            ConstAreaAttrScope::ATTRIBUTE_KEY_SCOPE_GET_REQUIRED => $tabRuleConfigValidBoolean,
            ConstAreaAttrScope::ATTRIBUTE_KEY_SCOPE_UPDATE_REQUIRED => $tabRuleConfigValidBoolean
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaAttrScope::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAreaAttrScope::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaAttrScope::ATTRIBUTE_KEY_ID:
            case ConstAreaAttrScope::ATTRIBUTE_KEY_AREA:
            case ConstAreaAttrScope::ATTRIBUTE_KEY_SCHEMA_ATTR:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstAreaAttrScope::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAreaAttrScope::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_string($value) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ($value instanceof DateTime) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstAreaAttrScope::ATTRIBUTE_KEY_SCOPE_GET_REQUIRED:
            case ConstAreaAttrScope::ATTRIBUTE_KEY_SCOPE_UPDATE_REQUIRED:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaAttrScope::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAreaAttrScope::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstAreaAttrScope::ATTRIBUTE_KEY_AREA:
                $result = array(
                    ConstArea::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof AreaEntity) ?
                            $value->getAttributeValueSave(ConstArea::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            case ConstAreaAttrScope::ATTRIBUTE_KEY_SCHEMA_ATTR:
                $result = (
                    ($value instanceof SchemaAttrEntity) ?
                        $value->getAttributeValueSave(ConstAttribute::ATTRIBUTE_KEY_ID) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objSchemaAttrEntityFactory = $this->objSchemaAttrEntityFactory;
        $objAreaEntityFactory = $this->objAreaEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaAttrScope::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAreaAttrScope::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    is_string($value) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstAreaAttrScope::ATTRIBUTE_KEY_AREA:
                $result = $value;
                if((!is_null($objAreaEntityFactory)) && is_array($value))
                {
                    $objAreaEntity = $objAreaEntityFactory->getObjEntity(
                        array(),
                        // Try to select area entity, by id, if required
                        (
                            (!is_null($this->tabAreaEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabAreaEntityFactoryExecConfig
                                        ) :
                                        $this->tabAreaEntityFactoryExecConfig
                                ) :
                                (
                                isset($value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]) ?
                                    array(
                                        ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                            $value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]
                                    ) :
                                    null
                                )
                        )
                    );
                    if($objAreaEntity->hydrateSave($value))
                    {
                        $objAreaEntity->setIsNew(false);
                        $result = $objAreaEntity;
                    }
                }
                else if(isset($value[ConstArea::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstArea::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            case ConstAreaAttrScope::ATTRIBUTE_KEY_SCHEMA_ATTR:
                $result = $value;
                if((!is_null($objSchemaAttrEntityFactory)) && is_array($value))
                {
                    $objSchemaAttrEntity = $objSchemaAttrEntityFactory->getObjEntity(
                        array(),
                        // Try to select schema attribute entity, by id, if required
                        (
                            (!is_null($this->tabSchemaAttrEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabSchemaAttrEntityFactoryExecConfig
                                        ) :
                                        $this->tabSchemaAttrEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objSchemaAttrEntity->hydrateSave($value))
                    {
                        $objSchemaAttrEntity->setIsNew(false);
                        $result = $objSchemaAttrEntity;
                    }
                }
                else if(isset($value[ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstSchemaAttr::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}