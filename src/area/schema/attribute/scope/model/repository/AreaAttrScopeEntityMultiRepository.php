<?php
/**
 * This class allows to define area attribute scope entity multi repository class.
 * Area attribute scope entity multi repository is multi repository,
 * allows to prepare data from area attribute scope entity, to save in requisition persistence.
 * Specified requisition persistence must be able to use HTTP client, persistor HTTP request and persistor HTTP response, with json parsing,
 * to handle HTTP request sending and HTTP response reception.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\schema\attribute\scope\model\repository;

use people_sdk\library\model\repository\multi\model\MultiRepository;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\parser\parser\factory\library\ConstParserFactory;
use liberty_code\parser\parser\factory\string_table\library\ConstStrTableParserFactory;
use liberty_code\model\repository\library\ConstRepository as BaseConstRepository;
use liberty_code\model\repository\multi\library\ConstMultiRepository;
use liberty_code\requisition\request\factory\library\ConstRequestFactory;
use liberty_code\requisition\response\factory\library\ConstResponseFactory;
use liberty_code\requisition\client\info\library\ConstInfoClient;
use liberty_code\requisition\persistence\library\ConstPersistor;
use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\data\library\ConstDataHttpRequest;
use liberty_code\http\requisition\request\persistence\library\ConstPersistorHttpRequest;
use liberty_code\http\requisition\request\factory\library\ConstHttpRequestFactory;
use liberty_code\http\requisition\response\data\library\ConstDataHttpResponse;
use liberty_code\http\requisition\response\persistence\library\ConstPersistorHttpResponse;
use liberty_code\http\requisition\response\factory\library\ConstHttpResponseFactory;
use people_sdk\library\requisition\request\info\library\ConstSndInfo;
use people_sdk\library\requisition\response\library\ConstResponse;
use people_sdk\library\model\entity\requisition\response\library\ConstResponseEntity;
use people_sdk\library\model\repository\library\ConstRepository;
use people_sdk\item\area\schema\attribute\scope\library\ConstAreaAttrScope;
use people_sdk\item\area\schema\attribute\scope\model\AreaAttrScopeEntity;



class AreaAttrScopeEntityMultiRepository extends MultiRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Init var
        $tabPersistConfig = array(
            ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                ConstRequestFactory::TAB_CONFIG_KEY_TYPE => ConstHttpRequestFactory::CONFIG_TYPE_HTTP_PERSISTOR,
                ConstDataHttpRequest::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                    ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON
                ],
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_PATH_SEPARATOR => '/',
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_DATA_ID_KEY => ConstAreaAttrScope::ATTRIBUTE_NAME_SAVE_ID
            ],
            ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                    ConstResponseFactory::TAB_CONFIG_KEY_TYPE => ConstHttpResponseFactory::CONFIG_TYPE_HTTP_PERSISTOR,
                    ConstDataHttpResponse::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                        ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON
                    ],
                    ConstPersistorHttpResponse::TAB_CONFIG_KEY_PATH_SEPARATOR => '/',
                    ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_ID_KEY => ConstAreaAttrScope::ATTRIBUTE_NAME_SAVE_ID
                ]
            ]
        );
        $tabPersistConfigSet = ToolBoxTable::getTabMerge(
            $tabPersistConfig,
            array(
                ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                    ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                        ConstHttpRequest::TAB_SND_INFO_KEY_HEADER => [
                            ConstSndInfo::HEADER_KEY_CONTENT_TYPE => ConstSndInfo::CONTENT_TYPE_JSON
                        ]
                    ]
                ]
            )
        );
        $result = array(
            BaseConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AreaAttrScopeEntity::class,
            BaseConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE => false,
            ConstMultiRepository::TAB_CONFIG_KEY_ATTRIBUTE_KEY_ID => ConstAreaAttrScope::ATTRIBUTE_KEY_ID,

            // Persistence configuration, for persistence action get
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_CONFIG => $tabPersistConfig,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstAreaAttrScope::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_ID_KEY => 'crit-equal-id',
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/item/area/attribute-scope'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s/0',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_TYPE => ConstAreaAttrScope::SUB_ACTION_TYPE_ADMIN,

            // Persistence configuration, for persistence action create
            ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG => $tabPersistConfigSet,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstAreaAttrScope::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_DATA_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY,
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_CREATE_DATA_PATH => '0',
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'POST',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/item/area/attribute-scope'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s/0',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_TYPE => ConstAreaAttrScope::SUB_ACTION_TYPE_ADMIN,

            // Persistence configuration, for persistence action update
            ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG => $tabPersistConfigSet,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstAreaAttrScope::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_DATA_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY,
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_UPDATE_DATA_PATH => '0',
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'POST',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/item/area/attribute-scope'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s/0',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_TYPE => ConstAreaAttrScope::SUB_ACTION_TYPE_ADMIN,

            // Persistence configuration, for persistence action delete
            ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG => $tabPersistConfig,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstAreaAttrScope::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_ID_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_ID_KEY => 'crit-equal-id',
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'DELETE',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/item/area/attribute-scope'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_OPTIONAL,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s/0',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_TYPE => ConstAreaAttrScope::SUB_ACTION_TYPE_ADMIN
        );

        // Return result
        return $result;
    }



}


