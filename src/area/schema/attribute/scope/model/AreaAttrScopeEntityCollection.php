<?php
/**
 * This class allows to define area attribute scope entity collection class.
 * key => AreaAttrScopeEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\schema\attribute\scope\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\item\area\schema\attribute\scope\model\AreaAttrScopeEntity;



/**
 * @method null|AreaAttrScopeEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(AreaAttrScopeEntity $objEntity) @inheritdoc
 */
class AreaAttrScopeEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return AreaAttrScopeEntity::class;
    }



}