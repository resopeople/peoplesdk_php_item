<?php
/**
 * This class allows to define area attribute scope entity factory class.
 * Area attribute scope entity factory allows to provide new area attribute scope entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\schema\attribute\scope\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\item\schema\attribute\model\SchemaAttrEntityFactory;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\schema\attribute\scope\library\ConstAreaAttrScope;
use people_sdk\item\area\schema\attribute\scope\exception\SchemaAttrEntityFactoryInvalidFormatException;
use people_sdk\item\area\schema\attribute\scope\exception\AreaEntityFactoryInvalidFormatException;
use people_sdk\item\area\schema\attribute\scope\exception\SchemaAttrEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\area\schema\attribute\scope\exception\AreaEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\area\schema\attribute\scope\model\AreaAttrScopeEntity;



/**
 * @method null|SchemaAttrEntityFactory getObjSchemaAttrEntityFactory() Get schema attribute entity factory object.
 * @method null|AreaEntityFactory getObjAreaEntityFactory() Get area entity factory object.
 * @method null|array getTabSchemaAttrEntityFactoryExecConfig() Get schema attribute entity factory execution configuration array.
 * @method null|array getTabAreaEntityFactoryExecConfig() Get area entity factory execution configuration array.
 * @method AreaAttrScopeEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjSchemaAttrEntityFactory(null|SchemaAttrEntityFactory $objSchemaAttrEntityFactory) Set schema attribute entity factory object.
 * @method void setObjAreaEntityFactory(null|AreaEntityFactory $objAreaEntityFactory) Set area entity factory object.
 * @method void setTabSchemaAttrEntityFactoryExecConfig(null|array $tabSchemaAttrEntityFactoryExecConfig) Set schema attribute entity factory execution configuration array.
 * @method void setTabAreaEntityFactoryExecConfig(null|array $tabAreaEntityFactoryExecConfig) Set area entity factory execution configuration array.
 */
class AreaAttrScopeEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|SchemaAttrEntityFactory $objSchemaAttrEntityFactory = null
     * @param null|AreaEntityFactory $objAreaEntityFactory = null
     * @param null|array $tabSchemaAttrEntityFactoryExecConfig = null
     * @param null|array $tabAreaEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        SchemaAttrEntityFactory $objSchemaAttrEntityFactory = null,
        AreaEntityFactory $objAreaEntityFactory = null,
        array $tabSchemaAttrEntityFactoryExecConfig = null,
        array $tabAreaEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init schema attribute entity factory
        $this->setObjSchemaAttrEntityFactory($objSchemaAttrEntityFactory);

        // Init area entity factory
        $this->setObjAreaEntityFactory($objAreaEntityFactory);

        // Init schema attribute entity factory execution config
        $this->setTabSchemaAttrEntityFactoryExecConfig($tabSchemaAttrEntityFactoryExecConfig);

        // Init area entity factory execution config
        $this->setTabAreaEntityFactoryExecConfig($tabAreaEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAreaAttrScope::DATA_KEY_SCHEMA_ATTR_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstAreaAttrScope::DATA_KEY_SCHEMA_ATTR_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstAreaAttrScope::DATA_KEY_AREA_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstAreaAttrScope::DATA_KEY_AREA_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstAreaAttrScope::DATA_KEY_SCHEMA_ATTR_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstAreaAttrScope::DATA_KEY_SCHEMA_ATTR_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstAreaAttrScope::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstAreaAttrScope::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAreaAttrScope::DATA_KEY_SCHEMA_ATTR_ENTITY_FACTORY,
            ConstAreaAttrScope::DATA_KEY_AREA_ENTITY_FACTORY,
            ConstAreaAttrScope::DATA_KEY_SCHEMA_ATTR_ENTITY_FACTORY_EXEC_CONFIG,
            ConstAreaAttrScope::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAreaAttrScope::DATA_KEY_SCHEMA_ATTR_ENTITY_FACTORY:
                    SchemaAttrEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaAttrScope::DATA_KEY_AREA_ENTITY_FACTORY:
                    AreaEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaAttrScope::DATA_KEY_SCHEMA_ATTR_ENTITY_FACTORY_EXEC_CONFIG:
                    SchemaAttrEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaAttrScope::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG:
                    AreaEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AreaAttrScopeEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstAreaAttrScope::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new AreaAttrScopeEntity(
            $objDateTimeFactory,
            array(),
            $objValidator,
            $this->getObjSchemaAttrEntityFactory(),
            $this->getObjAreaEntityFactory(),
            $this->getTabSchemaAttrEntityFactoryExecConfig(),
            $this->getTabAreaEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}