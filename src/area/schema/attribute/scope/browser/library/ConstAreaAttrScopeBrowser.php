<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\schema\attribute\scope\browser\library;



class ConstAreaAttrScopeBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_EQUAL_AREA_ID = 'intAttrCritEqualAreaId';
    const ATTRIBUTE_KEY_CRIT_IN_AREA_ID = 'tabAttrCritInAreaId';
    const ATTRIBUTE_KEY_CRIT_LIKE_AREA_NAME = 'strAttrCritLikeAreaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_AREA_NAME = 'strAttrCritEqualAreaName';
    const ATTRIBUTE_KEY_CRIT_IN_AREA_NAME = 'tabAttrCritInAreaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ATTR_ID = 'intAttrCritEqualSchemaAttrId';
    const ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ATTR_ID = 'tabAttrCritInSchemaAttrId';
    const ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_ATTR_NAME = 'strAttrCritLikeSchemaAttrName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ATTR_NAME = 'strAttrCritEqualSchemaAttrName';
    const ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ATTR_NAME = 'tabAttrCritInSchemaAttrName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_ID = 'intAttrCritEqualSchemaId';
    const ATTRIBUTE_KEY_CRIT_IN_SCHEMA_ID = 'tabAttrCritInSchemaId';
    const ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_NAME = 'strAttrCritLikeSchemaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_NAME = 'strAttrCritEqualSchemaName';
    const ATTRIBUTE_KEY_CRIT_IN_SCHEMA_NAME = 'tabAttrCritInSchemaName';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_AREA_ID = 'strAttrSortAreaId';
    const ATTRIBUTE_KEY_SORT_AREA_NAME = 'strAttrSortAreaName';
    const ATTRIBUTE_KEY_SORT_SCHEMA_ATTR_ID = 'strAttrSortSchemaAttrId';
    const ATTRIBUTE_KEY_SORT_SCHEMA_ATTR_NAME = 'strAttrSortSchemaAttrName';
    const ATTRIBUTE_KEY_SORT_SCHEMA_ID = 'strAttrSortSchemaId';
    const ATTRIBUTE_KEY_SORT_SCHEMA_NAME = 'strAttrSortSchemaName';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_ID = 'crit-equal-area-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_AREA_ID = 'crit-in-area-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_AREA_NAME = 'crit-like-area-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_NAME = 'crit-equal-area-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_AREA_NAME = 'crit-in-area-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_ATTR_ID = 'crit-equal-schema-attribute-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_ATTR_ID = 'crit-in-schema-attribute-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_SCHEMA_ATTR_NAME = 'crit-like-schema-attribute-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_ATTR_NAME = 'crit-equal-schema-attribute-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_ATTR_NAME = 'crit-in-schema-attribute-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_ID = 'crit-equal-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_ID = 'crit-in-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_SCHEMA_NAME = 'crit-like-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_NAME = 'crit-equal-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_NAME = 'crit-in-schema-name';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_AREA_ID = 'sort-area-id';
    const ATTRIBUTE_ALIAS_SORT_AREA_NAME = 'sort-area-name';
    const ATTRIBUTE_ALIAS_SORT_SCHEMA_ATTR_ID = 'sort-schema-attribute-id';
    const ATTRIBUTE_ALIAS_SORT_SCHEMA_ATTR_NAME = 'sort-schema-attribute-name';
    const ATTRIBUTE_ALIAS_SORT_SCHEMA_ID = 'sort-schema-id';
    const ATTRIBUTE_ALIAS_SORT_SCHEMA_NAME = 'sort-schema-name';



}