<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\schema\relation\scope\library;



class ConstAreaRelationScope
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY = 'objSchemaRelationEntityFactory';
    const DATA_KEY_AREA_ENTITY_FACTORY = 'objAreaEntityFactory';
    const DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY_EXEC_CONFIG = 'tabSchemaRelationEntityFactoryExecConfig';
    const DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG = 'tabAreaEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_AREA = 'attrArea';
    const ATTRIBUTE_KEY_SCHEMA_RELATION = 'attrSchemaRelation';
    const ATTRIBUTE_KEY_SUB_AREA = 'attrSubArea';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_AREA = 'area';
    const ATTRIBUTE_ALIAS_SCHEMA_RELATION = 'schema-relation';
    const ATTRIBUTE_ALIAS_SUB_AREA = 'sub-area';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_AREA = 'area';
    const ATTRIBUTE_NAME_SAVE_SCHEMA_RELATION = 'schema-relation';
    const ATTRIBUTE_NAME_SAVE_SUB_AREA = 'sub-area';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_PROFILE = 'profile';



    // Exception message constants
    const EXCEPT_MSG_SCHEMA_RELATION_ENTITY_FACTORY_INVALID_FORMAT =
        'Following schema relation entity factory "%1$s" invalid! It must be a schema relation entity factory object.';
    const EXCEPT_MSG_AREA_ENTITY_FACTORY_INVALID_FORMAT =
        'Following area entity factory "%1$s" invalid! It must be an area entity factory object.';
    const EXCEPT_MSG_SCHEMA_RELATION_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the schema relation entity factory execution configuration standard.';
    const EXCEPT_MSG_AREA_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the area entity factory execution configuration standard.';



}