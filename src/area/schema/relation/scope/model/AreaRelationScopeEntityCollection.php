<?php
/**
 * This class allows to define area relation scope entity collection class.
 * key => AreaRelationScopeEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\schema\relation\scope\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\item\area\schema\relation\scope\model\AreaRelationScopeEntity;



/**
 * @method null|AreaRelationScopeEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(AreaRelationScopeEntity $objEntity) @inheritdoc
 */
class AreaRelationScopeEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return AreaRelationScopeEntity::class;
    }



}