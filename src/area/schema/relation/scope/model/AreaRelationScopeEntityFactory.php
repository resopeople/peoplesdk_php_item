<?php
/**
 * This class allows to define area relation scope entity factory class.
 * Area relation scope entity factory allows to provide new area relation scope entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\schema\relation\scope\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\item\schema\relation\model\SchemaRelationEntityFactory;
use people_sdk\item\area\model\AreaEntityFactory;
use people_sdk\item\area\schema\relation\scope\library\ConstAreaRelationScope;
use people_sdk\item\area\schema\relation\scope\exception\SchemaRelationEntityFactoryInvalidFormatException;
use people_sdk\item\area\schema\relation\scope\exception\AreaEntityFactoryInvalidFormatException;
use people_sdk\item\area\schema\relation\scope\exception\SchemaRelationEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\area\schema\relation\scope\exception\AreaEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\area\schema\relation\scope\model\AreaRelationScopeEntity;



/**
 * @method null|SchemaRelationEntityFactory getObjSchemaRelationEntityFactory() Get schema relation entity factory object.
 * @method null|AreaEntityFactory getObjAreaEntityFactory() Get area entity factory object.
 * @method null|array getTabSchemaRelationEntityFactoryExecConfig() Get schema relation entity factory execution configuration array.
 * @method null|array getTabAreaEntityFactoryExecConfig() Get area entity factory execution configuration array.
 * @method AreaRelationScopeEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjSchemaRelationEntityFactory(null|SchemaRelationEntityFactory $objSchemaRelationEntityFactory) Set schema relation entity factory object.
 * @method void setObjAreaEntityFactory(null|AreaEntityFactory $objAreaEntityFactory) Set area entity factory object.
 * @method void setTabSchemaRelationEntityFactoryExecConfig(null|array $tabSchemaRelationEntityFactoryExecConfig) Set schema relation entity factory execution configuration array.
 * @method void setTabAreaEntityFactoryExecConfig(null|array $tabAreaEntityFactoryExecConfig) Set area entity factory execution configuration array.
 */
class AreaRelationScopeEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|SchemaRelationEntityFactory $objSchemaRelationEntityFactory = null
     * @param null|AreaEntityFactory $objAreaEntityFactory = null
     * @param null|array $tabSchemaRelationEntityFactoryExecConfig = null
     * @param null|array $tabAreaEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        SchemaRelationEntityFactory $objSchemaRelationEntityFactory = null,
        AreaEntityFactory $objAreaEntityFactory = null,
        array $tabSchemaRelationEntityFactoryExecConfig = null,
        array $tabAreaEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init schema relation entity factory
        $this->setObjSchemaRelationEntityFactory($objSchemaRelationEntityFactory);

        // Init area entity factory
        $this->setObjAreaEntityFactory($objAreaEntityFactory);

        // Init schema relation entity factory execution config
        $this->setTabSchemaRelationEntityFactoryExecConfig($tabSchemaRelationEntityFactoryExecConfig);

        // Init area entity factory execution config
        $this->setTabAreaEntityFactoryExecConfig($tabAreaEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAreaRelationScope::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstAreaRelationScope::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstAreaRelationScope::DATA_KEY_AREA_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstAreaRelationScope::DATA_KEY_AREA_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstAreaRelationScope::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstAreaRelationScope::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstAreaRelationScope::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstAreaRelationScope::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAreaRelationScope::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY,
            ConstAreaRelationScope::DATA_KEY_AREA_ENTITY_FACTORY,
            ConstAreaRelationScope::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY_EXEC_CONFIG,
            ConstAreaRelationScope::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAreaRelationScope::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY:
                    SchemaRelationEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaRelationScope::DATA_KEY_AREA_ENTITY_FACTORY:
                    AreaEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaRelationScope::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY_EXEC_CONFIG:
                    SchemaRelationEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstAreaRelationScope::DATA_KEY_AREA_ENTITY_FACTORY_EXEC_CONFIG:
                    AreaEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AreaRelationScopeEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstAreaRelationScope::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new AreaRelationScopeEntity(
            $objDateTimeFactory,
            array(),
            $objValidator,
            $this->getObjSchemaRelationEntityFactory(),
            $this->getObjAreaEntityFactory(),
            $this->getTabSchemaRelationEntityFactoryExecConfig(),
            $this->getTabAreaEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}