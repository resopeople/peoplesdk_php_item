<?php
/**
 * This class allows to define area relation scope browser entity class.
 * Area relation scope browser entity allows to define attributes,
 * to search area relation scope entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\schema\relation\scope\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\item\area\schema\relation\scope\browser\library\ConstAreaRelationScopeBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|integer $intAttrCritEqualAreaId
 * @property null|integer[] $tabAttrCritInAreaId
 * @property null|string $strAttrCritLikeAreaName
 * @property null|string $strAttrCritEqualAreaName
 * @property null|string[] $tabAttrCritInAreaName
 * @property null|integer $intAttrCritEqualAreaSchemaId
 * @property null|integer[] $tabAttrCritInAreaSchemaId
 * @property null|string $strAttrCritLikeAreaSchemaName
 * @property null|string $strAttrCritEqualAreaSchemaName
 * @property null|string[] $tabAttrCritInAreaSchemaName
 * @property null|integer $intAttrCritEqualSchemaRelationId
 * @property null|integer[] $tabAttrCritInSchemaRelationId
 * @property null|string $strAttrCritLikeSchemaRelationName
 * @property null|string $strAttrCritEqualSchemaRelationName
 * @property null|string[] $tabAttrCritInSchemaRelationName
 * @property null|integer $intAttrCritEqualSubAreaId
 * @property null|integer[] $tabAttrCritInSubAreaId
 * @property null|string $strAttrCritLikeSubAreaName
 * @property null|string $strAttrCritEqualSubAreaName
 * @property null|string[] $tabAttrCritInSubAreaName
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortAreaId
 * @property null|string $strAttrSortAreaName
 * @property null|string $strAttrSortAreaSchemaId
 * @property null|string $strAttrSortAreaSchemaName
 * @property null|string $strAttrSortSchemaRelationId
 * @property null|string $strAttrSortSchemaRelationName
 * @property null|string $strAttrSortSubAreaId
 * @property null|string $strAttrSortSubAreaName
 */
class AreaRelationScopeBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal area id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_AREA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in area id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_AREA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_AREA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like area name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal area name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in area name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal area schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_AREA_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in area schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_AREA_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_AREA_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like area schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_AREA_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_AREA_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal area schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_AREA_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in area schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_AREA_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_AREA_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal schema relation id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in schema relation id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal sub area id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_AREA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_AREA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in sub area id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_AREA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SUB_AREA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like sub area name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SUB_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_SUB_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal sub area name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in sub area name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SUB_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort area id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_AREA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_SORT_AREA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort area name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_SORT_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort area schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_AREA_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_SORT_AREA_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_AREA_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_SORT_AREA_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort schema relation id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_SORT_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_SORT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort sub area id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_SUB_AREA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_SORT_SUB_AREA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort sub area name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_SUB_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAreaRelationScopeBrowser::ATTRIBUTE_ALIAS_SORT_SUB_AREA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigValidDate,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigValidDate,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigValidDate,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigValidDate,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_AREA_ID => $tabRuleConfigValidId,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_AREA_ID => $tabRuleConfigValidTabId,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_AREA_NAME => $tabRuleConfigValidString,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_AREA_NAME => $tabRuleConfigValidString,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_AREA_NAME => $tabRuleConfigValidTabString,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_AREA_SCHEMA_ID => $tabRuleConfigValidId,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_AREA_SCHEMA_ID => $tabRuleConfigValidTabId,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_AREA_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_AREA_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_AREA_SCHEMA_NAME => $tabRuleConfigValidTabString,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_ID => $tabRuleConfigValidId,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_ID => $tabRuleConfigValidTabId,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_RELATION_NAME => $tabRuleConfigValidString,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_NAME => $tabRuleConfigValidString,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_NAME => $tabRuleConfigValidTabString,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_AREA_ID => $tabRuleConfigValidId,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_AREA_ID => $tabRuleConfigValidTabId,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SUB_AREA_NAME => $tabRuleConfigValidString,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_AREA_NAME => $tabRuleConfigValidString,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_AREA_NAME => $tabRuleConfigValidTabString,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigValidSort,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigValidSort,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_AREA_ID => $tabRuleConfigValidSort,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_AREA_NAME => $tabRuleConfigValidSort,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_AREA_SCHEMA_ID => $tabRuleConfigValidSort,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_AREA_SCHEMA_NAME => $tabRuleConfigValidSort,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_ID => $tabRuleConfigValidSort,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_NAME => $tabRuleConfigValidSort,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_SUB_AREA_ID => $tabRuleConfigValidSort,
                ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_SUB_AREA_NAME => $tabRuleConfigValidSort

            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_AREA_ID:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_AREA_SCHEMA_ID:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_ID:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_AREA_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_AREA_ID:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_AREA_SCHEMA_ID:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_ID:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_AREA_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_string($value) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ($value instanceof DateTime) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_AREA_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_AREA_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_AREA_SCHEMA_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_AREA_SCHEMA_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_RELATION_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SUB_AREA_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_AREA_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_AREA_ID:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_AREA_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_AREA_SCHEMA_ID:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_AREA_SCHEMA_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_ID:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_SUB_AREA_ID:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_SORT_SUB_AREA_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_AREA_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_AREA_SCHEMA_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_NAME:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_AREA_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstAreaRelationScopeBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = $this->getAttributeValueFormatGet($strKey, $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}