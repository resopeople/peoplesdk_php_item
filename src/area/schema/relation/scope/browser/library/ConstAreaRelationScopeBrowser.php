<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\area\schema\relation\scope\browser\library;



class ConstAreaRelationScopeBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_EQUAL_AREA_ID = 'intAttrCritEqualAreaId';
    const ATTRIBUTE_KEY_CRIT_IN_AREA_ID = 'tabAttrCritInAreaId';
    const ATTRIBUTE_KEY_CRIT_LIKE_AREA_NAME = 'strAttrCritLikeAreaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_AREA_NAME = 'strAttrCritEqualAreaName';
    const ATTRIBUTE_KEY_CRIT_IN_AREA_NAME = 'tabAttrCritInAreaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_AREA_SCHEMA_ID = 'intAttrCritEqualAreaSchemaId';
    const ATTRIBUTE_KEY_CRIT_IN_AREA_SCHEMA_ID = 'tabAttrCritInAreaSchemaId';
    const ATTRIBUTE_KEY_CRIT_LIKE_AREA_SCHEMA_NAME = 'strAttrCritLikeAreaSchemaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_AREA_SCHEMA_NAME = 'strAttrCritEqualAreaSchemaName';
    const ATTRIBUTE_KEY_CRIT_IN_AREA_SCHEMA_NAME = 'tabAttrCritInAreaSchemaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_ID = 'intAttrCritEqualSchemaRelationId';
    const ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_ID = 'tabAttrCritInSchemaRelationId';
    const ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_RELATION_NAME = 'strAttrCritLikeSchemaRelationName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_NAME = 'strAttrCritEqualSchemaRelationName';
    const ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_NAME = 'tabAttrCritInSchemaRelationName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SUB_AREA_ID = 'intAttrCritEqualSubAreaId';
    const ATTRIBUTE_KEY_CRIT_IN_SUB_AREA_ID = 'tabAttrCritInSubAreaId';
    const ATTRIBUTE_KEY_CRIT_LIKE_SUB_AREA_NAME = 'strAttrCritLikeSubAreaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SUB_AREA_NAME = 'strAttrCritEqualSubAreaName';
    const ATTRIBUTE_KEY_CRIT_IN_SUB_AREA_NAME = 'tabAttrCritInSubAreaName';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_AREA_ID = 'strAttrSortAreaId';
    const ATTRIBUTE_KEY_SORT_AREA_NAME = 'strAttrSortAreaName';
    const ATTRIBUTE_KEY_SORT_AREA_SCHEMA_ID = 'strAttrSortAreaSchemaId';
    const ATTRIBUTE_KEY_SORT_AREA_SCHEMA_NAME = 'strAttrSortAreaSchemaName';
    const ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_ID = 'strAttrSortSchemaRelationId';
    const ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_NAME = 'strAttrSortSchemaRelationName';
    const ATTRIBUTE_KEY_SORT_SUB_AREA_ID = 'strAttrSortSubAreaId';
    const ATTRIBUTE_KEY_SORT_SUB_AREA_NAME = 'strAttrSortSubAreaName';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_ID = 'crit-equal-area-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_AREA_ID = 'crit-in-area-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_AREA_NAME = 'crit-like-area-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_NAME = 'crit-equal-area-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_AREA_NAME = 'crit-in-area-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_SCHEMA_ID = 'crit-equal-area-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_AREA_SCHEMA_ID = 'crit-in-area-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_AREA_SCHEMA_NAME = 'crit-like-area-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_AREA_SCHEMA_NAME = 'crit-equal-area-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_AREA_SCHEMA_NAME = 'crit-in-area-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_RELATION_ID = 'crit-equal-schema-relation-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_RELATION_ID = 'crit-in-schema-relation-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_SCHEMA_RELATION_NAME = 'crit-like-schema-relation-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_RELATION_NAME = 'crit-equal-schema-relation-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_RELATION_NAME = 'crit-in-schema-relation-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_AREA_ID = 'crit-equal-sub-area-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_SUB_AREA_ID = 'crit-in-sub-area-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_SUB_AREA_NAME = 'crit-like-sub-area-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_AREA_NAME = 'crit-equal-sub-area-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_SUB_AREA_NAME = 'crit-in-sub-area-name';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_AREA_ID = 'sort-area-id';
    const ATTRIBUTE_ALIAS_SORT_AREA_NAME = 'sort-area-name';
    const ATTRIBUTE_ALIAS_SORT_AREA_SCHEMA_ID = 'sort-area-schema-id';
    const ATTRIBUTE_ALIAS_SORT_AREA_SCHEMA_NAME = 'sort-area-schema-name';
    const ATTRIBUTE_ALIAS_SORT_SCHEMA_RELATION_ID = 'sort-schema-relation-id';
    const ATTRIBUTE_ALIAS_SORT_SCHEMA_RELATION_NAME = 'sort-schema-relation-name';
    const ATTRIBUTE_ALIAS_SORT_SUB_AREA_ID = 'sort-sub-area-id';
    const ATTRIBUTE_ALIAS_SORT_SUB_AREA_NAME = 'sort-sub-area-name';



}