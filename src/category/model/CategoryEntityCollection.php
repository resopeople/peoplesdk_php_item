<?php
/**
 * This class allows to define category entity collection class.
 * key => CategoryEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\category\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\item\category\model\CategoryEntity;



/**
 * @method null|CategoryEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(CategoryEntity $objEntity) @inheritdoc
 */
class CategoryEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return CategoryEntity::class;
    }



}