<?php
/**
 * This class allows to define item entity class.
 * Item is specific data,
 * following specific schema definition.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\null_value\library\ConstNullValue;
use people_sdk\library\model\entity\null_value\library\ToolBoxNullValue;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\user_profile\user\library\ConstUserProfile;
use people_sdk\user_profile\user\model\UserProfileEntity;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\app_profile\app\library\ConstAppProfile;
use people_sdk\app_profile\app\model\AppProfileEntity;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\item\schema\library\ConstSchema;
use people_sdk\item\schema\model\SchemaEntity;
use people_sdk\item\schema\model\SchemaEntityFactory;
use people_sdk\item\item\attribute\value\model\ItemAttrValueEntity;
use people_sdk\item\item\attribute\value\model\ItemAttrValueEntityFactory;
use people_sdk\item\item\library\ConstItem;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|string $strAttrName
 * @property null|integer|SchemaEntity $attrSchema : schema id|entity
 * @property null|string $strAttrProfileType
 * @property null|string|integer|UserProfileEntity|AppProfileEntity $attrProfile : profile id|entity
 * @property null|ItemAttrValueEntity $objAttrItemAttrValueEntity
 */
class ItemEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: User profile entity factory instance.
     * @var null|UserProfileEntityFactory
     */
    protected $objUserProfileEntityFactory;



    /**
     * DI: Application profile entity factory instance.
     * @var null|AppProfileEntityFactory
     */
    protected $objAppProfileEntityFactory;



    /**
     * DI: Schema entity factory instance.
     * @var null|SchemaEntityFactory
     */
    protected $objSchemaEntityFactory;



    /**
     * DI: Item attribute value entity factory instance.
     * @var null|ItemAttrValueEntityFactory
     */
    protected $objItemAttrValueEntityFactory;



    /**
     * DI: User profile entity factory execution configuration.
     * @var null|array
     */
    protected $tabUserProfileEntityFactoryExecConfig;



    /**
     * DI: Application profile entity factory execution configuration.
     * @var null|array
     */
    protected $tabAppProfileEntityFactoryExecConfig;



    /**
     * DI: Schema entity factory execution configuration.
     * @var null|array
     */
    protected $tabSchemaEntityFactoryExecConfig;



    /**
     * DI: Item attribute value entity factory execution configuration.
     * @var null|array
     */
    protected $tabItemAttrValueEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     * @param null|UserProfileEntityFactory $objUserProfileEntityFactory = null
     * @param null|AppProfileEntityFactory $objAppProfileEntityFactory = null
     * @param null|SchemaEntityFactory $objSchemaEntityFactory = null
     * @param null|ItemAttrValueEntityFactory $objItemAttrValueEntityFactory = null
     * @param null|array $tabUserProfileEntityFactoryExecConfig = null
     * @param null|array $tabAppProfileEntityFactoryExecConfig = null
     * @param null|array $tabSchemaEntityFactoryExecConfig = null
     * @param null|array $tabItemAttrValueEntityFactoryExecConfig = null
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        UserProfileEntityFactory $objUserProfileEntityFactory = null,
        AppProfileEntityFactory $objAppProfileEntityFactory = null,
        SchemaEntityFactory $objSchemaEntityFactory = null,
        ItemAttrValueEntityFactory $objItemAttrValueEntityFactory = null,
        array $tabUserProfileEntityFactoryExecConfig = null,
        array $tabAppProfileEntityFactoryExecConfig = null,
        array $tabSchemaEntityFactoryExecConfig = null,
        array $tabItemAttrValueEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objUserProfileEntityFactory = $objUserProfileEntityFactory;
        $this->objAppProfileEntityFactory = $objAppProfileEntityFactory;
        $this->objSchemaEntityFactory = $objSchemaEntityFactory;
        $this->objItemAttrValueEntityFactory = $objItemAttrValueEntityFactory;
        $this->tabUserProfileEntityFactoryExecConfig = $tabUserProfileEntityFactoryExecConfig;
        $this->tabAppProfileEntityFactoryExecConfig = $tabAppProfileEntityFactoryExecConfig;
        $this->tabSchemaEntityFactoryExecConfig = $tabSchemaEntityFactoryExecConfig;
        $this->tabItemAttrValueEntityFactoryExecConfig = $tabItemAttrValueEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
    * @inheritdoc
    */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstItem::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * Get default attribute item attribute value entity,
     * used on attribute configuration.
     *
     * @return null|ItemAttrValueEntity
     */
    protected function getObjDefaultAttrItemAttrValueEntity()
    {
        // Return result
        return (
            (!is_null($this->objItemAttrValueEntityFactory)) ?
                $this
                    ->objItemAttrValueEntityFactory
                    ->getObjEntity(
                        array(),
                        $this->tabItemAttrValueEntityFactoryExecConfig
                    ) :
                null
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstItem::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstItem::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstItem::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstItem::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstItem::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstItem::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstItem::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstItem::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstItem::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstItem::ATTRIBUTE_KEY_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstItem::ATTRIBUTE_ALIAS_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstItem::ATTRIBUTE_NAME_SAVE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute schema
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstItem::ATTRIBUTE_KEY_SCHEMA,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstItem::ATTRIBUTE_ALIAS_SCHEMA,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstItem::ATTRIBUTE_NAME_SAVE_SCHEMA,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile type
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstItem::ATTRIBUTE_KEY_PROFILE_TYPE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstItem::ATTRIBUTE_ALIAS_PROFILE_TYPE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstItem::ATTRIBUTE_NAME_SAVE_PROFILE_TYPE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute profile
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstItem::ATTRIBUTE_KEY_PROFILE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstItem::ATTRIBUTE_ALIAS_PROFILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstItem::ATTRIBUTE_NAME_SAVE_PROFILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item attribute value entity
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstItem::ATTRIBUTE_KEY_ITEM_ATTR_VALUE_ENTITY,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstItem::ATTRIBUTE_ALIAS_ITEM_ATTR_VALUE_ENTITY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstItem::ATTRIBUTE_NAME_SAVE_ITEM_ATTR_VALUE_ENTITY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE =>
                    $this->getObjDefaultAttrItemAttrValueEntity()
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidSchema = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-id' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ],
                        'is-valid-entity' => [
                            [
                                'type_object',
                                [
                                    'class_path' => [SchemaEntity::class]
                                ]
                            ],
                            [
                                'validation_entity',
                                [
                                    'attribute_key' => ConstSchema::ATTRIBUTE_KEY_ID
                                ]
                            ],
                            [
                                'new_entity',
                                [
                                    'new_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be a valid schema entity or ID.'
                ]
            ]
        );
        $tabProfileType = ConstItem::getTabProfileType();
        $strTabProfileType = implode(', ', array_map(
            function($value) {return sprintf('\'%1$s\'', $value);},
            $tabProfileType
        ));
        $tabRuleConfigValidProfileType = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-null' => [
                            [
                                'compare_equal',
                                ['compare_value' => ConstNullValue::NULL_VALUE]
                            ]
                        ],
                        'is-valid-profile-type' => [
                            [
                                'compare_in',
                                [
                                    'compare_value' => $tabProfileType
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => sprintf(
                        '%1$s must be null or a valid profile type in (%2$s) else.',
                        '%1$s',
                        $strTabProfileType
                    )
                ]
            ]
        );
        $tabRuleConfigValidProfile = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-null' => [
                            [
                                'compare_equal',
                                ['compare_value' => ConstNullValue::NULL_VALUE]
                            ]
                        ],
                        'is-valid-id' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ],
                        'is-valid-user-profile-entity' => [
                            [
                                'callable',
                                [
                                    'valid_callable' => function() {
                                        $strProfileType = $this->getAttributeValue(ConstItem::ATTRIBUTE_KEY_PROFILE_TYPE, false);
                                        return (
                                            is_string($strProfileType) &&
                                            ($strProfileType === ConstItem::PROFILE_TYPE_USER_PROFILE)
                                        );
                                    }
                                ]
                            ],
                            [
                                'type_object',
                                [
                                    'class_path' => [UserProfileEntity::class]
                                ]
                            ],
                            [
                                'validation_entity',
                                [
                                    'attribute_key' => ConstUserProfile::ATTRIBUTE_KEY_ID
                                ]
                            ],
                            [
                                'new_entity',
                                [
                                    'new_require' => false
                                ]
                            ]
                        ],
                        'is-valid-app-profile-entity' => [
                            [
                                'callable',
                                [
                                    'valid_callable' => function() {
                                        $strProfileType = $this->getAttributeValue(ConstItem::ATTRIBUTE_KEY_PROFILE_TYPE, false);
                                        return (
                                            is_string($strProfileType) &&
                                            ($strProfileType === ConstItem::PROFILE_TYPE_APP_PROFILE)
                                        );
                                    }
                                ]
                            ],
                            [
                                'type_object',
                                [
                                    'class_path' => [AppProfileEntity::class]
                                ]
                            ],
                            [
                                'validation_entity',
                                [
                                    'attribute_key' => ConstAppProfile::ATTRIBUTE_KEY_ID
                                ]
                            ],
                            [
                                'new_entity',
                                [
                                    'new_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a valid user/application profile entity or ID, in accordance with specified profile type.'
                ]
            ]
        );


        // Init var
        $result = array(
            ConstItem::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstItem::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstItem::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstItem::ATTRIBUTE_KEY_NAME => $tabRuleConfigValidString,
            ConstItem::ATTRIBUTE_KEY_SCHEMA => $tabRuleConfigValidSchema,
            ConstItem::ATTRIBUTE_KEY_PROFILE_TYPE => $tabRuleConfigValidProfileType,
            ConstItem::ATTRIBUTE_KEY_PROFILE => $tabRuleConfigValidProfile,
            ConstItem::ATTRIBUTE_KEY_ITEM_ATTR_VALUE_ENTITY => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-entity' => [
                                [
                                    'type_object',
                                    [
                                        'class_path' => [ItemAttrValueEntity::class]
                                    ]
                                ],
                                ['validation_entity']
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a valid item attribute value entity.'
                    ]
                ]
            ]
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstItem::ATTRIBUTE_KEY_DT_CREATE:
            case ConstItem::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstItem::ATTRIBUTE_KEY_ID:
            case ConstItem::ATTRIBUTE_KEY_SCHEMA:
            case ConstItem::ATTRIBUTE_KEY_PROFILE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstItem::ATTRIBUTE_KEY_DT_CREATE:
            case ConstItem::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_string($value) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ($value instanceof DateTime) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstItem::ATTRIBUTE_KEY_NAME:
            case ConstItem::ATTRIBUTE_KEY_PROFILE_TYPE:
            case ConstItem::ATTRIBUTE_KEY_ITEM_ATTR_VALUE_ENTITY:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstItem::ATTRIBUTE_KEY_DT_CREATE:
            case ConstItem::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstItem::ATTRIBUTE_KEY_SCHEMA:
                $result = array(
                    ConstSchema::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof SchemaEntity) ?
                            $value->getAttributeValueSave(ConstSchema::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            case ConstItem::ATTRIBUTE_KEY_PROFILE_TYPE:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatGet($value);
                break;

            case ConstItem::ATTRIBUTE_KEY_PROFILE:
                $strProfileType = $this->getAttributeValue(ConstItem::ATTRIBUTE_KEY_PROFILE_TYPE);
                $result = (
                    (
                        is_string($strProfileType) &&
                        ($strProfileType == ConstItem::PROFILE_TYPE_USER_PROFILE) &&
                        (($value instanceof UserProfileEntity) || is_int($value))
                    ) ?
                        array(
                            ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID => (
                                ($value instanceof UserProfileEntity) ?
                                    $value->getAttributeValueSave(ConstUserProfile::ATTRIBUTE_KEY_ID) :
                                    $value
                            )
                        ) :
                        (
                            (
                                is_string($strProfileType) &&
                                ($strProfileType === ConstItem::PROFILE_TYPE_APP_PROFILE) &&
                                (($value instanceof AppProfileEntity) || is_int($value))
                            ) ?
                                array(
                                    ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID => (
                                        ($value instanceof AppProfileEntity) ?
                                            $value->getAttributeValueSave(ConstAppProfile::ATTRIBUTE_KEY_ID) :
                                            $value
                                    )
                                ) :
                                ToolBoxNullValue::getAttributeValueSaveFormatGet($value)
                        )
                );
                break;

            case ConstItem::ATTRIBUTE_KEY_ITEM_ATTR_VALUE_ENTITY:
                $result = (
                    ($value instanceof ItemAttrValueEntity) ?
                        $value->getTabDataSave() :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objUserProfileEntityFactory = $this->objUserProfileEntityFactory;
        $objAppProfileEntityFactory = $this->objAppProfileEntityFactory;
        $objSchemaEntityFactory = $this->objSchemaEntityFactory;
        $objItemAttrValueEntityFactory = $this->objItemAttrValueEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstItem::ATTRIBUTE_KEY_DT_CREATE:
            case ConstItem::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    is_string($value) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstItem::ATTRIBUTE_KEY_PROFILE_TYPE:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatSet($value);
                break;

            case ConstItem::ATTRIBUTE_KEY_SCHEMA:
                $result = $value;
                if((!is_null($objSchemaEntityFactory)) && is_array($value))
                {
                    $objSchemaEntity = $objSchemaEntityFactory->getObjEntity(
                        array(),
                        // Try to select schema entity, by id, if required
                        (
                            (!is_null($this->tabSchemaEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabSchemaEntityFactoryExecConfig
                                        ) :
                                        $this->tabSchemaEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objSchemaEntity->hydrateSave($value))
                    {
                        $objSchemaEntity->setIsNew(false);
                        $result = $objSchemaEntity;
                    }
                }
                else if(isset($value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstSchema::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            case ConstItem::ATTRIBUTE_KEY_PROFILE:
                $strProfileType = $this->getAttributeValue(ConstItem::ATTRIBUTE_KEY_PROFILE_TYPE);
                $result = ToolBoxNullValue::getAttributeValueSaveFormatSet($value);
                // Try to set user profile, if required
                if(is_string($strProfileType) && ($strProfileType === ConstItem::PROFILE_TYPE_USER_PROFILE))
                {
                    if((!is_null($objUserProfileEntityFactory)) && is_array($value))
                    {
                        $objUserProfileEntity = $objUserProfileEntityFactory->getObjEntity(
                            array(),
                            // Try to select user profile entity, by id, if required
                            (
                                (!is_null($this->tabUserProfileEntityFactoryExecConfig)) ?
                                    (
                                        isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array_merge(
                                                array(
                                                    ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                        $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]
                                                ),
                                                $this->tabUserProfileEntityFactoryExecConfig
                                            ) :
                                            $this->tabUserProfileEntityFactoryExecConfig
                                    ) :
                                    (
                                        isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]
                                            ) :
                                            null
                                    )
                            )
                        );
                        if($objUserProfileEntity->hydrateSave($value))
                        {
                            $objUserProfileEntity->setIsNew(false);
                            $result = $objUserProfileEntity;
                        }
                    }
                    else if(isset($value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID]))
                    {
                        $result = $value[ConstUserProfile::ATTRIBUTE_NAME_SAVE_ID];
                    }
                }
                // Try to set application profile, if required
                else if(is_string($strProfileType) && ($strProfileType == ConstItem::PROFILE_TYPE_APP_PROFILE))
                {
                    if((!is_null($objAppProfileEntityFactory)) && is_array($value))
                    {
                        $objAppProfileEntity = $objAppProfileEntityFactory->getObjEntity(
                            array(),
                            // Try to select application profile entity, by id, if required
                            (
                                (!is_null($this->tabAppProfileEntityFactoryExecConfig)) ?
                                    (
                                        isset($value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array_merge(
                                                array(
                                                    ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                        $value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]
                                                ),
                                                $this->tabAppProfileEntityFactoryExecConfig
                                            ) :
                                            $this->tabAppProfileEntityFactoryExecConfig
                                    ) :
                                    (
                                        isset($value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]
                                            ) :
                                            null
                                    )
                            )
                        );
                        if($objAppProfileEntity->hydrateSave($value))
                        {
                            $objAppProfileEntity->setIsNew(false);
                            $result = $objAppProfileEntity;
                        }
                    }
                    else if(isset($value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]))
                    {
                        $result = $value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID];
                    }
                }
                break;

            case ConstItem::ATTRIBUTE_KEY_ITEM_ATTR_VALUE_ENTITY:
                $result = $value;
                if((!is_null($objItemAttrValueEntityFactory)) && is_array($value))
                {
                    $objItemAttrValueEntity = $objItemAttrValueEntityFactory->getObjEntity(
                        array(),
                        $this->tabItemAttrValueEntityFactoryExecConfig
                    );

                    // Get data (filter required due to potential different schema)
                    $tabIncludeKey = array_map(
                        function($strKey) use ($objItemAttrValueEntity) {
                            return $objItemAttrValueEntity->getAttributeNameSave($strKey);
                        },
                        $objItemAttrValueEntity->getTabAttributeKey()
                    );
                    $tabData = array_filter(
                        $value,
                        function($key) use ($tabIncludeKey) {
                            return (
                                (!is_string($key)) ||
                                in_array($key, $tabIncludeKey)
                            );
                        },
                        ARRAY_FILTER_USE_KEY
                    );

                    // Hydrate item attribute value entity, from data
                    if($objItemAttrValueEntity->hydrateSave($tabData))
                    {
                        $objItemAttrValueEntity->setIsNew(false);
                        $result = $objItemAttrValueEntity;
                    }
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}