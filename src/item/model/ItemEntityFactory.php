<?php
/**
 * This class allows to define item entity factory class.
 * Item entity factory allows to provide new item entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\item\schema\model\SchemaEntityFactory;
use people_sdk\item\item\attribute\value\model\ItemAttrValueEntityFactory;
use people_sdk\item\item\library\ConstItem;
use people_sdk\item\item\exception\UserProfileEntityFactoryInvalidFormatException;
use people_sdk\item\item\exception\AppProfileEntityFactoryInvalidFormatException;
use people_sdk\item\item\exception\SchemaEntityFactoryInvalidFormatException;
use people_sdk\item\item\exception\ItemAttrValueEntityFactoryInvalidFormatException;
use people_sdk\item\item\exception\UserProfileEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\item\exception\AppProfileEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\item\exception\SchemaEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\item\exception\ItemAttrValueEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\item\model\ItemEntity;



/**
 * @method null|UserProfileEntityFactory getObjUserProfileEntityFactory() Get user profile entity factory object.
 * @method null|AppProfileEntityFactory getObjAppProfileEntityFactory() Get application profile entity factory object.
 * @method null|SchemaEntityFactory getObjSchemaEntityFactory() Get schema entity factory object.
 * @method null|ItemAttrValueEntityFactory getObjItemAttrValueEntityFactory() Get item attribute value entity factory object.
 * @method null|array getTabUserProfileEntityFactoryExecConfig() Get user profile entity factory execution configuration array.
 * @method null|array getTabAppProfileEntityFactoryExecConfig() Get application profile entity factory execution configuration array.
 * @method null|array getTabSchemaEntityFactoryExecConfig() Get schema entity factory execution configuration array.
 * @method null|array getTabItemAttrValueEntityFactoryExecConfig() Get item attribute value entity factory execution configuration array.
 * @method ItemEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjUserProfileEntityFactory(null|UserProfileEntityFactory $objUserProfileEntityFactory) Set user profile entity factory object.
 * @method void setObjAppProfileEntityFactory(null|AppProfileEntityFactory $objAppProfileEntityFactory) Set application profile entity factory object.
 * @method void setObjSchemaEntityFactory(null|SchemaEntityFactory $objSchemaEntityFactory) Set schema entity factory object.
 * @method void setObjItemAttrValueEntityFactory(null|ItemAttrValueEntityFactory $objItemAttrValueEntityFactory) Set item attribute value entity factory object.
 * @method void setTabUserProfileEntityFactoryExecConfig(null|array $tabUserProfileEntityFactoryExecConfig) Set user profile entity factory execution configuration array.
 * @method void setTabAppProfileEntityFactoryExecConfig(null|array $tabAppProfileEntityFactoryExecConfig) Set application profile entity factory execution configuration array.
 * @method void setTabSchemaEntityFactoryExecConfig(null|array $tabSchemaEntityFactoryExecConfig) Set schema entity factory execution configuration array.
 * @method void setTabItemAttrValueEntityFactoryExecConfig(null|array $tabItemAttrValueEntityFactoryExecConfig) Set item attribute value entity factory execution configuration array.
 */
class ItemEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|UserProfileEntityFactory $objUserProfileEntityFactory = null
     * @param null|AppProfileEntityFactory $objAppProfileEntityFactory = null
     * @param null|SchemaEntityFactory $objSchemaEntityFactory = null
     * @param null|ItemAttrValueEntityFactory $objItemAttrValueEntityFactory = null
     * @param null|array $tabUserProfileEntityFactoryExecConfig = null
     * @param null|array $tabAppProfileEntityFactoryExecConfig = null
     * @param null|array $tabSchemaEntityFactoryExecConfig = null
     * @param null|array $tabItemAttrValueEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        UserProfileEntityFactory $objUserProfileEntityFactory = null,
        AppProfileEntityFactory $objAppProfileEntityFactory = null,
        SchemaEntityFactory $objSchemaEntityFactory = null,
        ItemAttrValueEntityFactory $objItemAttrValueEntityFactory = null,
        array $tabUserProfileEntityFactoryExecConfig = null,
        array $tabAppProfileEntityFactoryExecConfig = null,
        array $tabSchemaEntityFactoryExecConfig = null,
        array $tabItemAttrValueEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init user profile entity factory
        $this->setObjUserProfileEntityFactory($objUserProfileEntityFactory);

        // Init application profile entity factory
        $this->setObjAppProfileEntityFactory($objAppProfileEntityFactory);

        // Init schema entity factory
        $this->setObjSchemaEntityFactory($objSchemaEntityFactory);

        // Init item attribute value entity factory
        $this->setObjItemAttrValueEntityFactory($objItemAttrValueEntityFactory);

        // Init user profile entity factory execution config
        $this->setTabUserProfileEntityFactoryExecConfig($tabUserProfileEntityFactoryExecConfig);

        // Init application profile entity factory execution config
        $this->setTabAppProfileEntityFactoryExecConfig($tabAppProfileEntityFactoryExecConfig);

        // Init schema entity factory execution config
        $this->setTabSchemaEntityFactoryExecConfig($tabSchemaEntityFactoryExecConfig);

        // Init item attribute value entity factory execution config
        $this->setTabItemAttrValueEntityFactoryExecConfig($tabItemAttrValueEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstItem::DATA_KEY_USER_PROFILE_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstItem::DATA_KEY_USER_PROFILE_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstItem::DATA_KEY_APP_PROFILE_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstItem::DATA_KEY_APP_PROFILE_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstItem::DATA_KEY_SCHEMA_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstItem::DATA_KEY_SCHEMA_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstItem::DATA_KEY_ITEM_ATTR_VALUE_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstItem::DATA_KEY_ITEM_ATTR_VALUE_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstItem::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstItem::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstItem::DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstItem::DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstItem::DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstItem::DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstItem::DATA_KEY_ITEM_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstItem::DATA_KEY_ITEM_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstItem::DATA_KEY_USER_PROFILE_ENTITY_FACTORY,
            ConstItem::DATA_KEY_APP_PROFILE_ENTITY_FACTORY,
            ConstItem::DATA_KEY_SCHEMA_ENTITY_FACTORY,
            ConstItem::DATA_KEY_ITEM_ATTR_VALUE_ENTITY_FACTORY,
            ConstItem::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG,
            ConstItem::DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG,
            ConstItem::DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG,
            ConstItem::DATA_KEY_ITEM_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstItem::DATA_KEY_USER_PROFILE_ENTITY_FACTORY:
                    UserProfileEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstItem::DATA_KEY_APP_PROFILE_ENTITY_FACTORY:
                    AppProfileEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstItem::DATA_KEY_SCHEMA_ENTITY_FACTORY:
                    SchemaEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstItem::DATA_KEY_ITEM_ATTR_VALUE_ENTITY_FACTORY:
                    ItemAttrValueEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstItem::DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG:
                    UserProfileEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstItem::DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG:
                    AppProfileEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstItem::DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG:
                    SchemaEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstItem::DATA_KEY_ITEM_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG:
                    ItemAttrValueEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => ItemEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstItem::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new ItemEntity(
            $objDateTimeFactory,
            array(),
            $objValidator,
            $this->getObjUserProfileEntityFactory(),
            $this->getObjAppProfileEntityFactory(),
            $this->getObjSchemaEntityFactory(),
            $this->getObjItemAttrValueEntityFactory(),
            $this->getTabUserProfileEntityFactoryExecConfig(),
            $this->getTabAppProfileEntityFactoryExecConfig(),
            $this->getTabSchemaEntityFactoryExecConfig(),
            $this->getTabItemAttrValueEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}