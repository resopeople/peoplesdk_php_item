<?php
/**
 * This class allows to define item entity collection class.
 * key => ItemEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\item\item\model\ItemEntity;



/**
 * @method null|ItemEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(ItemEntity $objEntity) @inheritdoc
 */
class ItemEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return ItemEntity::class;
    }



}