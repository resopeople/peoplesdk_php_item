<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\attribute\value\library;



class ConstItemAttrValue
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_SCHEMA_ATTR_PROVIDER = 'objSchemaAttrProvider';



    // Exception message constants
    const EXCEPT_MSG_SCHEMA_ATTR_PROVIDER_INVALID_FORMAT =
        'Following schema attribute provider "%1$s" invalid! It must be a schema attribute provider object.';



}