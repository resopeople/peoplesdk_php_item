<?php
/**
 * This class allows to define item attribute value entity factory class.
 * Item attribute value entity factory allows to provide new item attribute value entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\attribute\value\model;

use liberty_code\model\entity\factory\fix\model\FixEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use people_sdk\item\schema\attribute\provider\model\SchemaAttrProvider;
use people_sdk\item\item\attribute\value\library\ConstItemAttrValue;
use people_sdk\item\item\attribute\value\exception\SchemaAttrProviderInvalidFormatException;
use people_sdk\item\item\attribute\value\model\ItemAttrValueEntity;



/**
 * @method SchemaAttrProvider getObjSchemaAttrProvider() Get schema attribute provider object.
 * @method void setObjSchemaAttrProvider(SchemaAttrProvider $objSchemaAttrProvider) Set schema attribute provider object.
 * @method ItemAttrValueEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 */
class ItemAttrValueEntityFactory extends FixEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************

	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SchemaAttrProvider $objSchemaAttrProvider
     */
    public function __construct(
        ProviderInterface $objProvider,
        SchemaAttrProvider $objSchemaAttrProvider
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init schema attribute provider
        $this->setObjSchemaAttrProvider($objSchemaAttrProvider);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstItemAttrValue::DATA_KEY_SCHEMA_ATTR_PROVIDER))
        {
            $this->__beanTabData[ConstItemAttrValue::DATA_KEY_SCHEMA_ATTR_PROVIDER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstItemAttrValue::DATA_KEY_SCHEMA_ATTR_PROVIDER,
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstItemAttrValue::DATA_KEY_SCHEMA_ATTR_PROVIDER:
                    SchemaAttrProviderInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => ItemAttrValueEntity::class
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objAttrProvider = $this->getObjSchemaAttrProvider();
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $result = new ItemAttrValueEntity(
            $objAttrProvider,
            array(),
            $objValidator
        );

        // Return result
        return $result;
    }



}