<?php
/**
 * This class allows to define item category browser entity class.
 * Item category browser entity allows to define attributes,
 * to search item category entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\category\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\item\item\category\browser\library\ConstItemCategoryBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|integer $intAttrCritEqualItemId
 * @property null|integer[] $tabAttrCritInItemId
 * @property null|string $strAttrCritLikeItemName
 * @property null|string $strAttrCritEqualItemName
 * @property null|string[] $tabAttrCritInItemName
 * @property null|boolean $boolAttrCritIsItemCurrentUserProfile
 * @property null|integer $intAttrCritEqualCategoryId
 * @property null|integer[] $tabAttrCritInCategoryId
 * @property null|string $strAttrCritLikeCategoryName
 * @property null|string $strAttrCritEqualCategoryName
 * @property null|string[] $tabAttrCritInCategoryName
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortItemId
 * @property null|string $strAttrSortItemName
 * @property null|string $strAttrSortCategoryId
 * @property null|string $strAttrSortCategoryName
 */
class ItemCategoryBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal item id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in item id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like item name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_LIKE_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal item name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in item name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item current user profile
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal category id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in category id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_IN_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like category name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_LIKE_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal category name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in category name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_CRIT_IN_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort item id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_SORT_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort item name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_SORT_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort category id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_SORT_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort category name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemCategoryBrowser::ATTRIBUTE_ALIAS_SORT_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigValidDate,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigValidDate,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigValidDate,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigValidDate,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_ID => $tabRuleConfigValidId,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_ID => $tabRuleConfigValidTabId,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_LIKE_ITEM_NAME => $tabRuleConfigValidString,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_NAME => $tabRuleConfigValidString,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_NAME => $tabRuleConfigValidTabString,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CURRENT_USER_PROFILE => $tabRuleConfigValidBoolean,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_ID => $tabRuleConfigValidId,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_CATEGORY_ID => $tabRuleConfigValidTabId,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_LIKE_CATEGORY_NAME => $tabRuleConfigValidString,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_NAME => $tabRuleConfigValidString,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_CATEGORY_NAME => $tabRuleConfigValidTabString,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigValidSort,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigValidSort,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_ITEM_ID => $tabRuleConfigValidSort,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_ITEM_NAME => $tabRuleConfigValidSort,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_CATEGORY_ID => $tabRuleConfigValidSort,
                ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_CATEGORY_NAME => $tabRuleConfigValidSort
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_ID:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_ID:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_CATEGORY_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_string($value) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ($value instanceof DateTime) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_LIKE_ITEM_NAME:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_NAME:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_LIKE_CATEGORY_NAME:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_NAME:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_ITEM_ID:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_ITEM_NAME:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_CATEGORY_ID:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_SORT_CATEGORY_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_NAME:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IN_CATEGORY_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CURRENT_USER_PROFILE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = $this->getAttributeValueFormatGet($strKey, $value);
                break;

            case ConstItemCategoryBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CURRENT_USER_PROFILE:
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}