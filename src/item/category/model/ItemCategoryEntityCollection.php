<?php
/**
 * This class allows to define item category entity collection class.
 * key => ItemCategoryEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\category\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\item\item\category\model\ItemCategoryEntity;



/**
 * @method null|ItemCategoryEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(ItemCategoryEntity $objEntity) @inheritdoc
 */
class ItemCategoryEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return ItemCategoryEntity::class;
    }



}