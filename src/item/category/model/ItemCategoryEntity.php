<?php
/**
 * This class allows to define item category entity class.
 * Item category allows to classify specific item, on specific category.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\category\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\item\category\library\ConstCategory;
use people_sdk\item\category\model\CategoryEntity;
use people_sdk\item\category\model\CategoryEntityFactory;
use people_sdk\item\item\library\ConstItem;
use people_sdk\item\item\model\ItemEntity;
use people_sdk\item\item\model\ItemEntityFactory;
use people_sdk\item\item\category\library\ConstItemCategory;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|integer|ItemEntity $attrItem : item id|entity
 * @property null|integer|CategoryEntity $attrCategory : category id|entity
 */
class ItemCategoryEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: Category entity factory instance.
     * @var null|CategoryEntityFactory
     */
    protected $objCategoryEntityFactory;



    /**
     * DI: Item entity factory instance.
     * @var null|ItemEntityFactory
     */
    protected $objItemEntityFactory;



    /**
     * DI: Category entity factory execution configuration.
     * @var null|array
     */
    protected $tabCategoryEntityFactoryExecConfig;



    /**
     * DI: Item entity factory execution configuration.
     * @var null|array
     */
    protected $tabItemEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     * @param null|CategoryEntityFactory $objCategoryEntityFactory = null
     * @param null|ItemEntityFactory $objItemEntityFactory = null
     * @param null|array $tabCategoryEntityFactoryExecConfig = null
     * @param null|array $tabItemEntityFactoryExecConfig = null
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        CategoryEntityFactory $objCategoryEntityFactory = null,
        ItemEntityFactory $objItemEntityFactory = null,
        array $tabCategoryEntityFactoryExecConfig = null,
        array $tabItemEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objCategoryEntityFactory = $objCategoryEntityFactory;
        $this->objItemEntityFactory = $objItemEntityFactory;
        $this->tabCategoryEntityFactoryExecConfig = $tabCategoryEntityFactoryExecConfig;
        $this->tabItemEntityFactoryExecConfig = $tabItemEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
    * @inheritdoc
    */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstItemCategory::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstItemCategory::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstItemCategory::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstItemCategory::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstItemCategory::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstItemCategory::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstItemCategory::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstItemCategory::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstItemCategory::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstItemCategory::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute item
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstItemCategory::ATTRIBUTE_KEY_ITEM,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstItemCategory::ATTRIBUTE_ALIAS_ITEM,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstItemCategory::ATTRIBUTE_NAME_SAVE_ITEM,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute category
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstItemCategory::ATTRIBUTE_KEY_CATEGORY,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstItemCategory::ATTRIBUTE_ALIAS_CATEGORY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstItemCategory::ATTRIBUTE_NAME_SAVE_CATEGORY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidItem = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-id' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ],
                        'is-valid-entity' => [
                            [
                                'type_object',
                                [
                                    'class_path' => [ItemEntity::class]
                                ]
                            ],
                            [
                                'validation_entity',
                                [
                                    'attribute_key' => ConstItem::ATTRIBUTE_KEY_ID
                                ]
                            ],
                            [
                                'new_entity',
                                [
                                    'new_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be a valid item entity or ID.'
                ]
            ]
        );
        $tabRuleConfigValidCategory = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-id' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ],
                        'is-valid-entity' => [
                            [
                                'type_object',
                                [
                                    'class_path' => [CategoryEntity::class]
                                ]
                            ],
                            [
                                'validation_entity',
                                [
                                    'attribute_key' => ConstCategory::ATTRIBUTE_KEY_ID
                                ]
                            ],
                            [
                                'new_entity',
                                [
                                    'new_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be a valid category entity or ID.'
                ]
            ]
        );

        // Init var
        $result = array(
            ConstItemCategory::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstItemCategory::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstItemCategory::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstItemCategory::ATTRIBUTE_KEY_ITEM => $tabRuleConfigValidItem,
            ConstItemCategory::ATTRIBUTE_KEY_CATEGORY => $tabRuleConfigValidCategory
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstItemCategory::ATTRIBUTE_KEY_DT_CREATE:
            case ConstItemCategory::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstItemCategory::ATTRIBUTE_KEY_ID:
            case ConstItemCategory::ATTRIBUTE_KEY_ITEM:
            case ConstItemCategory::ATTRIBUTE_KEY_CATEGORY:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstItemCategory::ATTRIBUTE_KEY_DT_CREATE:
            case ConstItemCategory::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_string($value) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ($value instanceof DateTime) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstItemCategory::ATTRIBUTE_KEY_DT_CREATE:
            case ConstItemCategory::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstItemCategory::ATTRIBUTE_KEY_ITEM:
                $result = (
                    ($value instanceof ItemEntity) ?
                        $value->getAttributeValueSave(ConstItem::ATTRIBUTE_KEY_ID) :
                        $value
                );
                break;

            case ConstItemCategory::ATTRIBUTE_KEY_CATEGORY:
                $result = array(
                    ConstCategory::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof CategoryEntity) ?
                            $value->getAttributeValueSave(ConstCategory::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objCategoryEntityFactory = $this->objCategoryEntityFactory;
        $objItemEntityFactory = $this->objItemEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstItemCategory::ATTRIBUTE_KEY_DT_CREATE:
            case ConstItemCategory::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    is_string($value) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstItemCategory::ATTRIBUTE_KEY_ITEM:
                $result = $value;
                if((!is_null($objItemEntityFactory)) && is_array($value))
                {
                    $objItemEntity = $objItemEntityFactory->getObjEntity(
                        array(),
                        // Try to select item entity, by id, if required
                        (
                            (!is_null($this->tabItemEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstItem::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstItem::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabItemEntityFactoryExecConfig
                                        ) :
                                        $this->tabItemEntityFactoryExecConfig
                                ) :
                                (
                                isset($value[ConstItem::ATTRIBUTE_NAME_SAVE_ID]) ?
                                    array(
                                        ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                            $value[ConstItem::ATTRIBUTE_NAME_SAVE_ID]
                                    ) :
                                    null
                                )
                        )
                    );
                    if($objItemEntity->hydrateSave($value))
                    {
                        $objItemEntity->setIsNew(false);
                        $result = $objItemEntity;
                    }
                }
                else if(isset($value[ConstItem::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstItem::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            case ConstItemCategory::ATTRIBUTE_KEY_CATEGORY:
                $result = $value;
                if((!is_null($objCategoryEntityFactory)) && is_array($value))
                {
                    $objCategoryEntity = $objCategoryEntityFactory->getObjEntity(
                        array(),
                        // Try to select category entity, by id, if required
                        (
                            (!is_null($this->tabCategoryEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstCategory::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstCategory::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabCategoryEntityFactoryExecConfig
                                        ) :
                                        $this->tabCategoryEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstCategory::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstCategory::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objCategoryEntity->hydrateSave($value))
                    {
                        $objCategoryEntity->setIsNew(false);
                        $result = $objCategoryEntity;
                    }
                }
                else if(isset($value[ConstCategory::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstCategory::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}