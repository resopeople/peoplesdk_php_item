<?php
/**
 * This class allows to define item category entity factory class.
 * Item category entity factory allows to provide new item category entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\category\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\item\category\model\CategoryEntityFactory;
use people_sdk\item\item\model\ItemEntityFactory;
use people_sdk\item\item\category\library\ConstItemCategory;
use people_sdk\item\item\category\exception\CategoryEntityFactoryInvalidFormatException;
use people_sdk\item\item\category\exception\ItemEntityFactoryInvalidFormatException;
use people_sdk\item\item\category\exception\CategoryEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\item\category\exception\ItemEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\item\category\model\ItemCategoryEntity;



/**
 * @method null|CategoryEntityFactory getObjCategoryEntityFactory() Get category entity factory object.
 * @method null|ItemEntityFactory getObjItemEntityFactory() Get item entity factory object.
 * @method null|array getTabCategoryEntityFactoryExecConfig() Get category entity factory execution configuration array.
 * @method null|array getTabItemEntityFactoryExecConfig() Get item entity factory execution configuration array.
 * @method ItemCategoryEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjCategoryEntityFactory(null|CategoryEntityFactory $objCategoryEntityFactory) Set category entity factory object.
 * @method void setObjItemEntityFactory(null|ItemEntityFactory $objItemEntityFactory) Set item entity factory object.
 * @method void setTabCategoryEntityFactoryExecConfig(null|array $tabCategoryEntityFactoryExecConfig) Set category entity factory execution configuration array.
 * @method void setTabItemEntityFactoryExecConfig(null|array $tabItemEntityFactoryExecConfig) Set item entity factory execution configuration array.
 */
class ItemCategoryEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|CategoryEntityFactory $objCategoryEntityFactory = null
     * @param null|ItemEntityFactory $objItemEntityFactory = null
     * @param null|array $tabCategoryEntityFactoryExecConfig = null
     * @param null|array $tabItemEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        CategoryEntityFactory $objCategoryEntityFactory = null,
        ItemEntityFactory $objItemEntityFactory = null,
        array $tabCategoryEntityFactoryExecConfig = null,
        array $tabItemEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init category entity factory
        $this->setObjCategoryEntityFactory($objCategoryEntityFactory);

        // Init item entity factory
        $this->setObjItemEntityFactory($objItemEntityFactory);

        // Init category entity factory execution config
        $this->setTabCategoryEntityFactoryExecConfig($tabCategoryEntityFactoryExecConfig);

        // Init item entity factory execution config
        $this->setTabItemEntityFactoryExecConfig($tabItemEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstItemCategory::DATA_KEY_CATEGORY_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstItemCategory::DATA_KEY_CATEGORY_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstItemCategory::DATA_KEY_ITEM_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstItemCategory::DATA_KEY_ITEM_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstItemCategory::DATA_KEY_CATEGORY_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstItemCategory::DATA_KEY_CATEGORY_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstItemCategory::DATA_KEY_ITEM_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstItemCategory::DATA_KEY_ITEM_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstItemCategory::DATA_KEY_CATEGORY_ENTITY_FACTORY,
            ConstItemCategory::DATA_KEY_ITEM_ENTITY_FACTORY,
            ConstItemCategory::DATA_KEY_CATEGORY_ENTITY_FACTORY_EXEC_CONFIG,
            ConstItemCategory::DATA_KEY_ITEM_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstItemCategory::DATA_KEY_CATEGORY_ENTITY_FACTORY:
                    CategoryEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstItemCategory::DATA_KEY_ITEM_ENTITY_FACTORY:
                    ItemEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstItemCategory::DATA_KEY_CATEGORY_ENTITY_FACTORY_EXEC_CONFIG:
                    CategoryEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstItemCategory::DATA_KEY_ITEM_ENTITY_FACTORY_EXEC_CONFIG:
                    ItemEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => ItemCategoryEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstItemCategory::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new ItemCategoryEntity(
            $objDateTimeFactory,
            array(),
            $objValidator,
            $this->getObjCategoryEntityFactory(),
            $this->getObjItemEntityFactory(),
            $this->getTabCategoryEntityFactoryExecConfig(),
            $this->getTabItemEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}