<?php
/**
 * This class allows to define item category entity multi collection repository class.
 * Item category entity multi collection repository is multi collection repository,
 * allows to prepare data from item category entity collection, to save in requisition persistence.
 * Specified requisition persistence must be able to use HTTP client, persistor HTTP request and persistor HTTP response, with json parsing,
 * to handle HTTP request sending and HTTP response reception.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\category\model\repository;

use people_sdk\library\model\repository\multi\model\MultiCollectionRepository;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\parser\parser\factory\library\ConstParserFactory;
use liberty_code\parser\parser\factory\string_table\library\ConstStrTableParserFactory;
use liberty_code\model\repository\library\ConstRepository as BaseConstRepository;
use liberty_code\requisition\request\factory\library\ConstRequestFactory;
use liberty_code\requisition\response\factory\library\ConstResponseFactory;
use liberty_code\requisition\client\info\library\ConstInfoClient;
use liberty_code\requisition\persistence\library\ConstPersistor;
use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\data\library\ConstDataHttpRequest;
use liberty_code\http\requisition\request\persistence\library\ConstPersistorHttpRequest;
use liberty_code\http\requisition\request\factory\library\ConstHttpRequestFactory;
use liberty_code\http\requisition\response\data\library\ConstDataHttpResponse;
use liberty_code\http\requisition\response\persistence\library\ConstPersistorHttpResponse;
use liberty_code\http\requisition\response\factory\library\ConstHttpResponseFactory;
use people_sdk\library\requisition\request\info\library\ConstSndInfo;
use people_sdk\library\requisition\response\library\ConstResponse;
use people_sdk\library\model\entity\requisition\response\library\ConstResponseEntity;
use people_sdk\library\model\repository\library\ConstRepository;
use people_sdk\item\requisition\request\info\library\ToolBoxItemSndInfo;
use people_sdk\item\item\category\library\ConstItemCategory;
use people_sdk\item\item\category\model\ItemCategoryEntityFactory;
use people_sdk\item\item\category\model\repository\ItemCategoryEntityMultiRepository;



/**
 * @method null|ItemCategoryEntityFactory getObjEntityFactory() @inheritdoc
 * @method null|ItemCategoryEntityMultiRepository getObjRepository() @inheritdoc
 */
class ItemCategoryEntityMultiCollectionRepository extends MultiCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|ItemCategoryEntityFactory $objEntityFactory = null
     * @param null|ItemCategoryEntityMultiRepository $objRepository = null
     */
    public function __construct(
        ItemCategoryEntityFactory $objEntityFactory = null,
        ItemCategoryEntityMultiRepository $objRepository = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixEntityFactoryClassPath()
    {
        // Return result
        return ItemCategoryEntityFactory::class;
    }



    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return ItemCategoryEntityMultiRepository::class;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Init var
        $tabPersistConfig = array(
            ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                ConstRequestFactory::TAB_CONFIG_KEY_TYPE => ConstHttpRequestFactory::CONFIG_TYPE_HTTP_PERSISTOR,
                ConstDataHttpRequest::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                    ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON
                ],
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_DATA_ID_KEY => ConstItemCategory::ATTRIBUTE_NAME_SAVE_ID
            ],
            ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                    ConstResponseFactory::TAB_CONFIG_KEY_TYPE => ConstHttpResponseFactory::CONFIG_TYPE_HTTP_PERSISTOR,
                    ConstDataHttpResponse::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                        ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON
                    ],
                    ConstPersistorHttpResponse::TAB_CONFIG_KEY_PATH_SEPARATOR => '/',
                    ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_ID_KEY => ConstItemCategory::ATTRIBUTE_NAME_SAVE_ID
                ]
            ]
        );
        $tabPersistConfigSet = ToolBoxTable::getTabMerge(
            $tabPersistConfig,
            array(
                ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                    ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                        ConstHttpRequest::TAB_SND_INFO_KEY_HEADER => [
                            ConstSndInfo::HEADER_KEY_CONTENT_TYPE => ConstSndInfo::CONTENT_TYPE_JSON
                        ]
                    ]
                ]
            )
        );
        $result = array(
            BaseConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE => false,
            ConstRepository::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_NAME_SAVE_ID => ConstItemCategory::ATTRIBUTE_NAME_SAVE_ID,

            // Persistence configuration, for persistence action get
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_CONFIG => $tabPersistConfig,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstItemCategory::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_MULTI_ID_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_MULTI_ID_KEY => 'crit-in-id',
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => ToolBoxItemSndInfo::getTabSndInfoWithSchemaUrlRoute(
                            '/item/schema/%1$s/item/category',
                            [
                                ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET'
                            ]
                        )
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ],
                // Additional persistence configuration, for profile sub-action
                ConstItemCategory::SUB_ACTION_TYPE_PROFILE => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_MULTI_ID_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_MULTI_ID_KEY => 'crit-in-id',
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => ToolBoxItemSndInfo::getTabSndInfoWithAreaUrlRoute(
                            '/item/profile/area/%1$s/item/category',
                            [
                                ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET'
                            ]
                        )
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_TYPE => ConstItemCategory::SUB_ACTION_TYPE_PROFILE,

            // Persistence configuration, for persistence action search
            ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_CONFIG => $tabPersistConfig,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstItemCategory::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_QUERY_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG,
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => ToolBoxItemSndInfo::getTabSndInfoWithSchemaUrlRoute(
                            '/item/schema/%1$s/item/category',
                            [
                                ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET'
                            ]
                        )
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ],
                // Additional persistence configuration, for profile sub-action
                ConstItemCategory::SUB_ACTION_TYPE_PROFILE => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_QUERY_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG,
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => ToolBoxItemSndInfo::getTabSndInfoWithAreaUrlRoute(
                            '/item/profile/area/%1$s/item/category',
                            [
                                ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET'
                            ]
                        )
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SEARCH_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_SEARCH_SUB_ACTION_TYPE => ConstItemCategory::SUB_ACTION_TYPE_PROFILE,

            // Persistence configuration, for persistence action create
            ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_CONFIG => $tabPersistConfigSet,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstItemCategory::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_MULTI_DATA_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_DATA_PATH => null,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_MULTI_DATA_PATH => null,
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => ToolBoxItemSndInfo::getTabSndInfoWithSchemaUrlRoute(
                            '/item/schema/%1$s/item/category',
                            [
                                ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'POST'
                            ]
                        )
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ],
                // Additional persistence configuration, for profile sub-action
                ConstItemCategory::SUB_ACTION_TYPE_PROFILE => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_MULTI_DATA_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_DATA_PATH => null,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_CREATE_MULTI_DATA_PATH => null,
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => ToolBoxItemSndInfo::getTabSndInfoWithAreaUrlRoute(
                            '/item/profile/area/%1$s/item/category',
                            [
                                ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'POST'
                            ]
                        )
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_CREATE_MULTI_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_CREATE_SUB_ACTION_TYPE => ConstItemCategory::SUB_ACTION_TYPE_PROFILE,

            // Persistence configuration, for persistence action update
            ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_CONFIG => $tabPersistConfigSet,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstItemCategory::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_DATA_PATH => null,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_PATH => null,
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => ToolBoxItemSndInfo::getTabSndInfoWithSchemaUrlRoute(
                            '/item/schema/%1$s/item/category',
                            [
                                ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'POST'
                            ]
                        )
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ],
                // Additional persistence configuration, for profile sub-action
                ConstItemCategory::SUB_ACTION_TYPE_PROFILE => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_BODY,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_DATA_PATH => null,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_PATH => null,
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => ToolBoxItemSndInfo::getTabSndInfoWithAreaUrlRoute(
                            '/item/profile/area/%1$s/item/category',
                            [
                                ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'POST'
                            ]
                        )
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_UPDATE_MULTI_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_UPDATE_SUB_ACTION_TYPE => ConstItemCategory::SUB_ACTION_TYPE_PROFILE,

            // Persistence configuration, for persistence action delete
            ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_CONFIG => $tabPersistConfig,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for admin sub-action
                ConstItemCategory::SUB_ACTION_TYPE_ADMIN => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_DELETE_MULTI_ID_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_ID_KEY => 'crit-in-id',
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => ToolBoxItemSndInfo::getTabSndInfoWithSchemaUrlRoute(
                            '/item/schema/%1$s/item/category',
                            [
                                ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'DELETE'
                            ]
                        )
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ],
                // Additional persistence configuration, for profile sub-action
                ConstItemCategory::SUB_ACTION_TYPE_PROFILE => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstPersistorHttpRequest:: TAB_CONFIG_KEY_DELETE_MULTI_ID_SUPPORT_TYPE =>
                            ConstPersistorHttpRequest::CONFIG_SUPPORT_TYPE_URL_ARG,
                        ConstPersistorHttpRequest::TAB_CONFIG_KEY_DELETE_MULTI_ID_KEY => 'crit-in-id',
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => ToolBoxItemSndInfo::getTabSndInfoWithAreaUrlRoute(
                            '/item/profile/area/%1$s/item/category',
                            [
                                ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'DELETE'
                            ]
                        )
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_RETURN_STATUS =>
                                ConstPersistorHttpResponse::CONFIG_DATA_RETURN_STATUS_REQUIRED,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_DELETE_MULTI_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_DELETE_SUB_ACTION_TYPE => ConstItemCategory::SUB_ACTION_TYPE_PROFILE
        );

        // Return result
        return $result;
    }



}