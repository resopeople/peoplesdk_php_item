<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\category\library;



class ConstItemCategory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_CATEGORY_ENTITY_FACTORY = 'objCategoryEntityFactory';
    const DATA_KEY_ITEM_ENTITY_FACTORY = 'objItemEntityFactory';
    const DATA_KEY_CATEGORY_ENTITY_FACTORY_EXEC_CONFIG = 'tabCategoryEntityFactoryExecConfig';
    const DATA_KEY_ITEM_ENTITY_FACTORY_EXEC_CONFIG = 'tabItemEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_ITEM = 'attrItem';
    const ATTRIBUTE_KEY_CATEGORY = 'attrCategory';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_ITEM = 'item';
    const ATTRIBUTE_ALIAS_CATEGORY = 'category';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_ITEM = 'item';
    const ATTRIBUTE_NAME_SAVE_CATEGORY = 'category';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_PROFILE = 'profile';



    // Exception message constants
    const EXCEPT_MSG_CATEGORY_ENTITY_FACTORY_INVALID_FORMAT =
        'Following category entity factory "%1$s" invalid! It must be a category entity factory object.';
    const EXCEPT_MSG_ITEM_ENTITY_FACTORY_INVALID_FORMAT =
        'Following item entity factory "%1$s" invalid! It must be an item entity factory object.';
    const EXCEPT_MSG_CATEGORY_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the category entity factory execution configuration standard.';
    const EXCEPT_MSG_ITEM_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the item entity factory execution configuration standard.';



}