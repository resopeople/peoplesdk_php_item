<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\browser\library;



class ConstItemBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_LIKE_NAME = 'strAttrCritLikeName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_NAME = 'strAttrCritEqualName';
    const ATTRIBUTE_KEY_CRIT_IN_NAME = 'tabAttrCritInName';
    const ATTRIBUTE_KEY_CRIT_IS_PROFILE_DEFINED = 'boolAttrCritIsProfileDefined';
    const ATTRIBUTE_KEY_CRIT_EQUAL_PROFILE_TYPE = 'strAttrCritEqualProfileType';
    const ATTRIBUTE_KEY_CRIT_IN_PROFILE_TYPE = 'tabAttrCritInProfileType';
    const ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID = 'intAttrCritEqualUserProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID = 'tabAttrCritInUserProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN = 'strAttrCritLikeUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN = 'strAttrCritEqualUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN = 'tabAttrCritInUserProfileLogin';
    const ATTRIBUTE_KEY_CRIT_IS_CURRENT_USER_PROFILE = 'boolAttrCritIsCurrentUserProfile';
    const ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_ID = 'intAttrCritEqualAppProfileId';
    const ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_ID = 'tabAttrCritInAppProfileId';
    const ATTRIBUTE_KEY_CRIT_LIKE_APP_PROFILE_NAME = 'strAttrCritLikeAppProfileName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_NAME = 'strAttrCritEqualAppProfileName';
    const ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_NAME = 'tabAttrCritInAppProfileName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_ATTRIBUTE_DATA = 'attrCritEqualAttrData';
    const ATTRIBUTE_KEY_CRIT_IN_ATTRIBUTE_DATA = 'tabAttrCritInAttrData';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_ATTRIBUTE_DATA = 'attrCritNotEqualAttrData';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_ATTRIBUTE_DATA = 'tabAttrCritNotInAttrData';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_ID = 'intAttrCritEqualRelatedParentId';
    const ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_ID = 'intAttrCritInRelatedParentId';
    const ATTRIBUTE_KEY_CRIT_LIKE_RELATED_PARENT_NAME = 'strAttrCritLikeRelatedParentName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_NAME = 'strAttrCritEqualRelatedParentName';
    const ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_NAME = 'tabAttrCritInRelatedParentName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_ID = 'intAttrCritEqualRelatedParentSchemaId';
    const ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_ID = 'tabAttrCritInRelatedParentSchemaId';
    const ATTRIBUTE_KEY_CRIT_LIKE_RELATED_PARENT_SCHEMA_NAME = 'strAttrCritLikeRelatedParentSchemaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_NAME = 'strAttrCritEqualRelatedParentSchemaName';
    const ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_NAME = 'tabAttrCritInRelatedParentSchemaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_ID = 'intAttrCritEqualRelatedParentSchemaRelationId';
    const ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_RELATION_ID = 'intAttrCritInRelatedParentSchemaRelationId';
    const ATTRIBUTE_KEY_CRIT_LIKE_RELATED_PARENT_SCHEMA_RELATION_NAME = 'strAttrCritLikeRelatedParentSchemaRelationName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_NAME = 'strAttrCritEqualRelatedParentSchemaRelationName';
    const ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_RELATION_NAME = 'tabAttrCritInRelatedParentSchemaRelationName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_RELATION_DATA = 'attrCritEqualRelatedParentRelationData';
    const ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_RELATION_DATA = 'tabAttrCritInRelatedParentRelationData';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_ID = 'intAttrCritNotEqualRelatedParentId';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_ID = 'intAttrCritNotInRelatedParentId';
    const ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_PARENT_NAME = 'strAttrCritNotLikeRelatedParentName';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_NAME = 'strAttrCritNotEqualRelatedParentName';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_NAME = 'tabAttrCritNotInRelatedParentName';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_ID = 'intAttrCritNotEqualRelatedParentSchemaId';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_ID = 'tabAttrCritNotInRelatedParentSchemaId';
    const ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_PARENT_SCHEMA_NAME = 'strAttrCritNotLikeRelatedParentSchemaName';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_NAME = 'strAttrCritNotEqualRelatedParentSchemaName';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_NAME = 'tabAttrCritNotInRelatedParentSchemaName';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_ID = 'intAttrCritNotEqualRelatedParentSchemaRelationId';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_RELATION_ID = 'intAttrCritNotInRelatedParentSchemaRelationId';
    const ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_PARENT_SCHEMA_RELATION_NAME = 'strAttrCritNotLikeRelatedParentSchemaRelationName';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_NAME = 'strAttrCritNotEqualRelatedParentSchemaRelationName';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_RELATION_NAME = 'tabAttrCritNotInRelatedParentSchemaRelationName';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_RELATION_DATA = 'attrCritNotEqualRelatedParentRelationData';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_RELATION_DATA = 'tabAttrCritNotInRelatedParentRelationData';
    const ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_ID = 'intAttrCritEqualCategoryId';
    const ATTRIBUTE_KEY_CRIT_IN_CATEGORY_ID = 'tabAttrCritInCategoryId';
    const ATTRIBUTE_KEY_CRIT_LIKE_CATEGORY_NAME = 'strAttrCritLikeCategoryName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_NAME = 'strAttrCritEqualCategoryName';
    const ATTRIBUTE_KEY_CRIT_IN_CATEGORY_NAME = 'tabAttrCritInCategoryName';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_CATEGORY_ID = 'intAttrCritNotEqualCategoryId';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_CATEGORY_ID = 'tabAttrCritNotInCategoryId';
    const ATTRIBUTE_KEY_CRIT_NOT_LIKE_CATEGORY_NAME = 'strAttrCritNotLikeCategoryName';
    const ATTRIBUTE_KEY_CRIT_NOT_EQUAL_CATEGORY_NAME = 'strAttrCritNotEqualCategoryName';
    const ATTRIBUTE_KEY_CRIT_NOT_IN_CATEGORY_NAME = 'tabAttrCritNotInCategoryName';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_NAME = 'strAttrSortName';
    const ATTRIBUTE_KEY_SORT_PROFILE_TYPE = 'strAttrSortProfileType';
    const ATTRIBUTE_KEY_SORT_USER_PROFILE_ID = 'strAttrSortUserProfileId';
    const ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN = 'strAttrSortUserProfileLogin';
    const ATTRIBUTE_KEY_SORT_APP_PROFILE_ID = 'strAttrSortAppProfileId';
    const ATTRIBUTE_KEY_SORT_APP_PROFILE_NAME = 'strAttrSortAppProfileName';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_NAME = 'crit-like-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME = 'crit-equal-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_NAME = 'crit-in-name';
    const ATTRIBUTE_ALIAS_CRIT_IS_PROFILE_DEFINED = 'crit-is-profile-defined';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_PROFILE_TYPE = 'crit-equal-profile-type';
    const ATTRIBUTE_ALIAS_CRIT_IN_PROFILE_TYPE = 'crit-in-profile-type';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_ID = 'crit-equal-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_ID = 'crit-in-user-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_USER_PROFILE_LOGIN = 'crit-like-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_LOGIN = 'crit-equal-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_LOGIN = 'crit-in-user-profile-login';
    const ATTRIBUTE_ALIAS_CRIT_IS_CURRENT_USER_PROFILE = 'crit-is-current-user-profile';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_APP_PROFILE_ID = 'crit-equal-app-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_APP_PROFILE_ID = 'crit-in-app-profile-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_APP_PROFILE_NAME = 'crit-like-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_APP_PROFILE_NAME = 'crit-equal-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_APP_PROFILE_NAME = 'crit-in-app-profile-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ATTRIBUTE_DATA = 'crit-equal-attribute-data';
    const ATTRIBUTE_ALIAS_CRIT_IN_ATTRIBUTE_DATA = 'crit-in-attribute-data';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_ATTRIBUTE_DATA = 'crit-not-equal-attribute-data';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_ATTRIBUTE_DATA = 'crit-not-in-attribute-data';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_ID = 'crit-equal-related-parent-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_ID = 'crit-in-related-parent-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_RELATED_PARENT_NAME = 'crit-like-related-parent-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_NAME = 'crit-equal-related-parent-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_NAME = 'crit-in-related-parent-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_SCHEMA_ID = 'crit-equal-related-parent-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_SCHEMA_ID = 'crit-in-related-parent-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_RELATED_PARENT_SCHEMA_NAME = 'crit-like-related-parent-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_SCHEMA_NAME = 'crit-equal-related-parent-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_SCHEMA_NAME = 'crit-in-related-parent-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_ID = 'crit-equal-related-parent-schema-relation-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_SCHEMA_RELATION_ID = 'crit-in-related-parent-schema-relation-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_RELATED_PARENT_SCHEMA_RELATION_NAME = 'crit-like-related-parent-schema-relation-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_NAME = 'crit-equal-related-parent-schema-relation-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_SCHEMA_RELATION_NAME = 'crit-in-related-parent-schema-relation-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_RELATION_DATA = 'crit-equal-related-parent-relation-data';
    const ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_RELATION_DATA = 'crit-in-related-parent-relation-data';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_ID = 'crit-not-equal-related-parent-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_ID = 'crit-not-in-related-parent-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_RELATED_PARENT_NAME = 'crit-not-like-related-parent-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_NAME = 'crit-not-equal-related-parent-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_NAME = 'crit-not-in-related-parent-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_ID = 'crit-not-equal-related-parent-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_ID = 'crit-not-in-related-parent-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_RELATED_PARENT_SCHEMA_NAME = 'crit-not-like-related-parent-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_NAME = 'crit-not-equal-related-parent-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_NAME = 'crit-not-in-related-parent-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_ID = 'crit-not-equal-related-parent-schema-relation-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_RELATION_ID = 'crit-not-in-related-parent-schema-relation-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_RELATED_PARENT_SCHEMA_RELATION_NAME = 'crit-not-like-related-parent-schema-relation-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_NAME = 'crit-not-equal-related-parent-schema-relation-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_RELATION_NAME = 'crit-not-in-related-parent-schema-relation-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_RELATION_DATA = 'crit-not-equal-related-parent-relation-data';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_RELATION_DATA = 'crit-not-in-related-parent-relation-data';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_CATEGORY_ID = 'crit-equal-category-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_CATEGORY_ID = 'crit-in-category-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_CATEGORY_NAME = 'crit-like-category-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_CATEGORY_NAME = 'crit-equal-category-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_CATEGORY_NAME = 'crit-in-category-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_CATEGORY_ID = 'crit-not-equal-category-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_CATEGORY_ID = 'crit-not-in-category-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_CATEGORY_NAME = 'crit-not-like-category-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_CATEGORY_NAME = 'crit-not-equal-category-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_IN_CATEGORY_NAME = 'crit-not-in-category-name';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_NAME = 'sort-name';
    const ATTRIBUTE_ALIAS_SORT_PROFILE_TYPE = 'sort-profile-type';
    const ATTRIBUTE_ALIAS_SORT_USER_PROFILE_ID = 'sort-user-profile-id';
    const ATTRIBUTE_ALIAS_SORT_USER_PROFILE_LOGIN = 'sort-user-profile-login';
    const ATTRIBUTE_ALIAS_SORT_APP_PROFILE_ID = 'sort-app-profile-id';
    const ATTRIBUTE_ALIAS_SORT_APP_PROFILE_NAME = 'sort-app-profile-name';



}