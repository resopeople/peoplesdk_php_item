<?php
/**
 * This class allows to define item browser entity class.
 * Item browser entity allows to define attributes,
 * to search item entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\handle_model\attribute\library\ToolBoxAttribute;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\item\schema\attribute\model\SchemaAttrEntity;
use people_sdk\item\schema\attribute\provider\model\SchemaAttrProvider;
use people_sdk\item\item\library\ConstItem;
use people_sdk\item\item\browser\library\ConstItemBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|string $strAttrCritLikeName
 * @property null|string $strAttrCritEqualName
 * @property null|string[] $tabAttrCritInName
 * @property null|boolean $boolAttrCritIsProfileDefined
 * @property null|string $strAttrCritEqualProfileType
 * @property null|string[] $tabAttrCritInProfileType
 * @property null|integer $intAttrCritEqualUserProfileId
 * @property null|integer[] $tabAttrCritInUserProfileId
 * @property null|string $strAttrCritLikeUserProfileLogin
 * @property null|string $strAttrCritEqualUserProfileLogin
 * @property null|string[] $tabAttrCritInUserProfileLogin
 * @property null|boolean $boolAttrCritIsCurrentUserProfile
 * @property null|integer $intAttrCritEqualAppProfileId
 * @property null|integer[] $tabAttrCritInAppProfileId
 * @property null|string $strAttrCritLikeAppProfileName
 * @property null|string $strAttrCritEqualAppProfileName
 * @property null|string[] $tabAttrCritInAppProfileName
 * @property null|array $attrCritEqualAttrData:
 * Array format:
 * [
 *     'string attribute alias' (required),
 *     mixed attribute value (required)
 * ]
 * @property null|array[] $tabAttrCritInAttrData:
 * Array format:
 * [
 *     // Attribute data 1
 *     [@see ItemBrowserEntity::$attrCritEqualAttrData array format],
 *     ...,
 *     // Attribute data N
 *     [...]
 * ]
 * @property null|array $attrCritNotEqualAttrData:
 * Array format:
 * @see ItemBrowserEntity::$attrCritEqualAttrData array format.
 * @property null|array[] $tabAttrCritNotInAttrData:
 * Array format:
 * @see ItemBrowserEntity::$tabAttrCritInAttrData array format.
 * @property null|integer $intAttrCritEqualRelatedParentId
 * @property null|integer[] $intAttrCritInRelatedParentId
 * @property null|string $strAttrCritLikeRelatedParentName
 * @property null|string $strAttrCritEqualRelatedParentName
 * @property null|string[] $tabAttrCritInRelatedParentName
 * @property null|integer $intAttrCritEqualRelatedParentSchemaId
 * @property null|integer[] $tabAttrCritInRelatedParentSchemaId
 * @property null|string $strAttrCritLikeRelatedParentSchemaName
 * @property null|string $strAttrCritEqualRelatedParentSchemaName
 * @property null|string[] $tabAttrCritInRelatedParentSchemaName
 * @property null|integer $intAttrCritEqualRelatedParentSchemaRelationId
 * @property null|integer[] $intAttrCritInRelatedParentSchemaRelationId
 * @property null|string $strAttrCritLikeRelatedParentSchemaRelationName
 * @property null|string $strAttrCritEqualRelatedParentSchemaRelationName
 * @property null|string[] $tabAttrCritInRelatedParentSchemaRelationName
 * @property null|array $attrCritEqualRelatedParentRelationData:
 * Array format:
 * [
 *     integer item id OR 'string item name' (required),
 *     integer schema relation id OR 'string schema relation name' (required),
 *     integer schema id OR 'string schema name' (optional)
 * ]
 * @property null|array[] $tabAttrCritInRelatedParentRelationData:
 * Array format:
 * [
 *     // Parent relation data 1
 *     [@see ItemBrowserEntity::$attrCritEqualRelatedParentRelationData array format],
 *     ...,
 *     // Parent relation data N
 *     [...]
 * ]
 * @property null|integer $intAttrCritNotEqualRelatedParentId
 * @property null|integer[] $intAttrCritNotInRelatedParentId
 * @property null|string $strAttrCritNotLikeRelatedParentName
 * @property null|string $strAttrCritNotEqualRelatedParentName
 * @property null|string[] $tabAttrCritNotInRelatedParentName
 * @property null|integer $intAttrCritNotEqualRelatedParentSchemaId
 * @property null|integer[] $tabAttrCritNotInRelatedParentSchemaId
 * @property null|string $strAttrCritNotLikeRelatedParentSchemaName
 * @property null|string $strAttrCritNotEqualRelatedParentSchemaName
 * @property null|string[] $tabAttrCritNotInRelatedParentSchemaName
 * @property null|integer $intAttrCritNotEqualRelatedParentSchemaRelationId
 * @property null|integer[] $intAttrCritNotInRelatedParentSchemaRelationId
 * @property null|string $strAttrCritNotLikeRelatedParentSchemaRelationName
 * @property null|string $strAttrCritNotEqualRelatedParentSchemaRelationName
 * @property null|string[] $tabAttrCritNotInRelatedParentSchemaRelationName
 * @property null|array $attrCritNotEqualRelatedParentRelationData:
 * Array format:
 * @see ItemBrowserEntity::$attrCritEqualRelatedParentRelationData array format.
 * @property null|array[] $tabAttrCritNotInRelatedParentRelationData:
 * Array format:
 * @see ItemBrowserEntity::$tabAttrCritInRelatedParentRelationData array format.
 * @property null|integer $intAttrCritEqualCategoryId
 * @property null|integer[] $tabAttrCritInCategoryId
 * @property null|string $strAttrCritLikeCategoryName
 * @property null|string $strAttrCritEqualCategoryName
 * @property null|string[] $tabAttrCritInCategoryName
 * @property null|integer $intAttrCritNotEqualCategoryId
 * @property null|integer[] $tabAttrCritNotInCategoryId
 * @property null|string $strAttrCritNotLikeCategoryName
 * @property null|string $strAttrCritNotEqualCategoryName
 * @property null|string[] $tabAttrCritNotInCategoryName
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortName
 * @property null|string $strAttrSortProfileType
 * @property null|string $strAttrSortUserProfileId
 * @property null|string $strAttrSortUserProfileLogin
 * @property null|string $strAttrSortAppProfileId
 * @property null|string $strAttrSortAppProfileName
 */
class ItemBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: Schema attribute provider instance.
     * @var SchemaAttrProvider
     */
    protected $objSchemaAttrProvider;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     * @param SchemaAttrProvider $objSchemaAttrProvider
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        SchemaAttrProvider $objSchemaAttrProvider,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objSchemaAttrProvider = $objSchemaAttrProvider;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is profile defined
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IS_PROFILE_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IS_PROFILE_DEFINED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal profile type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in profile type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is current profile
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IS_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IS_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal application profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in application profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal attribute data
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ATTRIBUTE_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ATTRIBUTE_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in attribute data
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_ATTRIBUTE_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ATTRIBUTE_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal attribute data
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_ATTRIBUTE_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_ATTRIBUTE_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in attribute data
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_ATTRIBUTE_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_ATTRIBUTE_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal related parent id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in related parent id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like related parent name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RELATED_PARENT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_RELATED_PARENT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal related parent name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in related parent name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal related parent schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in related parent schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like related parent schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RELATED_PARENT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_RELATED_PARENT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal related parent schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in related parent schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal related parent schema relation id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in related parent schema relation id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like related parent schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RELATED_PARENT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_RELATED_PARENT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal related parent schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in related parent schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal related parent relation data
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_RELATION_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_RELATED_PARENT_RELATION_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in related parent relation data
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_RELATION_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_RELATED_PARENT_RELATION_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal related parent id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in related parent id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not like related parent name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_PARENT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_RELATED_PARENT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal related parent name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in related parent name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal related parent schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in related parent schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not like related parent schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_PARENT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_RELATED_PARENT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal related parent schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in related parent schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal related parent schema relation id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in related parent schema relation id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not like related parent schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_PARENT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_RELATED_PARENT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal related parent schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in related parent schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal related parent relation data
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_RELATION_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_RELATED_PARENT_RELATION_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in related parent relation data
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_RELATION_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_RELATED_PARENT_RELATION_DATA,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal category id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in category id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like category name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal category name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in category name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_IN_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal category id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in category id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_CATEGORY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not like category name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_LIKE_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not equal category name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_EQUAL_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not in category name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_IN_CATEGORY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_SORT_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort profile type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_SORT_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_SORT_PROFILE_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort user profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_SORT_USER_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort user profile login
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_SORT_USER_PROFILE_LOGIN,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort application profile id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_SORT_APP_PROFILE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort application profile name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemBrowser::ATTRIBUTE_ALIAS_SORT_APP_PROFILE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );
        $tabProfileType = ConstItem::getTabProfileType();
        $strTabProfileType = implode(', ', array_map(
            function($value) {return sprintf('\'%1$s\'', $value);},
            $tabProfileType
        ));
        $tabRuleConfigValidProfileType = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-profile-type' => [
                            [
                                'compare_in',
                                [
                                    'compare_value' => $tabProfileType
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => sprintf(
                        '%1$s must be null or a valid profile type in (%2$s) else.',
                        '%1$s',
                        $strTabProfileType
                    )
                ]
            ]
        );
        $tabRuleConfigValidTabProfileType = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-profile-type' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'compare_in',
                                            [
                                                'compare_value' => $tabProfileType
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => sprintf(
                        '%1$s must be null or an array of valid profile types in (%2$s) else.',
                        '%1$s',
                        $strTabProfileType
                    )
                ]
            ]
        );

        $tabRuleConfigValidAttributeDataEngine = array(
            [
                'type_array',
                [
                    'index_only_require' => true
                ]
            ],
            [
                'sub_rule_size',
                [
                    'rule_config' => [
                        [
                            'compare_equal',
                            ['compare_value' => 2]
                        ]
                    ]
                ]
            ],
            [
                'callable',
                [
                    'valid_callable' => function($strName, $value)
                    {
                        return (
                            is_array($value) &&
                            array_key_exists(0, $value) &&
                            is_string($value[0]) &&
                            (!is_null($this->getStrSchemaAttrEntityAttrNameSaveFromKey($value[0]))) &&
                            array_key_exists(1, $value)
                        );
                    }
                ]
            ]
        );
        $tabRuleConfigNullValidAttributeData = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-attribute-data' => $tabRuleConfigValidAttributeDataEngine
                    ],
                    'error_message_pattern' => '%1$s must be null or a valid attribute data.'
                ]
            ]
        );

        $tabRuleConfigNullValidTabAttributeData = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-attribute-data' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => $tabRuleConfigValidAttributeDataEngine
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of valid attribute data.'
                ]
            ]
        );

        $tabRuleConfigNullValidParentRelationDataEngine = array(
            [
                'type_array',
                [
                    'index_only_require' => true
                ]
            ],
            [
                'sub_rule_size',
                [
                    'rule_config' => [
                        [
                            'compare_between',
                            [
                                'greater_compare_value' => 2,
                                'less_compare_value' => 3
                            ]
                        ]
                    ]
                ]
            ],
            [
                'sub_rule_iterate',
                [
                    'rule_config' => [
                        [
                            'group_sub_rule_or',
                            [
                                'rule_config' => [
                                    'is-valid-integer' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ],
                                    'is-valid-string' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        );
        $tabRuleConfigNullValidParentRelationData = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-relation-data' => $tabRuleConfigNullValidParentRelationDataEngine
                    ],
                    'error_message_pattern' => '%1$s must be null or a valid parent relation data.'
                ]
            ]
        );
        $tabRuleConfigNullValidTabParentRelationData = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-relation-data' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => $tabRuleConfigNullValidParentRelationDataEngine
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of valid parent relation data.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigValidDate,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigValidDate,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigValidDate,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigValidDate,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME => $tabRuleConfigValidTabString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IS_PROFILE_DEFINED => $tabRuleConfigValidBoolean,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PROFILE_TYPE => $tabRuleConfigValidProfileType,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_PROFILE_TYPE => $tabRuleConfigValidTabProfileType,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID => $tabRuleConfigValidId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN => $tabRuleConfigValidTabString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IS_CURRENT_USER_PROFILE => $tabRuleConfigValidBoolean,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_ID => $tabRuleConfigValidId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_ID => $tabRuleConfigValidTabId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_APP_PROFILE_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_NAME => $tabRuleConfigValidTabString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ATTRIBUTE_DATA => $tabRuleConfigNullValidAttributeData,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_ATTRIBUTE_DATA => $tabRuleConfigNullValidTabAttributeData,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_ATTRIBUTE_DATA => $tabRuleConfigNullValidAttributeData,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_ATTRIBUTE_DATA => $tabRuleConfigNullValidTabAttributeData,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_ID => $tabRuleConfigValidId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_ID => $tabRuleConfigValidTabId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RELATED_PARENT_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_NAME => $tabRuleConfigValidTabString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_ID => $tabRuleConfigValidId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_ID => $tabRuleConfigValidTabId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RELATED_PARENT_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_NAME => $tabRuleConfigValidTabString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_ID => $tabRuleConfigValidId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_RELATION_ID => $tabRuleConfigValidTabId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RELATED_PARENT_SCHEMA_RELATION_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_RELATION_NAME => $tabRuleConfigValidTabString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_RELATION_DATA => $tabRuleConfigNullValidParentRelationData,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_RELATION_DATA => $tabRuleConfigNullValidTabParentRelationData,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_ID => $tabRuleConfigValidId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_ID => $tabRuleConfigValidTabId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_PARENT_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_NAME => $tabRuleConfigValidTabString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_ID => $tabRuleConfigValidId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_ID => $tabRuleConfigValidTabId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_PARENT_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_NAME => $tabRuleConfigValidTabString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_ID => $tabRuleConfigValidId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_RELATION_ID => $tabRuleConfigValidTabId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_PARENT_SCHEMA_RELATION_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_RELATION_NAME => $tabRuleConfigValidTabString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_RELATION_DATA => $tabRuleConfigNullValidParentRelationData,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_RELATION_DATA => $tabRuleConfigNullValidTabParentRelationData,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_ID => $tabRuleConfigValidId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_CATEGORY_ID => $tabRuleConfigValidTabId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_CATEGORY_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_CATEGORY_NAME => $tabRuleConfigValidTabString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_CATEGORY_ID => $tabRuleConfigValidId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_CATEGORY_ID => $tabRuleConfigValidTabId,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_CATEGORY_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_CATEGORY_NAME => $tabRuleConfigValidString,
                ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_CATEGORY_NAME => $tabRuleConfigValidTabString,
                ConstItemBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstItemBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigValidSort,
                ConstItemBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigValidSort,
                ConstItemBrowser::ATTRIBUTE_KEY_SORT_NAME => $tabRuleConfigValidSort,
                ConstItemBrowser::ATTRIBUTE_KEY_SORT_PROFILE_TYPE => $tabRuleConfigValidSort,
                ConstItemBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID => $tabRuleConfigValidSort,
                ConstItemBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN => $tabRuleConfigValidSort,
                ConstItemBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_ID => $tabRuleConfigValidSort,
                ConstItemBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_NAME => $tabRuleConfigValidSort
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objSchemaAttrProvider = $this->objSchemaAttrProvider;

        // Format by attribute
        switch($strKey)
        {
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ATTRIBUTE_DATA:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_ATTRIBUTE_DATA:
                $result = (
                    (
                        is_array($value) &&
                        array_key_exists(0, $value) &&
                        array_key_exists(1, $value) &&
                        (!is_null($strAlias = $this->getStrSchemaAttrEntityAttrAliasFromKey($value[0])))
                    ) ?
                        array(
                            $strAlias,
                            $objSchemaAttrProvider->getEntityAttrValueFormatGet(
                                $value[0],
                                $value[1]
                            )
                        ) :
                        $value
                );
                break;

            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_ATTRIBUTE_DATA:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_ATTRIBUTE_DATA:
                $result = (
                    is_array($value) ?
                        array_map(
                            function($value) use ($objSchemaAttrProvider) {
                                return (
                                (
                                    is_array($value) &&
                                    array_key_exists(0, $value) &&
                                    array_key_exists(1, $value) &&
                                    (!is_null($strAlias = $this->getStrSchemaAttrEntityAttrAliasFromKey($value[0])))
                                ) ?
                                    array(
                                        $strAlias,
                                        $objSchemaAttrProvider->getEntityAttrValueFormatGet(
                                            $value[0],
                                            $value[1]
                                        )
                                    ) :
                                    $value
                                );
                            },
                            $value
                        ) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objSchemaAttrProvider = $this->objSchemaAttrProvider;

        // Format by attribute
        switch($strKey)
        {
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_CATEGORY_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_RELATION_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_RELATION_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_CATEGORY_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_CATEGORY_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_string($value) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ($value instanceof DateTime) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_PROFILE_TYPE:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_USER_PROFILE_LOGIN:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_USER_PROFILE_LOGIN:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_APP_PROFILE_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_APP_PROFILE_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RELATED_PARENT_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RELATED_PARENT_SCHEMA_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_RELATED_PARENT_SCHEMA_RELATION_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_RELATED_PARENT_RELATION_DATA:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_PARENT_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_PARENT_SCHEMA_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_RELATED_PARENT_SCHEMA_RELATION_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_SCHEMA_RELATION_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_RELATED_PARENT_RELATION_DATA:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_LIKE_CATEGORY_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_CATEGORY_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_LIKE_CATEGORY_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_CATEGORY_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstItemBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstItemBrowser::ATTRIBUTE_KEY_SORT_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_SORT_PROFILE_TYPE:
            case ConstItemBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_SORT_USER_PROFILE_LOGIN:
            case ConstItemBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_ID:
            case ConstItemBrowser::ATTRIBUTE_KEY_SORT_APP_PROFILE_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_PROFILE_TYPE:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_USER_PROFILE_LOGIN:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_APP_PROFILE_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_SCHEMA_RELATION_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_RELATED_PARENT_RELATION_DATA:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_SCHEMA_RELATION_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_RELATED_PARENT_RELATION_DATA:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_CATEGORY_NAME:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_CATEGORY_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IS_PROFILE_DEFINED:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IS_CURRENT_USER_PROFILE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ATTRIBUTE_DATA:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_ATTRIBUTE_DATA:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                is_array($value) &&
                                array_key_exists(0, array_values($value)) &&
                                array_key_exists(1, array_values($value)) &&
                                (!is_null($strKey = $this->getStrSchemaAttrEntityAttrKeyFromAlias(array_values($value)[0])))
                            ) ?
                                array(
                                    $strKey,
                                    $objSchemaAttrProvider->getEntityAttrValueFormatSet(
                                        $strKey,
                                        array_values($value)[1]
                                    )
                                ) :
                                $value
                        )
                );
                break;

            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_ATTRIBUTE_DATA:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_ATTRIBUTE_DATA:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_array($value) ?
                                array_values(array_map(
                                    function($value) use ($objSchemaAttrProvider) {
                                        return (
                                            (
                                                is_array($value) &&
                                                array_key_exists(0, array_values($value)) &&
                                                array_key_exists(1, array_values($value)) &&
                                                (!is_null($strKey = $this->getStrSchemaAttrEntityAttrKeyFromAlias(array_values($value)[0])))
                                            ) ?
                                                array(
                                                    $strKey,
                                                    $objSchemaAttrProvider->getEntityAttrValueFormatSet(
                                                        $strKey,
                                                        array_values($value)[1]
                                                    )
                                                ) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of schema attribute entities.
     *
     * @return SchemaAttrEntity[]
     */
    protected function getTabSchemaAttrEntity()
    {
        // Return result
        return array_values($this->objSchemaAttrProvider->getObjAttributeCollection()->getTabItem());
    }



    /**
     * Get schema attribute entity attribute key,
     * from specified schema attribute entity attribute alias.
     *
     * @param string $strAlias
     * @return null|string
     */
    protected function getStrSchemaAttrEntityAttrKeyFromAlias($strAlias)
    {

        // Init var
        $objSchemaAttrEntity = ToolBoxAttribute::getObjAttributeFromEntityAttrConfigData(
            $this->getTabSchemaAttrEntity(),
            ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS,
            $strAlias
        );
        $result = (
            (!is_null($objSchemaAttrEntity)) ?
                $objSchemaAttrEntity->getStrAttributeKey() :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get schema attribute entity attribute alias,
     * from specified schema attribute entity attribute key.
     *
     * @param string $strKey
     * @return null|string
     */
    protected function getStrSchemaAttrEntityAttrAliasFromKey($strKey)
    {
        // Init var
        $objSchemaAttrEntity = ToolBoxAttribute::getObjAttributeFromAttrKey(
            $this->getTabSchemaAttrEntity(),
            $strKey
        );
        $result = (
            (!is_null($objSchemaAttrEntity)) ?
                $objSchemaAttrEntity->getTabEntityAttrConfig()[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get schema attribute entity attribute save name,
     * from specified schema attribute entity attribute key.
     *
     * @param string $strKey
     * @return null|string
     */
    protected function getStrSchemaAttrEntityAttrNameSaveFromKey($strKey)
    {
        // Init var
        $objSchemaAttrEntity = ToolBoxAttribute::getObjAttributeFromAttrKey(
            $this->getTabSchemaAttrEntity(),
            $strKey
        );
        $result = (
            (!is_null($objSchemaAttrEntity)) ?
                $objSchemaAttrEntity->getTabEntityAttrConfig()[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Init var
        $objSchemaAttrProvider = $this->objSchemaAttrProvider;

        // Format by attribute
        switch($strKey)
        {
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = $this->getAttributeValueFormatGet($strKey, $value);
                break;

            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IS_PROFILE_DEFINED:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IS_CURRENT_USER_PROFILE:
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
                break;

            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ATTRIBUTE_DATA:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_EQUAL_ATTRIBUTE_DATA:
                $result = (
                    (
                        is_array($value) &&
                        array_key_exists(0, $value) &&
                        array_key_exists(1, $value) &&
                        (!is_null($strNameSave = $this->getStrSchemaAttrEntityAttrNameSaveFromKey($value[0])))
                    ) ?
                        array(
                            $strNameSave,
                            $objSchemaAttrProvider->getEntityAttrValueSaveFormatGet(
                                $value[0],
                                $value[1]
                            )
                        ) :
                        $value
                );
                break;

            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_IN_ATTRIBUTE_DATA:
            case ConstItemBrowser::ATTRIBUTE_KEY_CRIT_NOT_IN_ATTRIBUTE_DATA:
                $result = (
                    is_array($value) ?
                        array_map(
                            function($value) use ($objSchemaAttrProvider) {
                                return (
                                    (
                                        is_array($value) &&
                                        array_key_exists(0, $value) &&
                                        array_key_exists(1, $value) &&
                                        (!is_null($strNameSave = $this->getStrSchemaAttrEntityAttrNameSaveFromKey($value[0])))
                                    ) ?
                                        array(
                                            $strNameSave,
                                            $objSchemaAttrProvider->getEntityAttrValueSaveFormatGet(
                                                $value[0],
                                                $value[1]
                                            )
                                        ) :
                                        $value
                                );
                            },
                            $value
                        ) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}