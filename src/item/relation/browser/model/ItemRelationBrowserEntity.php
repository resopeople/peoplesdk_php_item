<?php
/**
 * This class allows to define item relation browser entity class.
 * Item relation browser entity allows to define attributes,
 * to search item relation entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\relation\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\browser\library\ToolBoxValidBrowserEntity;
use people_sdk\item\item\relation\browser\library\ConstItemRelationBrowser;



/**
 * @property null|integer $intAttrCritEqualId
 * @property null|integer[] $tabAttrCritInId
 * @property null|string|DateTime $attrCritStartDtCreate
 * @property null|string|DateTime $attrCritEndDtCreate
 * @property null|string|DateTime $attrCritStartDtUpdate
 * @property null|string|DateTime $attrCritEndDtUpdate
 * @property null|integer $intAttrCritEqualItemId
 * @property null|integer[] $tabAttrCritInItemId
 * @property null|string $strAttrCritLikeItemName
 * @property null|string $strAttrCritEqualItemName
 * @property null|string[] $tabAttrCritInItemName
 * @property null|boolean $boolAttrCritIsItemCurrentUserProfile
 * @property null|integer $intAttrCritEqualSchemaRelationId
 * @property null|integer[] $tabAttrCritInSchemaRelationId
 * @property null|string $strAttrCritLikeSchemaRelationName
 * @property null|string $strAttrCritEqualSchemaRelationName
 * @property null|string[] $tabAttrCritInSchemaRelationName
 * @property null|integer $intAttrCritEqualSubItemId
 * @property null|integer[] $tabAttrCritInSubItemId
 * @property null|string $strAttrCritLikeSubItemName
 * @property null|string $strAttrCritEqualSubItemName
 * @property null|string[] $tabAttrCritInSubItemName
 * @property null|integer $intAttrCritEqualSubItemSchemaId
 * @property null|integer[] $tabAttrCritInSubItemSchemaId
 * @property null|string $strAttrCritLikeSubItemSchemaName
 * @property null|string $strAttrCritEqualSubItemSchemaName
 * @property null|string[] $tabAttrCritInSubItemSchemaName
 * @property null|string $strAttrSortId
 * @property null|string $strAttrSortDtCreate
 * @property null|string $strAttrSortDtUpdate
 * @property null|string $strAttrSortItemId
 * @property null|string $strAttrSortItemName
 * @property null|string $strAttrSortSchemaRelationId
 * @property null|string $strAttrSortSchemaRelationName
 * @property null|string $strAttrSortSubItemId
 * @property null|string $strAttrSortSubItemName
 * @property null|string $strAttrSortSubItemSchemaId
 * @property null|string $strAttrSortSubItemSchemaName
 */
class ItemRelationBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DateTimeFactoryInterface $objDateTimeFactory
     */
    public function __construct(
        DateTimeFactoryInterface $objDateTimeFactory,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria equal id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria start datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria end datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal item id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in item id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like item name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal item name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in item name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria is item current user profile
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IS_ITEM_CURRENT_USER_PROFILE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal schema relation id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in schema relation id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal sub item id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in sub item id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SUB_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like sub item name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SUB_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_SUB_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal sub item name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in sub item name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SUB_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal sub item schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_ITEM_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in sub item schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SUB_ITEM_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria like sub item schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SUB_ITEM_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_LIKE_SUB_ITEM_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria equal sub item schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_ITEM_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria in sub item schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_CRIT_IN_SUB_ITEM_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_SORT_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_SORT_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_SORT_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort item id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_SORT_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort item name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_SORT_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort schema relation id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_SORT_SCHEMA_RELATION_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort schema relation name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_SORT_SCHEMA_RELATION_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort sub item id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_SORT_SUB_ITEM_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort sub item name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_SORT_SUB_ITEM_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort sub item schema id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_SORT_SUB_ITEM_SCHEMA_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute sort sub item schema name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstItemRelationBrowser::ATTRIBUTE_ALIAS_SORT_SUB_ITEM_SCHEMA_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );
        $tabRuleConfigValidSort = ToolBoxValidBrowserEntity::getTabSortRuleConfig();

        // Init var
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID => $tabRuleConfigValidId,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ID => $tabRuleConfigValidTabId,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE => $tabRuleConfigValidDate,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE => $tabRuleConfigValidDate,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE => $tabRuleConfigValidDate,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE => $tabRuleConfigValidDate,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_ID => $tabRuleConfigValidId,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_ID => $tabRuleConfigValidTabId,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_ITEM_NAME => $tabRuleConfigValidString,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_NAME => $tabRuleConfigValidString,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_NAME => $tabRuleConfigValidTabString,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CURRENT_USER_PROFILE => $tabRuleConfigValidBoolean,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_ID => $tabRuleConfigValidId,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_ID => $tabRuleConfigValidTabId,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_RELATION_NAME => $tabRuleConfigValidString,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_NAME => $tabRuleConfigValidString,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_NAME => $tabRuleConfigValidTabString,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_ID => $tabRuleConfigValidId,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_ID => $tabRuleConfigValidTabId,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SUB_ITEM_NAME => $tabRuleConfigValidString,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_NAME => $tabRuleConfigValidString,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_NAME => $tabRuleConfigValidTabString,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_SCHEMA_ID => $tabRuleConfigValidId,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_SCHEMA_ID => $tabRuleConfigValidTabId,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SUB_ITEM_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_SCHEMA_NAME => $tabRuleConfigValidString,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_SCHEMA_NAME => $tabRuleConfigValidTabString,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_ID => $tabRuleConfigValidSort,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE => $tabRuleConfigValidSort,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE => $tabRuleConfigValidSort,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_ITEM_ID => $tabRuleConfigValidSort,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_ITEM_NAME => $tabRuleConfigValidSort,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_ID => $tabRuleConfigValidSort,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_NAME => $tabRuleConfigValidSort,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_ID => $tabRuleConfigValidSort,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_NAME => $tabRuleConfigValidSort,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_SCHEMA_ID => $tabRuleConfigValidSort,
                ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_SCHEMA_NAME => $tabRuleConfigValidSort
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    ($value instanceof DateTime) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ID:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_ID:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_ID:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_ID:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_SCHEMA_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ID:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_ID:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_ID:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_ID:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_SCHEMA_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            is_string($value) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ($value instanceof DateTime) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_ITEM_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_RELATION_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SUB_ITEM_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_LIKE_SUB_ITEM_SCHEMA_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_SCHEMA_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_ID:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_DT_CREATE:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_DT_UPDATE:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_ITEM_ID:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_ITEM_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_ID:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_ID:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_SCHEMA_ID:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_SORT_SUB_ITEM_SCHEMA_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_ITEM_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_NAME:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_SCHEMA_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CURRENT_USER_PROFILE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getAttributeValueQueryFormat($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_CREATE:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_CREATE:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_START_DT_UPDATE:
            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_END_DT_UPDATE:
                $result = $this->getAttributeValueFormatGet($strKey, $value);
                break;

            case ConstItemRelationBrowser::ATTRIBUTE_KEY_CRIT_IS_ITEM_CURRENT_USER_PROFILE:
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
                break;

            default:
                $result = parent::getAttributeValueQueryFormat($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}