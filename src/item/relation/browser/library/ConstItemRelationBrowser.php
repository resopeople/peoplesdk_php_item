<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\relation\browser\library;



class ConstItemRelationBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_EQUAL_ID = 'intAttrCritEqualId';
    const ATTRIBUTE_KEY_CRIT_IN_ID = 'tabAttrCritInId';
    const ATTRIBUTE_KEY_CRIT_START_DT_CREATE = 'attrCritStartDtCreate';
    const ATTRIBUTE_KEY_CRIT_END_DT_CREATE = 'attrCritEndDtCreate';
    const ATTRIBUTE_KEY_CRIT_START_DT_UPDATE = 'attrCritStartDtUpdate';
    const ATTRIBUTE_KEY_CRIT_END_DT_UPDATE = 'attrCritEndDtUpdate';
    const ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_ID = 'intAttrCritEqualItemId';
    const ATTRIBUTE_KEY_CRIT_IN_ITEM_ID = 'tabAttrCritInItemId';
    const ATTRIBUTE_KEY_CRIT_LIKE_ITEM_NAME = 'strAttrCritLikeItemName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_ITEM_NAME = 'strAttrCritEqualItemName';
    const ATTRIBUTE_KEY_CRIT_IN_ITEM_NAME = 'tabAttrCritInItemName';
    const ATTRIBUTE_KEY_CRIT_IS_ITEM_CURRENT_USER_PROFILE = 'boolAttrCritIsItemCurrentUserProfile';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_ID = 'intAttrCritEqualSchemaRelationId';
    const ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_ID = 'tabAttrCritInSchemaRelationId';
    const ATTRIBUTE_KEY_CRIT_LIKE_SCHEMA_RELATION_NAME = 'strAttrCritLikeSchemaRelationName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SCHEMA_RELATION_NAME = 'strAttrCritEqualSchemaRelationName';
    const ATTRIBUTE_KEY_CRIT_IN_SCHEMA_RELATION_NAME = 'tabAttrCritInSchemaRelationName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_ID = 'intAttrCritEqualSubItemId';
    const ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_ID = 'tabAttrCritInSubItemId';
    const ATTRIBUTE_KEY_CRIT_LIKE_SUB_ITEM_NAME = 'strAttrCritLikeSubItemName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_NAME = 'strAttrCritEqualSubItemName';
    const ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_NAME = 'tabAttrCritInSubItemName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_SCHEMA_ID = 'intAttrCritEqualSubItemSchemaId';
    const ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_SCHEMA_ID = 'tabAttrCritInSubItemSchemaId';
    const ATTRIBUTE_KEY_CRIT_LIKE_SUB_ITEM_SCHEMA_NAME = 'strAttrCritLikeSubItemSchemaName';
    const ATTRIBUTE_KEY_CRIT_EQUAL_SUB_ITEM_SCHEMA_NAME = 'strAttrCritEqualSubItemSchemaName';
    const ATTRIBUTE_KEY_CRIT_IN_SUB_ITEM_SCHEMA_NAME = 'tabAttrCritInSubItemSchemaName';
    const ATTRIBUTE_KEY_SORT_ID = 'strAttrSortId';
    const ATTRIBUTE_KEY_SORT_DT_CREATE = 'strAttrSortDtCreate';
    const ATTRIBUTE_KEY_SORT_DT_UPDATE = 'strAttrSortDtUpdate';
    const ATTRIBUTE_KEY_SORT_ITEM_ID = 'strAttrSortItemId';
    const ATTRIBUTE_KEY_SORT_ITEM_NAME = 'strAttrSortItemName';
    const ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_ID = 'strAttrSortSchemaRelationId';
    const ATTRIBUTE_KEY_SORT_SCHEMA_RELATION_NAME = 'strAttrSortSchemaRelationName';
    const ATTRIBUTE_KEY_SORT_SUB_ITEM_ID = 'strAttrSortSubItemId';
    const ATTRIBUTE_KEY_SORT_SUB_ITEM_NAME = 'strAttrSortSubItemName';
    const ATTRIBUTE_KEY_SORT_SUB_ITEM_SCHEMA_ID = 'strAttrSortSubItemSchemaId';
    const ATTRIBUTE_KEY_SORT_SUB_ITEM_SCHEMA_NAME = 'strAttrSortSubItemSchemaName';

    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ID = 'crit-equal-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ID = 'crit-in-id';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_CREATE = 'crit-start-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_CREATE = 'crit-end-dt-create';
    const ATTRIBUTE_ALIAS_CRIT_START_DT_UPDATE = 'crit-start-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_END_DT_UPDATE = 'crit-end-dt-update';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ITEM_ID = 'crit-equal-item-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_ITEM_ID = 'crit-in-item-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_ITEM_NAME = 'crit-like-item-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_ITEM_NAME = 'crit-equal-item-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_ITEM_NAME = 'crit-in-item-name';
    const ATTRIBUTE_ALIAS_CRIT_IS_ITEM_CURRENT_USER_PROFILE = 'crit-is-item-current-user-profile';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_RELATION_ID = 'crit-equal-schema-relation-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_RELATION_ID = 'crit-in-schema-relation-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_SCHEMA_RELATION_NAME = 'crit-like-schema-relation-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SCHEMA_RELATION_NAME = 'crit-equal-schema-relation-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_SCHEMA_RELATION_NAME = 'crit-in-schema-relation-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_ITEM_ID = 'crit-equal-sub-item-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_SUB_ITEM_ID = 'crit-in-sub-item-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_SUB_ITEM_NAME = 'crit-like-sub-item-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_ITEM_NAME = 'crit-equal-sub-item-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_SUB_ITEM_NAME = 'crit-in-sub-item-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_ITEM_SCHEMA_ID = 'crit-equal-sub-item-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_IN_SUB_ITEM_SCHEMA_ID = 'crit-in-sub-item-schema-id';
    const ATTRIBUTE_ALIAS_CRIT_LIKE_SUB_ITEM_SCHEMA_NAME = 'crit-like-sub-item-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_EQUAL_SUB_ITEM_SCHEMA_NAME = 'crit-equal-sub-item-schema-name';
    const ATTRIBUTE_ALIAS_CRIT_IN_SUB_ITEM_SCHEMA_NAME = 'crit-in-sub-item-schema-name';
    const ATTRIBUTE_ALIAS_SORT_ID = 'sort-id';
    const ATTRIBUTE_ALIAS_SORT_DT_CREATE = 'sort-dt-create';
    const ATTRIBUTE_ALIAS_SORT_DT_UPDATE = 'sort-dt-update';
    const ATTRIBUTE_ALIAS_SORT_ITEM_ID = 'sort-item-id';
    const ATTRIBUTE_ALIAS_SORT_ITEM_NAME = 'sort-item-name';
    const ATTRIBUTE_ALIAS_SORT_SCHEMA_RELATION_ID = 'sort-schema-relation-id';
    const ATTRIBUTE_ALIAS_SORT_SCHEMA_RELATION_NAME = 'sort-schema-relation-name';
    const ATTRIBUTE_ALIAS_SORT_SUB_ITEM_ID = 'sort-sub-item-id';
    const ATTRIBUTE_ALIAS_SORT_SUB_ITEM_NAME = 'sort-sub-item-name';
    const ATTRIBUTE_ALIAS_SORT_SUB_ITEM_SCHEMA_ID = 'sort-sub-item-schema-id';
    const ATTRIBUTE_ALIAS_SORT_SUB_ITEM_SCHEMA_NAME = 'sort-sub-item-schema-name';



}