<?php
/**
 * This class allows to define item relation entity collection class.
 * key => ItemRelationEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\relation\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\item\item\relation\model\ItemRelationEntity;



/**
 * @method null|ItemRelationEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(ItemRelationEntity $objEntity) @inheritdoc
 */
class ItemRelationEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return ItemRelationEntity::class;
    }



}