<?php
/**
 * This class allows to define item relation entity factory class.
 * Item relation entity factory allows to provide new item relation entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\relation\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\item\schema\relation\model\SchemaRelationEntityFactory;
use people_sdk\item\item\model\ItemEntityFactory;
use people_sdk\item\item\relation\library\ConstItemRelation;
use people_sdk\item\item\relation\exception\SchemaRelationEntityFactoryInvalidFormatException;
use people_sdk\item\item\relation\exception\ItemEntityFactoryInvalidFormatException;
use people_sdk\item\item\relation\exception\SchemaRelationEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\item\relation\exception\ItemEntityFactoryExecConfigInvalidFormatException;
use people_sdk\item\item\relation\model\ItemRelationEntity;



/**
 * @method null|SchemaRelationEntityFactory getObjSchemaRelationEntityFactory() Get schema relation entity factory object.
 * @method null|ItemEntityFactory getObjItemEntityFactory() Get item entity factory object.
 * @method null|array getTabSchemaRelationEntityFactoryExecConfig() Get schema relation entity factory execution configuration array.
 * @method null|array getTabItemEntityFactoryExecConfig() Get item entity factory execution configuration array.
 * @method ItemRelationEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjSchemaRelationEntityFactory(null|SchemaRelationEntityFactory $objSchemaRelationEntityFactory) Set schema relation entity factory object.
 * @method void setObjItemEntityFactory(null|ItemEntityFactory $objItemEntityFactory) Set item entity factory object.
 * @method void setTabSchemaRelationEntityFactoryExecConfig(null|array $tabSchemaRelationEntityFactoryExecConfig) Set schema relation entity factory execution configuration array.
 * @method void setTabItemEntityFactoryExecConfig(null|array $tabItemEntityFactoryExecConfig) Set item entity factory execution configuration array.
 */
class ItemRelationEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|SchemaRelationEntityFactory $objSchemaRelationEntityFactory = null
     * @param null|ItemEntityFactory $objItemEntityFactory = null
     * @param null|array $tabSchemaRelationEntityFactoryExecConfig = null
     * @param null|array $tabItemEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        SchemaRelationEntityFactory $objSchemaRelationEntityFactory = null,
        ItemEntityFactory $objItemEntityFactory = null,
        array $tabSchemaRelationEntityFactoryExecConfig = null,
        array $tabItemEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init schema relation entity factory
        $this->setObjSchemaRelationEntityFactory($objSchemaRelationEntityFactory);

        // Init item entity factory
        $this->setObjItemEntityFactory($objItemEntityFactory);

        // Init schema relation entity factory execution config
        $this->setTabSchemaRelationEntityFactoryExecConfig($tabSchemaRelationEntityFactoryExecConfig);

        // Init item entity factory execution config
        $this->setTabItemEntityFactoryExecConfig($tabItemEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstItemRelation::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstItemRelation::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstItemRelation::DATA_KEY_ITEM_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstItemRelation::DATA_KEY_ITEM_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstItemRelation::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstItemRelation::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstItemRelation::DATA_KEY_ITEM_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstItemRelation::DATA_KEY_ITEM_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstItemRelation::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY,
            ConstItemRelation::DATA_KEY_ITEM_ENTITY_FACTORY,
            ConstItemRelation::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY_EXEC_CONFIG,
            ConstItemRelation::DATA_KEY_ITEM_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstItemRelation::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY:
                    SchemaRelationEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstItemRelation::DATA_KEY_ITEM_ENTITY_FACTORY:
                    ItemEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstItemRelation::DATA_KEY_SCHEMA_RELATION_ENTITY_FACTORY_EXEC_CONFIG:
                    SchemaRelationEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstItemRelation::DATA_KEY_ITEM_ENTITY_FACTORY_EXEC_CONFIG:
                    ItemEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => ItemRelationEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstItemRelation::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new ItemRelationEntity(
            $objDateTimeFactory,
            array(),
            $objValidator,
            $this->getObjSchemaRelationEntityFactory(),
            $this->getObjItemEntityFactory(),
            $this->getTabSchemaRelationEntityFactoryExecConfig(),
            $this->getTabItemEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}