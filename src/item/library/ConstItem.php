<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\item\library;



class ConstItem
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_USER_PROFILE_ENTITY_FACTORY = 'objUserProfileEntityFactory';
    const DATA_KEY_APP_PROFILE_ENTITY_FACTORY = 'objAppProfileEntityFactory';
    const DATA_KEY_SCHEMA_ENTITY_FACTORY = 'objSchemaEntityFactory';
    const DATA_KEY_ITEM_ATTR_VALUE_ENTITY_FACTORY = 'objItemAttrValueEntityFactory';
    const DATA_KEY_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG = 'tabUserProfileEntityFactoryExecConfig';
    const DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG = 'tabAppProfileEntityFactoryExecConfig';
    const DATA_KEY_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG = 'tabSchemaEntityFactoryExecConfig';
    const DATA_KEY_ITEM_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG = 'tabItemAttrValueEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_NAME = 'strAttrName';
    const ATTRIBUTE_KEY_SCHEMA = 'attrSchema';
    const ATTRIBUTE_KEY_PROFILE_TYPE = 'strAttrProfileType';
    const ATTRIBUTE_KEY_PROFILE = 'attrProfile';
    const ATTRIBUTE_KEY_ITEM_ATTR_VALUE_ENTITY = 'objAttrItemAttrValueEntity';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_NAME = 'name';
    const ATTRIBUTE_ALIAS_SCHEMA = 'schema';
    const ATTRIBUTE_ALIAS_PROFILE_TYPE = 'profile-type';
    const ATTRIBUTE_ALIAS_PROFILE = 'profile';
    const ATTRIBUTE_ALIAS_ITEM_ATTR_VALUE_ENTITY = 'attribute';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_NAME = 'name';
    const ATTRIBUTE_NAME_SAVE_SCHEMA = 'schema';
    const ATTRIBUTE_NAME_SAVE_PROFILE_TYPE = 'profile-type';
    const ATTRIBUTE_NAME_SAVE_PROFILE = 'profile';
    const ATTRIBUTE_NAME_SAVE_ITEM_ATTR_VALUE_ENTITY = 'attribute';



    // Profile type configuration
    const PROFILE_TYPE_USER_PROFILE = 'UserProfile';
    const PROFILE_TYPE_APP_PROFILE = 'AppProfile';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_PROFILE = 'profile';



    // Exception message constants
    const EXCEPT_MSG_USER_PROFILE_ENTITY_FACTORY_INVALID_FORMAT =
        'Following user profile entity factory "%1$s" invalid! It must be an user profile entity factory object.';
    const EXCEPT_MSG_APP_PROFILE_ENTITY_FACTORY_INVALID_FORMAT =
        'Following application profile entity factory "%1$s" invalid! It must be an application profile entity factory object.';
    const EXCEPT_MSG_SCHEMA_ENTITY_FACTORY_INVALID_FORMAT =
        'Following schema entity factory "%1$s" invalid! It must be a schema entity factory object.';
    const EXCEPT_MSG_ITEM_ATTR_VALUE_ENTITY_INVALID_FORMAT =
        'Following item attribute value entity factory "%1$s" invalid! It must be an item attribute value entity factory object.';
    const EXCEPT_MSG_USER_PROFILE_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the user profile entity factory execution configuration standard.';
    const EXCEPT_MSG_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the application profile entity factory execution configuration standard.';
    const EXCEPT_MSG_SCHEMA_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the schema entity factory execution configuration standard.';
    const EXCEPT_MSG_ITEM_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the item attribute value entity factory execution configuration standard.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get index array of profile types.
     *
     * @return array
     */
    public static function getTabProfileType()
    {
        // Return result
        return array(
            self::PROFILE_TYPE_USER_PROFILE,
            self::PROFILE_TYPE_APP_PROFILE
        );
    }



}