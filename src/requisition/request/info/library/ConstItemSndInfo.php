<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\requisition\request\info\library;



class ConstItemSndInfo
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // URL route arguments configuration
    const URL_ROUTE_ARG_KEY_SCHEMA_ID = '{schema-id}';
    const URL_ROUTE_ARG_KEY_AREA_ID = '{area-id}';



}