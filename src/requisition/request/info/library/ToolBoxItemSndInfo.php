<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\item\requisition\request\info\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\model\HttpRequest;
use people_sdk\library\table\library\ToolBoxTable;
use people_sdk\item\requisition\request\info\library\ConstItemSndInfo;



class ToolBoxItemSndInfo extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get sending information array,
     * with specified schema URL route.
     *
     * Route pattern format:
     * "string sprintf pattern,
     * where '%1$s' replaced by schema id URL route argument key."
     *
     * Sending information array format:
     * Null or @see HttpRequest sending information array format.
     *
     * Return format:
     * Null or @see HttpRequest sending information array format.
     *
     * @param string $strRoutePattern
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithSchemaUrlRoute($strRoutePattern, array $tabInfo = null)
    {
        // Init var
        $tabSrcInfo = $tabInfo;
        $tabInfo = (
            is_string($strRoutePattern) ?
                array(
                    ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => sprintf(
                        $strRoutePattern,
                        ConstItemSndInfo::URL_ROUTE_ARG_KEY_SCHEMA_ID
                    ),
                    ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG_SPEC => [
                        ConstItemSndInfo::URL_ROUTE_ARG_KEY_SCHEMA_ID => null
                    ]
                ) :
                null
        );
        $result = ToolBoxTable::getTabItemFromSrc($tabInfo, $tabSrcInfo);

        // Return result
        return $result;
    }




    /**
     * Get sending information array,
     * with specified schema URL route arguments.
     *
     * Sending information array format:
     * Null or @see HttpRequest sending information array format.
     *
     * Return format:
     * Null or @see HttpRequest sending information array format.
     *
     * @param integer $intSchemaId
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithSchemaUrlRouteArg($intSchemaId, array $tabInfo = null)
    {
        // Init var
        $tabSrcInfo = $tabInfo;
        $tabInfo = (
            is_int($intSchemaId) ?
                array(
                    ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG => [
                        ConstItemSndInfo::URL_ROUTE_ARG_KEY_SCHEMA_ID => $intSchemaId
                    ]
                ) :
                null
        );
        $result = ToolBoxTable::getTabItemFromSrc($tabInfo, $tabSrcInfo);

        // Return result
        return $result;
    }



    /**
     * Get sending information array,
     * with specified area URL route.
     *
     * Route pattern format:
     * "string sprintf pattern,
     * where '%1$s' replaced by area id URL route argument key."
     *
     * Sending information array format:
     * Null or @see HttpRequest sending information array format.
     *
     * Return format:
     * Null or @see HttpRequest sending information array format.
     *
     * @param string $strRoutePattern
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAreaUrlRoute($strRoutePattern, array $tabInfo = null)
    {
        // Init var
        $tabSrcInfo = $tabInfo;
        $tabInfo = (
            is_string($strRoutePattern) ?
                array(
                    ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => sprintf(
                        $strRoutePattern,
                        ConstItemSndInfo::URL_ROUTE_ARG_KEY_AREA_ID
                    ),
                    ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG_SPEC => [
                        ConstItemSndInfo::URL_ROUTE_ARG_KEY_AREA_ID => null
                    ]
                ) :
                null
        );
        $result = ToolBoxTable::getTabItemFromSrc($tabInfo, $tabSrcInfo);

        // Return result
        return $result;
    }



    /**
     * Get sending information array,
     * with specified area URL route arguments.
     *
     * Sending information array format:
     * Null or @see HttpRequest sending information array format.
     *
     * Return format:
     * Null or @see HttpRequest sending information array format.
     *
     * @param integer $intAreaId
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAreaUrlRouteArg($intAreaId, array $tabInfo = null)
    {
        // Init var
        $tabSrcInfo = $tabInfo;
        $tabInfo = (
            is_int($intAreaId) ?
                array(
                    ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE_ARG => [
                        ConstItemSndInfo::URL_ROUTE_ARG_KEY_AREA_ID => $intAreaId
                    ]
                ) :
                null
        );
        $result = ToolBoxTable::getTabItemFromSrc($tabInfo, $tabSrcInfo);

        // Return result
        return $result;
    }



}