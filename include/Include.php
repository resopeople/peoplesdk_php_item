<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/requisition/request/info/library/ConstItemSndInfo.php');
include($strRootPath . '/src/requisition/request/info/library/ToolBoxItemSndInfo.php');

include($strRootPath . '/src/schema/library/ConstSchema.php');
include($strRootPath . '/src/schema/model/SchemaEntity.php');
include($strRootPath . '/src/schema/model/SchemaEntityCollection.php');
include($strRootPath . '/src/schema/model/SchemaEntityFactory.php');
include($strRootPath . '/src/schema/model/repository/SchemaEntityMultiRepository.php');
include($strRootPath . '/src/schema/model/repository/SchemaEntityMultiCollectionRepository.php');

include($strRootPath . '/src/schema/browser/library/ConstSchemaBrowser.php');
include($strRootPath . '/src/schema/browser/model/SchemaBrowserEntity.php');

include($strRootPath . '/src/schema/attribute/library/ConstSchemaAttr.php');
include($strRootPath . '/src/schema/attribute/library/ToolBoxSchemaAttr.php');
include($strRootPath . '/src/schema/attribute/library/ToolBoxSchemaAttrRepository.php');
include($strRootPath . '/src/schema/attribute/exception/SchemaEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/schema/attribute/exception/SchemaEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/schema/attribute/model/SchemaAttrEntity.php');
include($strRootPath . '/src/schema/attribute/model/SchemaAttrEntityCollection.php');
include($strRootPath . '/src/schema/attribute/model/SchemaAttrEntityFactory.php');
include($strRootPath . '/src/schema/attribute/model/repository/SchemaAttrEntitySimpleRepository.php');
include($strRootPath . '/src/schema/attribute/model/repository/SchemaAttrEntitySimpleCollectionRepository.php');
include($strRootPath . '/src/schema/attribute/model/repository/SchemaAttrEntityMultiRepository.php');
include($strRootPath . '/src/schema/attribute/model/repository/SchemaAttrEntityMultiCollectionRepository.php');

include($strRootPath . '/src/schema/attribute/browser/library/ConstSchemaAttrBrowser.php');
include($strRootPath . '/src/schema/attribute/browser/model/SchemaAttrBrowserEntity.php');

include($strRootPath . '/src/schema/attribute/provider/model/SchemaAttrProvider.php');

include($strRootPath . '/src/schema/relation/library/ConstSchemaRelation.php');
include($strRootPath . '/src/schema/relation/exception/SchemaEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/schema/relation/exception/SchemaEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/schema/relation/model/SchemaRelationEntity.php');
include($strRootPath . '/src/schema/relation/model/SchemaRelationEntityCollection.php');
include($strRootPath . '/src/schema/relation/model/SchemaRelationEntityFactory.php');
include($strRootPath . '/src/schema/relation/model/repository/SchemaRelationEntityMultiRepository.php');
include($strRootPath . '/src/schema/relation/model/repository/SchemaRelationEntityMultiCollectionRepository.php');

include($strRootPath . '/src/schema/relation/browser/library/ConstSchemaRelationBrowser.php');
include($strRootPath . '/src/schema/relation/browser/model/SchemaRelationBrowserEntity.php');

include($strRootPath . '/src/category/library/ConstCategory.php');
include($strRootPath . '/src/category/model/CategoryEntity.php');
include($strRootPath . '/src/category/model/CategoryEntityCollection.php');
include($strRootPath . '/src/category/model/CategoryEntityFactory.php');
include($strRootPath . '/src/category/model/repository/CategoryEntityMultiRepository.php');
include($strRootPath . '/src/category/model/repository/CategoryEntityMultiCollectionRepository.php');

include($strRootPath . '/src/category/browser/library/ConstCategoryBrowser.php');
include($strRootPath . '/src/category/browser/model/CategoryBrowserEntity.php');

include($strRootPath . '/src/area/library/ConstArea.php');
include($strRootPath . '/src/area/exception/SchemaEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/area/exception/SchemaEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/area/model/AreaEntity.php');
include($strRootPath . '/src/area/model/AreaEntityCollection.php');
include($strRootPath . '/src/area/model/AreaEntityFactory.php');
include($strRootPath . '/src/area/model/repository/AreaEntityMultiRepository.php');
include($strRootPath . '/src/area/model/repository/AreaEntityMultiCollectionRepository.php');

include($strRootPath . '/src/area/browser/library/ConstAreaBrowser.php');
include($strRootPath . '/src/area/browser/model/AreaBrowserEntity.php');

include($strRootPath . '/src/area/schema/attribute/scope/library/ConstAreaAttrScope.php');
include($strRootPath . '/src/area/schema/attribute/scope/exception/SchemaAttrEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/area/schema/attribute/scope/exception/AreaEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/area/schema/attribute/scope/exception/SchemaAttrEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/area/schema/attribute/scope/exception/AreaEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/area/schema/attribute/scope/model/AreaAttrScopeEntity.php');
include($strRootPath . '/src/area/schema/attribute/scope/model/AreaAttrScopeEntityCollection.php');
include($strRootPath . '/src/area/schema/attribute/scope/model/AreaAttrScopeEntityFactory.php');
include($strRootPath . '/src/area/schema/attribute/scope/model/repository/AreaAttrScopeEntityMultiRepository.php');
include($strRootPath . '/src/area/schema/attribute/scope/model/repository/AreaAttrScopeEntityMultiCollectionRepository.php');

include($strRootPath . '/src/area/schema/attribute/scope/browser/library/ConstAreaAttrScopeBrowser.php');
include($strRootPath . '/src/area/schema/attribute/scope/browser/model/AreaAttrScopeBrowserEntity.php');

include($strRootPath . '/src/area/schema/relation/scope/library/ConstAreaRelationScope.php');
include($strRootPath . '/src/area/schema/relation/scope/exception/SchemaRelationEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/area/schema/relation/scope/exception/AreaEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/area/schema/relation/scope/exception/SchemaRelationEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/area/schema/relation/scope/exception/AreaEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/area/schema/relation/scope/model/AreaRelationScopeEntity.php');
include($strRootPath . '/src/area/schema/relation/scope/model/AreaRelationScopeEntityCollection.php');
include($strRootPath . '/src/area/schema/relation/scope/model/AreaRelationScopeEntityFactory.php');
include($strRootPath . '/src/area/schema/relation/scope/model/repository/AreaRelationScopeEntityMultiRepository.php');
include($strRootPath . '/src/area/schema/relation/scope/model/repository/AreaRelationScopeEntityMultiCollectionRepository.php');

include($strRootPath . '/src/area/schema/relation/scope/browser/library/ConstAreaRelationScopeBrowser.php');
include($strRootPath . '/src/area/schema/relation/scope/browser/model/AreaRelationScopeBrowserEntity.php');

include($strRootPath . '/src/area/category/scope/library/ConstAreaCategoryScope.php');
include($strRootPath . '/src/area/category/scope/exception/CategoryEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/area/category/scope/exception/AreaEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/area/category/scope/exception/CategoryEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/area/category/scope/exception/AreaEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/area/category/scope/model/AreaCategoryScopeEntity.php');
include($strRootPath . '/src/area/category/scope/model/AreaCategoryScopeEntityCollection.php');
include($strRootPath . '/src/area/category/scope/model/AreaCategoryScopeEntityFactory.php');
include($strRootPath . '/src/area/category/scope/model/repository/AreaCategoryScopeEntityMultiRepository.php');
include($strRootPath . '/src/area/category/scope/model/repository/AreaCategoryScopeEntityMultiCollectionRepository.php');

include($strRootPath . '/src/area/category/scope/browser/library/ConstAreaCategoryScopeBrowser.php');
include($strRootPath . '/src/area/category/scope/browser/model/AreaCategoryScopeBrowserEntity.php');

include($strRootPath . '/src/area/member/library/ConstAreaMember.php');
include($strRootPath . '/src/area/member/exception/UserProfileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/area/member/exception/AreaEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/area/member/exception/UserProfileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/area/member/exception/AreaEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/area/member/model/AreaMemberEntity.php');
include($strRootPath . '/src/area/member/model/AreaMemberEntityCollection.php');
include($strRootPath . '/src/area/member/model/AreaMemberEntityFactory.php');
include($strRootPath . '/src/area/member/model/repository/AreaMemberEntityMultiRepository.php');
include($strRootPath . '/src/area/member/model/repository/AreaMemberEntityMultiCollectionRepository.php');

include($strRootPath . '/src/area/member/browser/library/ConstAreaMemberBrowser.php');
include($strRootPath . '/src/area/member/browser/model/AreaMemberBrowserEntity.php');

include($strRootPath . '/src/area/group/member/library/ConstAreaGrpMember.php');
include($strRootPath . '/src/area/group/member/exception/GroupEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/area/group/member/exception/AreaEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/area/group/member/exception/GroupEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/area/group/member/exception/AreaEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/area/group/member/model/AreaGrpMemberEntity.php');
include($strRootPath . '/src/area/group/member/model/AreaGrpMemberEntityCollection.php');
include($strRootPath . '/src/area/group/member/model/AreaGrpMemberEntityFactory.php');
include($strRootPath . '/src/area/group/member/model/repository/AreaGrpMemberEntityMultiRepository.php');
include($strRootPath . '/src/area/group/member/model/repository/AreaGrpMemberEntityMultiCollectionRepository.php');

include($strRootPath . '/src/area/group/member/browser/library/ConstAreaGrpMemberBrowser.php');
include($strRootPath . '/src/area/group/member/browser/model/AreaGrpMemberBrowserEntity.php');

include($strRootPath . '/src/item/attribute/value/library/ConstItemAttrValue.php');
include($strRootPath . '/src/item/attribute/value/exception/SchemaAttrProviderInvalidFormatException.php');
include($strRootPath . '/src/item/attribute/value/model/ItemAttrValueEntity.php');
include($strRootPath . '/src/item/attribute/value/model/ItemAttrValueEntityFactory.php');

include($strRootPath . '/src/item/library/ConstItem.php');
include($strRootPath . '/src/item/exception/UserProfileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/item/exception/AppProfileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/item/exception/SchemaEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/item/exception/ItemAttrValueEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/item/exception/UserProfileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/item/exception/AppProfileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/item/exception/SchemaEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/item/exception/ItemAttrValueEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/item/model/ItemEntity.php');
include($strRootPath . '/src/item/model/ItemEntityCollection.php');
include($strRootPath . '/src/item/model/ItemEntityFactory.php');
include($strRootPath . '/src/item/model/repository/ItemEntityMultiRepository.php');
include($strRootPath . '/src/item/model/repository/ItemEntityMultiCollectionRepository.php');

include($strRootPath . '/src/item/browser/library/ConstItemBrowser.php');
include($strRootPath . '/src/item/browser/model/ItemBrowserEntity.php');

include($strRootPath . '/src/item/relation/library/ConstItemRelation.php');
include($strRootPath . '/src/item/relation/exception/SchemaRelationEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/item/relation/exception/ItemEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/item/relation/exception/SchemaRelationEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/item/relation/exception/ItemEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/item/relation/model/ItemRelationEntity.php');
include($strRootPath . '/src/item/relation/model/ItemRelationEntityCollection.php');
include($strRootPath . '/src/item/relation/model/ItemRelationEntityFactory.php');
include($strRootPath . '/src/item/relation/model/repository/ItemRelationEntityMultiRepository.php');
include($strRootPath . '/src/item/relation/model/repository/ItemRelationEntityMultiCollectionRepository.php');

include($strRootPath . '/src/item/relation/browser/library/ConstItemRelationBrowser.php');
include($strRootPath . '/src/item/relation/browser/model/ItemRelationBrowserEntity.php');

include($strRootPath . '/src/item/category/library/ConstItemCategory.php');
include($strRootPath . '/src/item/category/exception/CategoryEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/item/category/exception/ItemEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/item/category/exception/CategoryEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/item/category/exception/ItemEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/item/category/model/ItemCategoryEntity.php');
include($strRootPath . '/src/item/category/model/ItemCategoryEntityCollection.php');
include($strRootPath . '/src/item/category/model/ItemCategoryEntityFactory.php');
include($strRootPath . '/src/item/category/model/repository/ItemCategoryEntityMultiRepository.php');
include($strRootPath . '/src/item/category/model/repository/ItemCategoryEntityMultiCollectionRepository.php');

include($strRootPath . '/src/item/category/browser/library/ConstItemCategoryBrowser.php');
include($strRootPath . '/src/item/category/browser/model/ItemCategoryBrowserEntity.php');